from django import forms
from .models import Article, Category

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = [
            "title",
            "descriptions",
            'parent',
            "slug",
        ]
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # unused_files = Category.objects.filter(category__isnull=True)
        instance = kwargs.get("instance")

class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ["title","image","content","category"]
