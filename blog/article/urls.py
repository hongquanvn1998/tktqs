from django.contrib import admin
from django.urls import path
from .views import *
from django.conf.urls import url

app_name = "article"

urlpatterns = [
    path('', dashboard, name = "dashboard"),
    # path('dashboard/', dashboard, name = "dashboard"),
    # path('addarticle/', addarticle, name = "addarticle"),
    path('detail/<int:id>', detail, name = "detail"),
    # path('update/<int:id>', updateArticle, name = "updateArticle"),
    # path('delete/<int:id>', deleteArticle, name = "deleteArticle"),
    # path('comment/<int:id>', comment, name = "comment"),

    # path('<slug:slug>/', allpost, name = "allpost"),
    # path('<int:id>/', allpost, name = "allpost"),
    # url(r'^(?P<slug>.*)/$', allpost, name = "allpost"),

    # path('category-dashboard/', categorydashboard, name = "categorydashboard"),
    # path('add-category-article/', addcategoryArticle, name = "addcategoryArticle"),
    # path('category-detail/<int:id>', categorydetail, name = "categorydetail"),
    # path('category-update/<int:id>', updateCategoryArticle, name = "updateCategoryArticle"),
    # path('category-delete/<int:id>', deleteCategoryArticle, name = "deleteCategoryArticle"),
    # path('category/<int:id>', category,name="category")

]