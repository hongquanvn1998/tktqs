from django.shortcuts import render, HttpResponse, redirect, get_object_or_404, reverse
from django.contrib import messages
from .models import Article, Comment, Category
from .forms import ArticleForm,CategoryForm
from django.contrib.auth.decorators import login_required
from product.models import Category as prod_category,Contact,Product
from slideshow.models import AddSlide, CreateGroupSlide
from product.forms import ContactForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail.backends import console
from django.db.models import Q

def post(request):
    # articles = Article.objects.all()[:3]
    creator_list = [1,2,3]
    my_filter_qs = Q()
    for creator in creator_list:
        my_filter_qs = my_filter_qs | Q(category_id=creator)
    articles = Article.objects.filter(my_filter_qs)
    category_list = Category.objects.filter(parent_id = 1)
    science_list = Category.objects.filter(parent_id = 4)
    prod_category_list = prod_category.objects.filter(parent_id = 1)
    prod_other_category_list = prod_category.objects.filter(parent_id = 2)
    prod_standrad_category_list = prod_category.objects.filter(parent_id = 9)
    list_prod = Product.objects.all()[:12]
    hehe=['1','2','3','4']
    print(list_prod)
    
    group_slide = CreateGroupSlide.objects.filter(active=True)[:1]
    slide_show = AddSlide.objects.filter(slide=group_slide,active=True)
    context = {
        "articles" : articles,
        "category_list":category_list,
        "science_list":science_list,
        "prod_category_list":prod_category_list,
        "prod_other_category_list":prod_other_category_list,
        "prod_standrad_category_list":prod_standrad_category_list,
        "list_prod":list_prod,
        "slide_show":slide_show,
    }
    return render(request,"index.html", context)

def get_category(request,id):
    categories = Category.objects.filter(parent_id=id)
    paginator = Paginator(categories, 6)
    page = request.GET.get('page')
    categories = paginator.get_page(page)

    category_list = Category.objects.filter(parent_id = 1)
    science_list = Category.objects.filter(parent_id = 4)
    prod_category_list = prod_category.objects.filter(parent_id = 1)
    prod_other_category_list = prod_category.objects.filter(parent_id = 2)
    prod_standrad_category_list = prod_category.objects.filter(parent_id = 9)

    context = {
        "categories":categories,
        "category_list":category_list,
        "science_list":science_list,
        "prod_category_list":prod_category_list,
        "prod_other_category_list":prod_other_category_list,
        "prod_standrad_category_list":prod_standrad_category_list,
    }
    return render(request,"category_article.html", context)

def contact(request):

    category_list = Category.objects.filter(parent_id = 1)
    science_list = Category.objects.filter(parent_id = 4)
    prod_category_list = prod_category.objects.filter(parent_id = 1)
    prod_other_category_list = prod_category.objects.filter(parent_id = 2)
    prod_standrad_category_list = prod_category.objects.filter(parent_id = 9)

    form = ContactForm(request.POST or None)
    if form.is_valid():
        contact = form.save(commit = False)
        contact.save()
        messages.warning(request, "Gửi yêu cầu thành công!")
        return redirect('contact')

    context = {
        "category_list":category_list,
        "science_list":science_list,
        "prod_category_list":prod_category_list,
        "prod_other_category_list":prod_other_category_list,
        "prod_standrad_category_list":prod_standrad_category_list,
        "form":form,
    }
    return render(request,"contact.html", context)


def allpost(request,slug):
    # category = get_object_or_404(Article, id = id)

    category_list = Category.objects.filter(parent_id = 1)
    science_list = Category.objects.filter(parent_id = 4)
    prod_category_list = prod_category.objects.filter(parent_id = 1)
    prod_other_category_list = prod_category.objects.filter(parent_id = 2)
    prod_standrad_category_list = prod_category.objects.filter(parent_id = 9)

    category = get_object_or_404(Category, slug=slug)
    articles = Article.objects.filter(category_id = category.id)

    paginator = Paginator(articles, 6)
    page = request.GET.get('page')
    articles = paginator.get_page(page)

    context = {
        "category":category,
        "articles" : articles,
        "category_list":category_list,
        "science_list":science_list,
        "prod_category_list":prod_category_list,
        "prod_other_category_list":prod_other_category_list,
        "prod_standrad_category_list":prod_standrad_category_list,
    }
    return render(request,"news/news_action.html", context)

# @login_required(login_url="user:login")
# def index(request):
#     #return HttpResponse('<h3>Anasayfa</h3>')
#     return render(request, "admin/index.html")

# @login_required(login_url="user:login")
# def about(request):
#     return render(request, "admin/about.html")

@login_required(login_url="user:login")
def dashboard(request):
    articles = Article.objects.all()
    context = {
        "articles" : articles
    }
    return render(request,"admin/dashboard.html", context)

# @login_required(login_url="user:login")
# def addarticle(request):
#     form = ArticleForm(request.POST or None, request.FILES or None)
#     if form.is_valid():
#         article = form.save(commit = False)
#         article.author = request.user
#         article.save()
#         messages.warning(request, "Tạo bài viết thành công!")
#         return redirect("article:dashboard")
#     context = {
#         "form" : form
#     }
#     return render(request,"admin/addarticle.html", context)

def detail(request, id):
    article = get_object_or_404(Article, id = id)
    article_list = Article.objects.all()[:7]

    category_list = Category.objects.filter(parent_id = 1)
    science_list = Category.objects.filter(parent_id = 4)
    prod_category_list = prod_category.objects.filter(parent_id = 1)
    prod_other_category_list = prod_category.objects.filter(parent_id = 2)
    prod_standrad_category_list = prod_category.objects.filter(parent_id = 9)


    context = {
        "article" : article,
        "article_list":article_list,
        "category_list":category_list,
        "science_list":science_list,
        "prod_category_list":prod_category_list,
        "prod_other_category_list":prod_other_category_list,
        "prod_standrad_category_list":prod_standrad_category_list,
    }
    return render(request,"post_detail.html", context)