from django.db import models
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from mptt.models import MPTTModel, TreeForeignKey
from django.utils import timezone
from slugify import slugify
from datetime import datetime


class Category(MPTTModel): 

    parent = TreeForeignKey('self',null=True, blank=True,
    related_name='children', verbose_name='Danh mục cha',
    db_index=True, on_delete=models.CASCADE, 
    default=0,)
    image = models.ImageField(blank = True, null = True, verbose_name="Ảnh đại diện danh mục")
    slug = models.SlugField(max_length=500, verbose_name='Đường dẫn')
    title = models.CharField(max_length=255, verbose_name='Tên danh mục')
    descriptions = models.CharField(max_length=100, blank=True,null=True, verbose_name='Mô tả')
    created_date =  models.DateTimeField(auto_now_add=True, blank=True, verbose_name='Ngày tạo')
    updated_date =  models.DateTimeField(blank=True,null=True, verbose_name='Ngày cập nhật')

    def __str__(self):
        return self.title 

    class Meta:
        unique_together = (('parent', 'slug',))  
        verbose_name_plural = "Danh mục"
        verbose_name = 'Danh mục'
        indexes =(
            models.Index(fields=['title']),
            models.Index(fields=['parent']),
            )
    class MPTTMeta: 
        order_insertion_by = ['id']
        
    def _generate_slug(self):
        if self.parent is None:
            r = slugify(self.title)
            self.slug = r
            return self
        else:
            r= slugify(self.title)
            self.slug = '%s/%s' % (self.parent.slug,r)
            return self
    
    def _change_slug(self): 
        if self.parent is None:
            return self._generate_slug()
        
        if self.parent.slug == '':
            return self._generate_slug() 

        _index = self.slug.find(self.parent.slug)
        if _index < 0:
            self._generate_slug() 
        if _index == 0:
            self._generate_slug() 
        else:
            self._generate_slug()  

        return self

        
    def save(self, *args, **kwargs): 
        if not self.pk:
            self._generate_slug() 
        else:
            self._change_slug()
            self.updated_date = datetime.now()

        super().save(*args, **kwargs)


    def get_slug_list(self):
        try:
            ancestors = self.get_ancestors(include_self=True)
        except:
            ancestors = []
        else:
            ancestors = [ i.slug for i in ancestors]
            slugs = []
        for i in range(len(ancestors)):
            slugs.append('/'.join(ancestors[:i+1]))
        return slugs
    @property
    def image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url


class Article(models.Model):
    category = TreeForeignKey('Category',on_delete=models.CASCADE, verbose_name='Danh mục')
    title = models.CharField(max_length = 500, verbose_name = 'Tiêu đề')
    content = RichTextUploadingField(blank=True,null=True,verbose_name="Nội dung",external_plugin_resources=[(
                                        'youtube',
                                        '/static/base/vendor/ckeditor_plugins/youtube/youtube/',
                                        'plugin.js',
                                    ),])
    created_date = models.DateTimeField(auto_now_add=True,verbose_name='Ngày tạo')
    image = models.ImageField(verbose_name="Ảnh đại diện")
    def __str__(self):
        return self.title
    class Meta:
        db_table = 'Article_post'
        ordering = ["-created_date"]
        verbose_name = 'Bài viết'
        verbose_name_plural = 'Bài viết'
    @property
    def image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url

class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE, verbose_name = "Bài viết", related_name = "comments")
    author = models.ForeignKey("auth.User", on_delete = models.CASCADE, verbose_name = "Tác giả")
    contents = models.CharField(max_length = 200, verbose_name = "Bình luận")
    date = models.DateTimeField(auto_now_add = True, verbose_name = "Ngày bình luận")
    def __str__(self):
        return "{} - {}".format(self.author, self.article)
    class Meta:
        db_table = "Article_comment"
        verbose_name = 'Bình luận'
        verbose_name_plural = 'Bình luận'
        ordering = ["-date"]

# class ChangeBanner(models.Model):
    