from django.contrib import admin
from .models import Article, Comment, Category
from django.conf.urls import url

class CategoryInline(admin.TabularInline):
    model = Category
    fieldsets = (
        ('', { 
            'fields': (
                ('title','descriptions'),
                ('image'),
            )
        }), 
    )   
    extra =0

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    fieldsets = (
        ('', { 
            'fields': (
                ('parent','title'),
                ('descriptions','image'),
            )
        }),    
 
    )   
    list_display = (
                    'title',
                    'created_date',
                    'parent',
                    'descriptions'
                    )
    list_display_links = (
                        'title',
                        )
    search_fields = (
                        'title',
                    )
    list_filter = (
                    'created_date',
                    )
    inlines = [CategoryInline]

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = (
                    'title',
                    'created_date',
                    'category'
                    )
    list_display_links = (
                        'title',
                        )
    search_fields = (
                        'title',
                    )
    list_filter = (
                    'created_date',
                    )
