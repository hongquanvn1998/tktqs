/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function(config) {
    // Define changes to default configuration here. For example:
    config.language = 'en';
    // config.uiColor = '#AADC6E';
    config.extraPlugins = 'imageuploader';

    /// MY CONFIG
    // config.enterMode = CKEDITOR.ENTER_BR;
    // config.shiftEnterMode = CKEDITOR.ENTER_P;
    config.removePlugins = 'showborders';
    // config.enterMode = 2
    // config.allowedContent = true 
    config.font_names = 'Myriad-Pro Regular/Myriad-Pro Regular; Myriad-Pro Italic/Myriad-Pro Italic;Myriad-Pro Bold/Myriad-Pro Bold;' + config.font_names

    config.font_defaultLabel = 'Myriad-Pro Regular';
    config.fontSize_defaultLabel = '14px';
    config.extraPlugins = 'autogrow';
    config.autoGrow_minHeight = 250;
    config.autoGrow_maxHeight = 600;
    config.enterMode = CKEDITOR.ENTER_BR // pressing the ENTER Key puts the <br/> tag
    config.shiftEnterMode = CKEDITOR.ENTER_P; //pressing the SHIFT + ENTER Keys puts the <p> tag
};