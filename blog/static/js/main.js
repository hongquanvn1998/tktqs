$(document).ready(function(){
    var slideit = $('.spam-ct .img-spam').lightSlider({
        item:1,
        slideMargin:0,
        loop:true,
        pager:false
    })
    $('.clprev').click(function(){
        slideit.goToPrevSlide();
    })
    $('.clnext').click(function(){
        slideit.goToNextSlide();
    })
    
    $('.slidebanner').lightSlider({
        item:1,
        slideMargin:0,
        loop:true,
        auto: true,
        pause: 3000,
        speed: 1000,
    })
    $('.header .search-mntop').click(function(){
        $('.fs-mnt').toggleClass('active')
    })
    if ($('header').length) {
        fixmenu = function () {
            var tigger1 = $('.banner, .banner-mn').height() - 50;
            var scrollTop1 = $(window).scrollTop();
            if (scrollTop1 > tigger1) {
                $('.header').addClass('fixed');
            } else {
                $('.header').removeClass('fixed');
            }
        };
        fixmenu();
        $(window).on('scroll', function () {
            fixmenu();
        });
    }

    var sliderglr = $('#imageGallery').lightSlider({
        gallery:true,
        item:1,
        loop:true,
        thumbItem:4,
        slideMargin:0,
        enableDrag: false,
        currentPagerPosition:'left',
        responsive : [
            {
                breakpoint:800,
                settings: {
                    thumbItem:3,
                }
            },
        ],
        onSliderLoad: function(el) {
            el.lightGallery({
                selector: '#imageGallery .lslide'
            });
        }   
    }); 
    $('.prevglr').click(function(){
        sliderglr.goToPrevSlide();
    })
    $('.nextglr').click(function(){
        sliderglr.goToNextSlide();
    })
    $('.touch-onmb').click(function(){
        $(this).parents('.header-ct').children('.touch-onmb').toggleClass('active')
        $(this).parents('.header-ct').children('.bg-clmn').toggleClass('active')
        $(this).parents('.header-ct').children('.menu-top').toggleClass('active')
    })
    $('.bg-clmn').click(function(){
        $(this).parents('.header-ct').children('.touch-onmb').removeClass('active')
        $(this).parents('.header-ct').children('.bg-clmn').removeClass('active')
        $(this).parents('.header-ct').children('.menu-top').removeClass('active')
    })
    if ($(window).width() > 992) {
    $('.menu-top .see-1').hover(function(){
        $(this).children('.sub-menu1').slideToggle(200)
    })
    $('.menu-top .see-2').hover(function(){
        $(this).children('.sub-menu2').toggleClass('active')
    })}
    $('.click-mb1').click(function(){
        $(this).parents('.see-1').children('.sub-menu1').slideToggle()
        $(this).parents('.see-1').children('.click-mb1').toggleClass('active')
    })
    $('.click-mb2').click(function(){
        $(this).parents('.see-2').children('.sub-menu2').toggleClass('active')
        $(this).parents('.see-2').children('.click-mb2').toggleClass('active')
    })
    $('.bt-rdtb, ._wrRe').click(function(){
        $('.popup-form').show()
    })
    $('.bg-close, .btclose').click(function(){
        $('.popup-form').hide()
    })
})