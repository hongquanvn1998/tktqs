--
-- PostgreSQL database dump
--

-- Dumped from database version 10.12
-- Dumped by pg_dump version 10.12

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Article_comment; Type: TABLE; Schema: public; Owner: ckcvnvtk
--

CREATE TABLE public."Article_comment" (
    id integer NOT NULL,
    contents character varying(200) NOT NULL,
    date timestamp with time zone NOT NULL,
    article_id integer NOT NULL,
    author_id integer NOT NULL
);


ALTER TABLE public."Article_comment" OWNER TO ckcvnvtk;

--
-- Name: Article_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvnvtk
--

CREATE SEQUENCE public."Article_comment_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Article_comment_id_seq" OWNER TO ckcvnvtk;

--
-- Name: Article_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvnvtk
--

ALTER SEQUENCE public."Article_comment_id_seq" OWNED BY public."Article_comment".id;


--
-- Name: Article_post; Type: TABLE; Schema: public; Owner: ckcvn
--

CREATE TABLE public."Article_post" (
    id integer NOT NULL,
    title character varying(500) NOT NULL,
    content text,
    created_date timestamp with time zone NOT NULL,
    image character varying(100) NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public."Article_post" OWNER TO ckcvn;

--
-- Name: Article_post_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvn
--

CREATE SEQUENCE public."Article_post_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Article_post_id_seq" OWNER TO ckcvn;

--
-- Name: Article_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvn
--

ALTER SEQUENCE public."Article_post_id_seq" OWNED BY public."Article_post".id;


--
-- Name: Product_product; Type: TABLE; Schema: public; Owner: ckcvn
--

CREATE TABLE public."Product_product" (
    id integer NOT NULL,
    title character varying(500) NOT NULL,
    image character varying(100) NOT NULL,
    content text,
    code character varying(100),
    created_date timestamp with time zone,
    updated_at timestamp with time zone NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public."Product_product" OWNER TO ckcvn;

--
-- Name: Product_product_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvn
--

CREATE SEQUENCE public."Product_product_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Product_product_id_seq" OWNER TO ckcvn;

--
-- Name: Product_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvn
--

ALTER SEQUENCE public."Product_product_id_seq" OWNED BY public."Product_product".id;


--
-- Name: addslide; Type: TABLE; Schema: public; Owner: ckcvn
--

CREATE TABLE public.addslide (
    id integer NOT NULL,
    name character varying(100),
    link character varying(500),
    content1 character varying(500),
    content2 character varying(500),
    image character varying(100) NOT NULL,
    active boolean NOT NULL,
    slide_id integer NOT NULL
);


ALTER TABLE public.addslide OWNER TO ckcvn;

--
-- Name: addslide_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvn
--

CREATE SEQUENCE public.addslide_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.addslide_id_seq OWNER TO ckcvn;

--
-- Name: addslide_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvn
--

ALTER SEQUENCE public.addslide_id_seq OWNED BY public.addslide.id;


--
-- Name: article_category; Type: TABLE; Schema: public; Owner: ckcvn
--

CREATE TABLE public.article_category (
    id integer NOT NULL,
    image character varying(100),
    slug character varying(500) NOT NULL,
    title character varying(255) NOT NULL,
    descriptions character varying(100),
    created_date timestamp with time zone NOT NULL,
    updated_date timestamp with time zone,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    parent_id integer,
    CONSTRAINT article_category_level_check CHECK ((level >= 0)),
    CONSTRAINT article_category_lft_check CHECK ((lft >= 0)),
    CONSTRAINT article_category_rght_check CHECK ((rght >= 0)),
    CONSTRAINT article_category_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.article_category OWNER TO ckcvn;

--
-- Name: article_category_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvn
--

CREATE SEQUENCE public.article_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.article_category_id_seq OWNER TO ckcvn;

--
-- Name: article_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvn
--

ALTER SEQUENCE public.article_category_id_seq OWNED BY public.article_category.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: ckcvnvtk
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO ckcvnvtk;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvnvtk
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO ckcvnvtk;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvnvtk
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: ckcvnvtk
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO ckcvnvtk;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvnvtk
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO ckcvnvtk;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvnvtk
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: ckcvnvtk
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO ckcvnvtk;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvnvtk
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO ckcvnvtk;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvnvtk
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: ckcvnvtk
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO ckcvnvtk;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: ckcvnvtk
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO ckcvnvtk;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvnvtk
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO ckcvnvtk;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvnvtk
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvnvtk
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO ckcvnvtk;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvnvtk
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: ckcvnvtk
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO ckcvnvtk;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvnvtk
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO ckcvnvtk;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvnvtk
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: contact; Type: TABLE; Schema: public; Owner: ckcvnvtk
--

CREATE TABLE public.contact (
    id integer NOT NULL,
    full_name character varying(500) NOT NULL,
    phone character varying(11) NOT NULL,
    email character varying(250) NOT NULL,
    address character varying(500) NOT NULL,
    note character varying(500) NOT NULL,
    created_date timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.contact OWNER TO ckcvnvtk;

--
-- Name: contact_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvnvtk
--

CREATE SEQUENCE public.contact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contact_id_seq OWNER TO ckcvnvtk;

--
-- Name: contact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvnvtk
--

ALTER SEQUENCE public.contact_id_seq OWNED BY public.contact.id;


--
-- Name: create_group_slide; Type: TABLE; Schema: public; Owner: ckcvn
--

CREATE TABLE public.create_group_slide (
    id integer NOT NULL,
    name character varying(100),
    active boolean NOT NULL
);


ALTER TABLE public.create_group_slide OWNER TO ckcvn;

--
-- Name: create_group_slide_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvn
--

CREATE SEQUENCE public.create_group_slide_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.create_group_slide_id_seq OWNER TO ckcvn;

--
-- Name: create_group_slide_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvn
--

ALTER SEQUENCE public.create_group_slide_id_seq OWNED BY public.create_group_slide.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: ckcvnvtk
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO ckcvnvtk;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvnvtk
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO ckcvnvtk;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvnvtk
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: ckcvnvtk
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO ckcvnvtk;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvnvtk
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO ckcvnvtk;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvnvtk
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: ckcvnvtk
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO ckcvnvtk;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvnvtk
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO ckcvnvtk;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvnvtk
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: ckcvnvtk
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO ckcvnvtk;

--
-- Name: product_category; Type: TABLE; Schema: public; Owner: ckcvn
--

CREATE TABLE public.product_category (
    id integer NOT NULL,
    image character varying(100),
    slug character varying(500) NOT NULL,
    title character varying(255) NOT NULL,
    descriptions character varying(100),
    created_date timestamp with time zone,
    updated_date timestamp with time zone,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    parent_id integer,
    CONSTRAINT product_category_level_check CHECK ((level >= 0)),
    CONSTRAINT product_category_lft_check CHECK ((lft >= 0)),
    CONSTRAINT product_category_rght_check CHECK ((rght >= 0)),
    CONSTRAINT product_category_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.product_category OWNER TO ckcvn;

--
-- Name: product_category_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvn
--

CREATE SEQUENCE public.product_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_category_id_seq OWNER TO ckcvn;

--
-- Name: product_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvn
--

ALTER SEQUENCE public.product_category_id_seq OWNED BY public.product_category.id;


--
-- Name: products_feature; Type: TABLE; Schema: public; Owner: ckcvnvtk
--

CREATE TABLE public.products_feature (
    id integer NOT NULL,
    name character varying(255),
    unit character varying(100),
    value double precision,
    product_id integer NOT NULL
);


ALTER TABLE public.products_feature OWNER TO ckcvnvtk;

--
-- Name: products_feature_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvnvtk
--

CREATE SEQUENCE public.products_feature_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_feature_id_seq OWNER TO ckcvnvtk;

--
-- Name: products_feature_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvnvtk
--

ALTER SEQUENCE public.products_feature_id_seq OWNED BY public.products_feature.id;


--
-- Name: products_images; Type: TABLE; Schema: public; Owner: ckcvnvtk
--

CREATE TABLE public.products_images (
    id integer NOT NULL,
    image character varying(100),
    post_id integer NOT NULL
);


ALTER TABLE public.products_images OWNER TO ckcvnvtk;

--
-- Name: products_images_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvnvtk
--

CREATE SEQUENCE public.products_images_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_images_id_seq OWNER TO ckcvnvtk;

--
-- Name: products_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvnvtk
--

ALTER SEQUENCE public.products_images_id_seq OWNED BY public.products_images.id;


--
-- Name: Article_comment id; Type: DEFAULT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public."Article_comment" ALTER COLUMN id SET DEFAULT nextval('public."Article_comment_id_seq"'::regclass);


--
-- Name: Article_post id; Type: DEFAULT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public."Article_post" ALTER COLUMN id SET DEFAULT nextval('public."Article_post_id_seq"'::regclass);


--
-- Name: Product_product id; Type: DEFAULT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public."Product_product" ALTER COLUMN id SET DEFAULT nextval('public."Product_product_id_seq"'::regclass);


--
-- Name: addslide id; Type: DEFAULT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.addslide ALTER COLUMN id SET DEFAULT nextval('public.addslide_id_seq'::regclass);


--
-- Name: article_category id; Type: DEFAULT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.article_category ALTER COLUMN id SET DEFAULT nextval('public.article_category_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: contact id; Type: DEFAULT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.contact ALTER COLUMN id SET DEFAULT nextval('public.contact_id_seq'::regclass);


--
-- Name: create_group_slide id; Type: DEFAULT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.create_group_slide ALTER COLUMN id SET DEFAULT nextval('public.create_group_slide_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: product_category id; Type: DEFAULT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.product_category ALTER COLUMN id SET DEFAULT nextval('public.product_category_id_seq'::regclass);


--
-- Name: products_feature id; Type: DEFAULT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.products_feature ALTER COLUMN id SET DEFAULT nextval('public.products_feature_id_seq'::regclass);


--
-- Name: products_images id; Type: DEFAULT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.products_images ALTER COLUMN id SET DEFAULT nextval('public.products_images_id_seq'::regclass);


--
-- Data for Name: Article_comment; Type: TABLE DATA; Schema: public; Owner: ckcvnvtk
--

COPY public."Article_comment" (id, contents, date, article_id, author_id) FROM stdin;
\.


--
-- Data for Name: Article_post; Type: TABLE DATA; Schema: public; Owner: ckcvn
--

COPY public."Article_post" (id, title, content, created_date, image, category_id) FROM stdin;
1	Chức năng - Nhiệm vụ	<div style="text-align: center;"><span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif">CHỨC NĂNG</span></span></b></span></span></span></div>\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Viện Thiết kế t&agrave;u qu&acirc;n sự trực thuộc Tổng cục C&ocirc;ng nghiệp quốc ph&ograve;ng; l&agrave; cơ sở nghi&ecirc;n cứu, thiết kế t&agrave;u ng&agrave;nh trong lĩnh vực đ&oacute;ng t&agrave;u qu&acirc;n sự.</span></span></span></span></span>\r\n\r\n<div style="text-align: center;"><span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif">NHIỆM VỤ</span></span></b></span></span></span></div>\r\n\r\n<ul>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Thiết kế đ&oacute;ng mới (đ&oacute;ng mới v&agrave; ho&aacute;n cải), nghi&ecirc;n cứu lắp đặt, hiệu chỉnh v&agrave; t&iacute;ch hợp hệ thống vũ kh&iacute;, kh&iacute; t&agrave;i tr&ecirc;n t&agrave;u. Tham gia thiết kế đ&oacute;ng mới t&agrave;u, phương tiện đường thủy phục vụ nhu cầu d&acirc;n sinh, xuất khẩu.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Nghi&ecirc;n cứu, tư vấn gi&uacute;p Tổng cục C&ocirc;ng nghiện quốc ph&ograve;ng đề xuất với Bộ Quốc ph&ograve;ng về định hướng quy hoạch ph&aacute;t triển ng&agrave;nh đ&oacute;ng t&agrave;u qu&acirc;n sự v&agrave; định hướng ph&aacute;t triển ng&agrave;nh đ&oacute;ng t&agrave;u qu&acirc;n sự v&agrave; định hướng ph&aacute;t triển t&agrave;u qu&acirc;n sự, vũ kh&iacute;, trang bị kỹ thuật tr&ecirc;n t&agrave;u; nghi&ecirc;n cứu, đề xuất Tổng cục định hướng ph&aacute;t triển, n&acirc;ng cao tiềm lực c&aacute;c nh&agrave; m&aacute;y đ&oacute;ng mới, sửa chữa t&agrave;u qu&acirc;n sự.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Quản l&yacute; lưu trữ t&agrave;u liệu thiết kế c&aacute;c t&agrave;u qu&acirc;n sự. X&acirc;y dựng, ban h&agrave;nh v&agrave; tr&igrave;nh c&aacute;c cấp c&oacute; thẩm quyền ban h&agrave;nh c&aacute;c ti&ecirc;u chuẩn ng&agrave;nh đ&oacute;ng t&agrave;u qu&acirc;n sự.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Tư vấn v&agrave; tham gia thẩm định c&aacute;c dự &aacute;n đầu tư đ&oacute;ng mới, sửa chữa t&agrave;u qu&acirc;n sự.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Gi&aacute;m s&aacute;t thiết kế trong qu&aacute; tr&igrave;nh thi c&ocirc;ng v&agrave; tham gia nghiệm thu đối với c&aacute;c sản phẩm t&agrave;u do Viện thiết kế; gi&aacute;m s&aacute;t thi c&ocirc;ng đ&oacute;ng mới, sửa chữa t&agrave;u theo y&ecirc;u cầu nhiệm vụ.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">X&acirc;y dựng v&agrave; ph&aacute;t triển tiềm lực khoa học c&ocirc;ng nghệ của Viện.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Tham gia đ&agrave;o tạo nguồn nh&acirc;n lực cho ng&agrave;nh đ&oacute;ng t&agrave;u qu&acirc;n sự ph&acirc;n c&ocirc;ng của Nh&agrave; nước , Bộ Quốc ph&ograve;ng v&agrave; Tổng cục C&ocirc;ng nghiệp quốc ph&ograve;ng.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Tổ chức hợp t&aacute;c về khoa học c&ocirc;ng nghệ, chuyển giao c&ocirc;ng nghệ với đối t&aacute;c trong, ngo&agrave;i nước thuộc lĩnh vực&nbsp;đ&oacute;ng t&agrave;u qu&acirc;n sự theo quy định của ph&aacute;p luật v&agrave; của Bộ Quốc ph&ograve;ng.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Hoạt động kinh tế, dịch vụ khoa học c&ocirc;ng nghệ trong lĩnh vực đ&oacute;ng t&agrave;u v&agrave; thực hiện c&aacute;c nhiệm vụ kh&aacute;c do cấp tr&ecirc;n giao.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Kiểm định phương tiện đo, thử nghiệm vũ kh&iacute; trang bị.</span></span></span></span></span></span></li>\r\n</ul>	2020-07-01 22:58:14.068343+07	Chức_năng_-_nhiệm_vụ_21lbNNj.jpg	11
2	Đội ngũ cán bộ	<span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Đội ngũ c&aacute;n bộ, kỹ sư của Viện tr&ecirc;n 70 người, trong đ&oacute; c&oacute; tr&ecirc;n 70% c&oacute; tr&igrave;nh độ đại học trở l&ecirc;n. Phần lớn c&aacute;n bộ nghi&ecirc;n cứu của Viện được đ&agrave;o tạo cơ bản v&agrave; chuy&ecirc;n s&acirc;u trong c&aacute;c lĩnh vực đ&oacute;ng t&agrave;u, cơ kh&iacute; động lực, vũ kh&iacute; - kh&iacute; t&agrave;i c&aacute;c trường đại học của Nga, Ba Lan, Ucraina, Belarus, Australia, Đại học H&agrave;ng hải Việt Nam, Học viện kỹ thuật qu&acirc;n sự, Đại học b&aacute;ch khoa H&agrave; Nội.</span></span>\r\n<div>&nbsp;</div>\r\n<img height="353" src="/media/uploads/2020/07/01/nhan-luc.jpg" width="522" /><br />\r\n<span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Nhiều&nbsp;c&aacute;n bộ từng tham gia nhiều dự &aacute;n thiết kế t&agrave;u cho qu&acirc;n đội v&agrave; d&acirc;n sự; gi&aacute;m s&aacute;t thi c&ocirc;ng đ&oacute;ng mới, sửa chữa t&agrave;u; c&aacute;c dự &aacute;n chuyển giao c&ocirc;ng nghệ đ&oacute;ng t&agrave;u.<br />\r\n<br />\r\nNhiều c&aacute;n bộ của Viện từng c&ocirc;ng t&aacute;c nhiều năm tại c&aacute;c đơn vị thiết kế, đ&oacute;ng t&agrave;u trong v&agrave; ngo&agrave;i Qu&acirc;n đội tr&ecirc;n c&aacute;c cương vị chuy&ecirc;n gia lập dự &aacute;n, thiết kế gi&aacute;m s&aacute;t thi c&ocirc;ng đ&oacute;ng mới, sửa chữa c&aacute;c loại t&agrave;u, thuyền kh&aacute;c nhau.</span></span><br />\r\n&nbsp;	2020-07-01 23:01:56.478413+07	nhân_lực_0PNHpcz.jpg	13
3	Cơ sở vật chất - Hạ tầng	<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><b><i><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Trụ sở l&agrave;m việc của Viện được x&acirc;y dựng tr&ecirc;n khu&ocirc;n vi&ecirc;n rộng 1,65 ha tại Dương X&aacute;, Gia L&acirc;m, H&agrave; Nội gồm 01 nh&agrave; l&agrave;m việc trung t&acirc;m 9 tầng, 01 nh&agrave; đa năng 5 tầng, 01 nh&agrave; xưởng được thiết kế hiện đại phục vụ c&ocirc;ng t&aacute;c nghi&ecirc;n cứu, thiết kế, chế thử.</span></span></i></b></span></span></span><br />\r\n<br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Hệ thống m&aacute;y t&iacute;nh chuy&ecirc;n dụng được lắp đặt đồng bộ phục vụ c&ocirc;ng t&aacute;c thiết kế t&agrave;u tr&ecirc;n c&aacute;c phần mềm như AVEVA Marine, FINE Marine, ... bao gồm: 02 m&aacute;y chủ (Server), 32 m&aacute;y trạm l&agrave;m việc (WordStation), 03 m&aacute;y trạm l&agrave;m việc di động (Mobile WorkStation) v&agrave; hơn 60 bộ m&aacute;y t&iacute;nh. Trao đổi dữ liệu thiết kế được thực hiện đơn giản, nhanh ch&oacute;ng th&ocirc;ng qua hệ thống mạng LAN.</span></span></span></span></span>\r\n<div style="text-align: center;"><img height="188" src="/media/uploads/2020/07/01/image.png" width="336" /></div>\r\n<br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Hệ thống ph&ograve;ng hội thảo quốc tế hiện đại</span></span></span></span></span>\r\n\r\n<div style="text-align: center;"><img height="198" src="/media/uploads/2020/07/01/image_s5B22a4.png" width="299" /><br />\r\n<img height="199" src="/media/uploads/2020/07/01/image_8VDE9lq.png" width="298" /></div>\r\n<br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">C&aacute;c m&aacute;y in, m&aacute;y scan hiện đại từ khổ A0-A4 đảm bảo việc in ấn bản vẽ, t&agrave;i liệu thiết kế nhanh ch&oacute;ng, chất lượng.</span></span></span></span></span><br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Hệ thống&nbsp;thư viện điện tử vớ i trên 1000 đầu&nbsp;sách kỹ thuật chuyên ngành. Cơ sở dữ liệu về&nbsp;các mẫu tàu phong phú , đa dạng và cập nhập nhật liên tục.</span></span><br />\r\n<br />\r\n<b><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Thiết bị gia c&ocirc;ng cơ kh&iacute;</span></span></b></span></span></span><br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Viện c&oacute; 01 xưởng gia c&ocirc;ng cơ kh&iacute; c&oacute; diện t&iacute;ch 916 m2 với hệ thống m&aacute;y gia c&ocirc;ng hiện đại phục vụ nghi&ecirc;n cứu, chế thử c&aacute;c sản phẩm đề t&agrave;i NCKH v&agrave; tham gia dịch vụ KHCN:</span></span></span></span></span>\r\n\r\n<div style="text-align: center;"><img height="534" src="/media/uploads/2020/07/01/image_HcIaDlS.png" width="680" /></div>\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><v:rect alt="/media/uploads/2020/06/09/6_70wTkCA.jpg" filled="f" id="Rectangle_x0020_10" o:gfxdata="UEsDBBQABgAIAAAAIQC75UiUBQEAAB4CAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbKSRvU7DMBSF\r\ndyTewfKKEqcMCKEmHfgZgaE8wMW+SSwc27JvS/v23KTJgkoXFsu+P+c7Ol5vDoMTe0zZBl/LVVlJ\r\ngV4HY31Xy4/tS3EvRSbwBlzwWMsjZrlprq/W22PELHjb51r2RPFBqax7HCCXIaLnThvSAMTP1KkI\r\n+gs6VLdVdad08ISeCho1ZLN+whZ2jsTzgcsnJwldluLxNDiyagkxOquB2Knae/OLUsyEkjenmdzb\r\nmG/YhlRnCWPnb8C898bRJGtQvEOiVxjYhtLOxs8AySiT4JuDystlVV4WPeM6tK3VaILeDZxIOSsu\r\nti/jidNGNZ3/J08yC1dNv9v8AAAA//8DAFBLAwQUAAYACAAAACEArTA/8cEAAAAyAQAACwAAAF9y\r\nZWxzLy5yZWxzhI/NCsIwEITvgu8Q9m7TehCRpr2I4FX0AdZk2wbbJGTj39ubi6AgeJtl2G9m6vYx\r\njeJGka13CqqiBEFOe2Ndr+B03C3WIDihMzh6RwqexNA281l9oBFTfuLBBhaZ4ljBkFLYSMl6oAm5\r\n8IFcdjofJ0z5jL0MqC/Yk1yW5UrGTwY0X0yxNwri3lQgjs+Qk/+zfddZTVuvrxO59CNCmoj3vCwj\r\nMfaUFOjRhrPHaN4Wv0VV5OYgm1p+LW1eAAAA//8DAFBLAwQUAAYACAAAACEAdd2zxs0DAACNCAAA\r\nHwAAAGNsaXBib2FyZC9kcmF3aW5ncy9kcmF3aW5nMS54bWy0Vttu4zYQfS/QfyD4rohy5IuMVRaO\r\nHS0KpLtBnH0uaIqy2KVIlaRvW/Rf+i37ZR1ScnxJ0YddVAFockgenjkzQ+bd+30j0ZYbK7TKcXJD\r\nMOKK6VKodY4/vxTRBCPrqCqp1Irn+MAtfn/380/v6HRtaFsLhgBB2SnNce1cO41jy2reUHujW65g\r\nrtKmoQ6GZh2Xhu4AuZHxgJBR3FCh8N0JakEdRRsjvgNKavaFl3OqttQCpGTTc0vPUbIfR6ZTtf1g\r\n2mX7ZDxz9nH7ZJAocwzKKdqARDjuJ/plMIyvdq1PAPvKNH69riq0DygH3wYMvneIgfGWpBMC+Aym\r\n+n53Rv3pX3ax+uE/9wGZ7lDonBGxraehtm89S15de+YMcmEtOfK2kltmchw3vBQ03rRS09JCZAck\r\nhuCSLB79Nia7ly/z2c3v7fpVluMBtn2EoFmk9LwGUD6zLcBDEoKQR5MxeldzgPXmTkhQvEMIop7A\r\nIAyr3a+6hAjQjdMhr75f3FeR6LQ11n3gukG+k2MDJAM43T5a13E6LgkK6kJIGeIj1YUBMDsLxBW2\r\n+jkf4ZDwf2Yke5g8TNIoHYweopQsFtGsmKfRqEjGw8XtYj5fJH/5c5N0Wouy5Mofcyy+JH2T2Y1g\r\nRltduRummxjSSzB+LEAIUkJO5We1FKWH85SsWa/m0qAtlTkuwtcrf7YsvqQRMhx8uXIpGaTkfpBF\r\nxWgyjtIiHUbZmEwikmT32YikWbooLl16FIr/uEtol+NsOBiGKJ2RvvKNhO+tb3TaCMcNkqLJMdQd\r\nfF3u+kR8UGUIraNCdv0zKTz9kxQQ7mOgoWv7C8Ptl6HQ3P5elwcv2Ap+IXmNhuSCqoLLGDq1Nl8x\r\n2sEVm2P7x4YajpH8RUEdZEmawjIXBulwPICBOZ9Znc9QxQAqxw6jrjt3MIItm9aIdQ0nJUEmpWdQ\r\nNJXoE7rj5NlJ65buIHnwOjAHR1BDzWOAgc5z6FC5hgeEORPgpFq2LGRTy56Y65IpIeNey1AJpxX3\r\nvDqudbZbGyQPsrHT7KwKGWYB87jO34tdDYI1zMOmXmrjQQ2wlXC95Jir6PMSXrOv4HMC2/wsryoo\r\n566OwVfqhELu0PKKMrhH5lSKlREYtVRpCwa42goyhNb/peTWtzArHKsL2gjp72cwsJoay0M4g2yc\r\n/g+gzJ6BvoiGW/SR79Czbqi6YDwgI2A6BL6eObwfV4wTeOIvGYOEIJvXx919+9u/XqC7N4YWksBb\r\nXvN3Y/my9S9DF4cuwWGFf1Diq4c4bO3/cfCv/fn47h8AAAD//wMAUEsDBBQABgAIAAAAIQCRLWpJ\r\nWAYAAA8aAAAaAAAAY2xpcGJvYXJkL3RoZW1lL3RoZW1lMS54bWzsWUtvGzcQvhfof1jsvbHeio3I\r\nga1H3MROgkhJkSOlpXYZc5cLkrKjW5GceilQIC16aIDeeiiKBmiABr30xxhw0KY/okPuQ6RExQ+4\r\nQFDEAozd2W+Gw5nZb0jujZtPY+odYS4ISzp+9VrF93AyYQFJwo7/cDT47LrvCYmSAFGW4I4/x8K/\r\nuf3pJzfQ1oSSdMwQD0YRjrEHhhKxhTp+JGW6tbEhJiBG4hpLcQLPpozHSMItDzcCjo5hgJhu1CqV\r\n1kaMSOJvg0WpDPUp/EukUIIJ5UNlBnsJimH0e9MpmWCNDQ6rCiHmoku5d4RoxwebATse4afS9ygS\r\nEh50/Ir+8ze2b2ygrVyJyjW6ht5A/+V6uUJwWNNj8nBcDtpoNButndK+BlC5iuu3+61+q7SnAWgy\r\ngZlmvpg2m7ubu71mjjVA2aXDdq/dq1ctvGG/vuLzTlP9LLwGZfYbK/jBoAtRtPAalOGbK/hGo13r\r\nNiy8BmX41gq+XdnpNdoWXoMiSpLDFXSl2ap3i9mWkCmje074ZrMxaNdy4wsUVENZXWqIKUvkulqL\r\n0RPGBwBQQIokSTw5T/EUTaAmu4iSMSfePgkjKLwUJUyAuFKrDCp1+K9+DX2lI4K2MDK0lV/giVgR\r\nKX88MeEklR3/Nlj1Dcjpmzcnz16fPPv95Pnzk2e/5mNrU5beHkpCU+/dT9/88/JL7+/ffnz34tts\r\n6GW8MPFvf/nq7R9/vs88zHgRitPvXr19/er0+6//+vmFw/oOR2MTPiIxFt5dfOw9YDFM0OE/HvOL\r\naYwiREyNnSQUKEFqFIf9vows9N05osiB28V2HB9xoBoX8NbsieXwMOIzSRwW70SxBTxgjO4y7ozC\r\nHTWWEebRLAndg/OZiXuA0JFr7C5KrCz3ZylwLHGZ7EbYcvM+RYlEIU6w9NQzdoixY3aPCbHiekAm\r\nnAk2ld5j4u0i4gzJiIytaloo7ZEY8jJ3OQj5tmJz8MjbZdQ16x4+spHwbiDqcH6EqRXGW2gmUewy\r\nOUIxNQO+j2TkcnI45xMT1xcSMh1iyrx+gIVw6dzjMF8j6XeAZtxpP6Dz2EZySQ5dNvcRYyayxw67\r\nEYpTF3ZIksjEfi4OoUSRd59JF/yA2W+Iuoc8oGRtuh8RbKX7bDZ4CAxrurQoEPVkxh25vIWZVb/D\r\nOZ0irKkGGoDF6zFJziT5JXpv/nf0DiR6+sNLx4yuhtLdhq18XJDMdzhxvk17SxS+DrdM3F3GA/Lh\r\n83YPzZL7GF6V1eb1kbY/0rb/v6ftde/z1ZP1gp+ButWyNVuu68V7vHbtPiWUDuWc4n2hl+8CulIw\r\nAKHS03tUXO7l0ggu1ZsMA1i4kCOt43EmvyAyGkYohTV+1VdGQpGbDoWXMgFLfy122lZ4OosPWJBt\r\nWatVtT3NyEMguZBXmqUcthsyQ7fai21YaV57G+rtcuGA0r2IE8ZgthN1hxPtQqiCpDfnEDSHE3pm\r\nV+LFpsOL68p8kaoVL8C1MiuwbPJgsdXxmw1QASXYVSGKA5WnLNVFdnUyrzLT64JpVQCsIYoKWGR6\r\nU/m6dnpqdlmpnSPTlhNGudlO6MjoHiYiFOC8OpX0PG5cNNebi5Ra7qlQ6PGgtBZutK+/z4vL5hr0\r\nlrmBJiZT0MQ77vitehNKZoLSjj+FrT9cxinUjlDLXURDODSbSJ698JdhlpQL2UMiygKuSSdjg5hI\r\nzD1K4o6vpl+mgSaaQ7Rv1RoQwgfr3CbQyofmHCTdTjKeTvFEmmk3JCrS2S0wfMYVzqda/fJgpclm\r\nkO5hFBx7YzrjDxCUWLNdVQEMiIAToGoWzYDAkWZJZIv6W2pMOe2aZ4q6hjI5ommE8o5iknkG11Re\r\nuqPvyhgYd/mcIaBGSPJGOA5VgzWDanXTsmtkPqztumcrqcgZpLnomRarqK7pZjFrhKINLMXyck3e\r\n8KoIMXCa2eEz6l6m3M2C65bWCWWXgICX8XN03XM0BMO1xWCWa8rjVRpWnJ1L7d5RTPAM187TJAzW\r\nbxVml+JW9gjncCC8VOcHveWqBdG0WFfqSLs+Txyg1BuH1Y4PnwjgbOIpXMFHBh9kNSWrKRlcwZcD\r\naBfZcX/Hzy8KCTzPJCWmXkjqBaZRSBqFpFlImoWkVUhavqfPxeFbjDoS973i2Bt6WH5Mnq8t7G84\r\n2/8CAAD//wMAUEsDBBQABgAIAAAAIQCcZkZBuwAAACQBAAAqAAAAY2xpcGJvYXJkL2RyYXdpbmdz\r\nL19yZWxzL2RyYXdpbmcxLnhtbC5yZWxzhI/NCsIwEITvgu8Q9m7SehCRJr2I0KvUBwjJNi02PyRR\r\n7Nsb6EVB8LIws+w3s037sjN5YkyTdxxqWgFBp7yenOFw6y+7I5CUpdNy9g45LJigFdtNc8VZ5nKU\r\nxikkUigucRhzDifGkhrRykR9QFc2g49W5iKjYUGquzTI9lV1YPGTAeKLSTrNIXa6BtIvoST/Z/th\r\nmBSevXpYdPlHBMulFxagjAYzB0pXZ501LV2BiYZ9/SbeAAAA//8DAFBLAQItABQABgAIAAAAIQC7\r\n5UiUBQEAAB4CAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAG\r\nAAgAAAAhAK0wP/HBAAAAMgEAAAsAAAAAAAAAAAAAAAAANgEAAF9yZWxzLy5yZWxzUEsBAi0AFAAG\r\nAAgAAAAhAHXds8bNAwAAjQgAAB8AAAAAAAAAAAAAAAAAIAIAAGNsaXBib2FyZC9kcmF3aW5ncy9k\r\ncmF3aW5nMS54bWxQSwECLQAUAAYACAAAACEAkS1qSVgGAAAPGgAAGgAAAAAAAAAAAAAAAAAqBgAA\r\nY2xpcGJvYXJkL3RoZW1lL3RoZW1lMS54bWxQSwECLQAUAAYACAAAACEAnGZGQbsAAAAkAQAAKgAA\r\nAAAAAAAAAAAAAAC6DAAAY2xpcGJvYXJkL2RyYXdpbmdzL19yZWxzL2RyYXdpbmcxLnhtbC5yZWxz\r\nUEsFBgAAAAAFAAUAZwEAAL0NAAAAAA==\r\n" stroked="f" style="width:24pt; height:24pt; v-text-anchor:top"> <o:lock aspectratio="t" v:ext="edit"> <v:textbox>\r\n<table width="100%">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<div>&nbsp;</div>\r\n\r\n\t\t\t<div style="text-align: center;">&nbsp;</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</v:textbox></o:lock></v:rect></span></span></span><img height="188" src="/media/uploads/2020/07/01/image_q8lBbKA.png" width="284" />&nbsp;<img height="189" src="/media/uploads/2020/07/01/image_ejCJ0bl.png" width="287" /><span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><v:rect alt="/media/uploads/2020/06/09/6_70wTkCA.jpg" filled="f" o:gfxdata="UEsDBBQABgAIAAAAIQC75UiUBQEAAB4CAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbKSRvU7DMBSF\r\ndyTewfKKEqcMCKEmHfgZgaE8wMW+SSwc27JvS/v23KTJgkoXFsu+P+c7Ol5vDoMTe0zZBl/LVVlJ\r\ngV4HY31Xy4/tS3EvRSbwBlzwWMsjZrlprq/W22PELHjb51r2RPFBqax7HCCXIaLnThvSAMTP1KkI\r\n+gs6VLdVdad08ISeCho1ZLN+whZ2jsTzgcsnJwldluLxNDiyagkxOquB2Knae/OLUsyEkjenmdzb\r\nmG/YhlRnCWPnb8C898bRJGtQvEOiVxjYhtLOxs8AySiT4JuDystlVV4WPeM6tK3VaILeDZxIOSsu\r\nti/jidNGNZ3/J08yC1dNv9v8AAAA//8DAFBLAwQUAAYACAAAACEArTA/8cEAAAAyAQAACwAAAF9y\r\nZWxzLy5yZWxzhI/NCsIwEITvgu8Q9m7TehCRpr2I4FX0AdZk2wbbJGTj39ubi6AgeJtl2G9m6vYx\r\njeJGka13CqqiBEFOe2Ndr+B03C3WIDihMzh6RwqexNA281l9oBFTfuLBBhaZ4ljBkFLYSMl6oAm5\r\n8IFcdjofJ0z5jL0MqC/Yk1yW5UrGTwY0X0yxNwri3lQgjs+Qk/+zfddZTVuvrxO59CNCmoj3vCwj\r\nMfaUFOjRhrPHaN4Wv0VV5OYgm1p+LW1eAAAA//8DAFBLAwQUAAYACAAAACEAdd2zxs0DAACNCAAA\r\nHwAAAGNsaXBib2FyZC9kcmF3aW5ncy9kcmF3aW5nMS54bWy0Vttu4zYQfS/QfyD4rohy5IuMVRaO\r\nHS0KpLtBnH0uaIqy2KVIlaRvW/Rf+i37ZR1ScnxJ0YddVAFockgenjkzQ+bd+30j0ZYbK7TKcXJD\r\nMOKK6VKodY4/vxTRBCPrqCqp1Irn+MAtfn/380/v6HRtaFsLhgBB2SnNce1cO41jy2reUHujW65g\r\nrtKmoQ6GZh2Xhu4AuZHxgJBR3FCh8N0JakEdRRsjvgNKavaFl3OqttQCpGTTc0vPUbIfR6ZTtf1g\r\n2mX7ZDxz9nH7ZJAocwzKKdqARDjuJ/plMIyvdq1PAPvKNH69riq0DygH3wYMvneIgfGWpBMC+Aym\r\n+n53Rv3pX3ax+uE/9wGZ7lDonBGxraehtm89S15de+YMcmEtOfK2kltmchw3vBQ03rRS09JCZAck\r\nhuCSLB79Nia7ly/z2c3v7fpVluMBtn2EoFmk9LwGUD6zLcBDEoKQR5MxeldzgPXmTkhQvEMIop7A\r\nIAyr3a+6hAjQjdMhr75f3FeR6LQ11n3gukG+k2MDJAM43T5a13E6LgkK6kJIGeIj1YUBMDsLxBW2\r\n+jkf4ZDwf2Yke5g8TNIoHYweopQsFtGsmKfRqEjGw8XtYj5fJH/5c5N0Wouy5Mofcyy+JH2T2Y1g\r\nRltduRummxjSSzB+LEAIUkJO5We1FKWH85SsWa/m0qAtlTkuwtcrf7YsvqQRMhx8uXIpGaTkfpBF\r\nxWgyjtIiHUbZmEwikmT32YikWbooLl16FIr/uEtol+NsOBiGKJ2RvvKNhO+tb3TaCMcNkqLJMdQd\r\nfF3u+kR8UGUIraNCdv0zKTz9kxQQ7mOgoWv7C8Ptl6HQ3P5elwcv2Ap+IXmNhuSCqoLLGDq1Nl8x\r\n2sEVm2P7x4YajpH8RUEdZEmawjIXBulwPICBOZ9Znc9QxQAqxw6jrjt3MIItm9aIdQ0nJUEmpWdQ\r\nNJXoE7rj5NlJ65buIHnwOjAHR1BDzWOAgc5z6FC5hgeEORPgpFq2LGRTy56Y65IpIeNey1AJpxX3\r\nvDqudbZbGyQPsrHT7KwKGWYB87jO34tdDYI1zMOmXmrjQQ2wlXC95Jir6PMSXrOv4HMC2/wsryoo\r\n566OwVfqhELu0PKKMrhH5lSKlREYtVRpCwa42goyhNb/peTWtzArHKsL2gjp72cwsJoay0M4g2yc\r\n/g+gzJ6BvoiGW/SR79Czbqi6YDwgI2A6BL6eObwfV4wTeOIvGYOEIJvXx919+9u/XqC7N4YWksBb\r\nXvN3Y/my9S9DF4cuwWGFf1Diq4c4bO3/cfCv/fn47h8AAAD//wMAUEsDBBQABgAIAAAAIQCRLWpJ\r\nWAYAAA8aAAAaAAAAY2xpcGJvYXJkL3RoZW1lL3RoZW1lMS54bWzsWUtvGzcQvhfof1jsvbHeio3I\r\nga1H3MROgkhJkSOlpXYZc5cLkrKjW5GceilQIC16aIDeeiiKBmiABr30xxhw0KY/okPuQ6RExQ+4\r\nQFDEAozd2W+Gw5nZb0jujZtPY+odYS4ISzp+9VrF93AyYQFJwo7/cDT47LrvCYmSAFGW4I4/x8K/\r\nuf3pJzfQ1oSSdMwQD0YRjrEHhhKxhTp+JGW6tbEhJiBG4hpLcQLPpozHSMItDzcCjo5hgJhu1CqV\r\n1kaMSOJvg0WpDPUp/EukUIIJ5UNlBnsJimH0e9MpmWCNDQ6rCiHmoku5d4RoxwebATse4afS9ygS\r\nEh50/Ir+8ze2b2ygrVyJyjW6ht5A/+V6uUJwWNNj8nBcDtpoNButndK+BlC5iuu3+61+q7SnAWgy\r\ngZlmvpg2m7ubu71mjjVA2aXDdq/dq1ctvGG/vuLzTlP9LLwGZfYbK/jBoAtRtPAalOGbK/hGo13r\r\nNiy8BmX41gq+XdnpNdoWXoMiSpLDFXSl2ap3i9mWkCmje074ZrMxaNdy4wsUVENZXWqIKUvkulqL\r\n0RPGBwBQQIokSTw5T/EUTaAmu4iSMSfePgkjKLwUJUyAuFKrDCp1+K9+DX2lI4K2MDK0lV/giVgR\r\nKX88MeEklR3/Nlj1Dcjpmzcnz16fPPv95Pnzk2e/5mNrU5beHkpCU+/dT9/88/JL7+/ffnz34tts\r\n6GW8MPFvf/nq7R9/vs88zHgRitPvXr19/er0+6//+vmFw/oOR2MTPiIxFt5dfOw9YDFM0OE/HvOL\r\naYwiREyNnSQUKEFqFIf9vows9N05osiB28V2HB9xoBoX8NbsieXwMOIzSRwW70SxBTxgjO4y7ozC\r\nHTWWEebRLAndg/OZiXuA0JFr7C5KrCz3ZylwLHGZ7EbYcvM+RYlEIU6w9NQzdoixY3aPCbHiekAm\r\nnAk2ld5j4u0i4gzJiIytaloo7ZEY8jJ3OQj5tmJz8MjbZdQ16x4+spHwbiDqcH6EqRXGW2gmUewy\r\nOUIxNQO+j2TkcnI45xMT1xcSMh1iyrx+gIVw6dzjMF8j6XeAZtxpP6Dz2EZySQ5dNvcRYyayxw67\r\nEYpTF3ZIksjEfi4OoUSRd59JF/yA2W+Iuoc8oGRtuh8RbKX7bDZ4CAxrurQoEPVkxh25vIWZVb/D\r\nOZ0irKkGGoDF6zFJziT5JXpv/nf0DiR6+sNLx4yuhtLdhq18XJDMdzhxvk17SxS+DrdM3F3GA/Lh\r\n83YPzZL7GF6V1eb1kbY/0rb/v6ftde/z1ZP1gp+ButWyNVuu68V7vHbtPiWUDuWc4n2hl+8CulIw\r\nAKHS03tUXO7l0ggu1ZsMA1i4kCOt43EmvyAyGkYohTV+1VdGQpGbDoWXMgFLfy122lZ4OosPWJBt\r\nWatVtT3NyEMguZBXmqUcthsyQ7fai21YaV57G+rtcuGA0r2IE8ZgthN1hxPtQqiCpDfnEDSHE3pm\r\nV+LFpsOL68p8kaoVL8C1MiuwbPJgsdXxmw1QASXYVSGKA5WnLNVFdnUyrzLT64JpVQCsIYoKWGR6\r\nU/m6dnpqdlmpnSPTlhNGudlO6MjoHiYiFOC8OpX0PG5cNNebi5Ra7qlQ6PGgtBZutK+/z4vL5hr0\r\nlrmBJiZT0MQ77vitehNKZoLSjj+FrT9cxinUjlDLXURDODSbSJ698JdhlpQL2UMiygKuSSdjg5hI\r\nzD1K4o6vpl+mgSaaQ7Rv1RoQwgfr3CbQyofmHCTdTjKeTvFEmmk3JCrS2S0wfMYVzqda/fJgpclm\r\nkO5hFBx7YzrjDxCUWLNdVQEMiIAToGoWzYDAkWZJZIv6W2pMOe2aZ4q6hjI5ommE8o5iknkG11Re\r\nuqPvyhgYd/mcIaBGSPJGOA5VgzWDanXTsmtkPqztumcrqcgZpLnomRarqK7pZjFrhKINLMXyck3e\r\n8KoIMXCa2eEz6l6m3M2C65bWCWWXgICX8XN03XM0BMO1xWCWa8rjVRpWnJ1L7d5RTPAM187TJAzW\r\nbxVml+JW9gjncCC8VOcHveWqBdG0WFfqSLs+Txyg1BuH1Y4PnwjgbOIpXMFHBh9kNSWrKRlcwZcD\r\naBfZcX/Hzy8KCTzPJCWmXkjqBaZRSBqFpFlImoWkVUhavqfPxeFbjDoS973i2Bt6WH5Mnq8t7G84\r\n2/8CAAD//wMAUEsDBBQABgAIAAAAIQCcZkZBuwAAACQBAAAqAAAAY2xpcGJvYXJkL2RyYXdpbmdz\r\nL19yZWxzL2RyYXdpbmcxLnhtbC5yZWxzhI/NCsIwEITvgu8Q9m7SehCRJr2I0KvUBwjJNi02PyRR\r\n7Nsb6EVB8LIws+w3s037sjN5YkyTdxxqWgFBp7yenOFw6y+7I5CUpdNy9g45LJigFdtNc8VZ5nKU\r\nxikkUigucRhzDifGkhrRykR9QFc2g49W5iKjYUGquzTI9lV1YPGTAeKLSTrNIXa6BtIvoST/Z/th\r\nmBSevXpYdPlHBMulFxagjAYzB0pXZ501LV2BiYZ9/SbeAAAA//8DAFBLAQItABQABgAIAAAAIQC7\r\n5UiUBQEAAB4CAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAG\r\nAAgAAAAhAK0wP/HBAAAAMgEAAAsAAAAAAAAAAAAAAAAANgEAAF9yZWxzLy5yZWxzUEsBAi0AFAAG\r\nAAgAAAAhAHXds8bNAwAAjQgAAB8AAAAAAAAAAAAAAAAAIAIAAGNsaXBib2FyZC9kcmF3aW5ncy9k\r\ncmF3aW5nMS54bWxQSwECLQAUAAYACAAAACEAkS1qSVgGAAAPGgAAGgAAAAAAAAAAAAAAAAAqBgAA\r\nY2xpcGJvYXJkL3RoZW1lL3RoZW1lMS54bWxQSwECLQAUAAYACAAAACEAnGZGQbsAAAAkAQAAKgAA\r\nAAAAAAAAAAAAAAC6DAAAY2xpcGJvYXJkL2RyYXdpbmdzL19yZWxzL2RyYXdpbmcxLnhtbC5yZWxz\r\nUEsFBgAAAAAFAAUAZwEAAL0NAAAAAA==\r\n" stroked="f" style="width:24pt; height:24pt; v-text-anchor:top"><o:lock aspectratio="t" v:ext="edit"><v:textbox>\r\n<table width="100%">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<div style="text-align: center;">&nbsp;</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</v:textbox></o:lock></v:rect></span></span></span>\r\n\r\n<div style="text-align: center;">&nbsp;</div>\r\n\r\n<div style="text-align: center;">&nbsp;</div>\r\n\r\n<div style="text-align: center;">&nbsp;</div>\r\n\r\n<div style="text-align: center;">&nbsp;</div>\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Cơ sở đo lường - Thử nghiệm</span></span></b></span></span></span><br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Ng&agrave;y 03 th&aacute;ng 8 năm 2018, Viện Thiết kế t&agrave;u qu&acirc;n sựu được Cục TC-ĐL-CL/BTTM c&ocirc;ng nhận năng lực kiểm định phương tiện đo, thử nghiệm vũ kh&iacute; trang bị (Bao gồm 05 lĩnh vực kiểm định; 11 lĩnh vực thử nghiệm).</span></span></span></span></span><br />\r\n<img height="261" src="/media/uploads/2020/07/01/image_Zx1uFwH.png" width="346" />&nbsp; &nbsp; &nbsp;<img height="257" src="/media/uploads/2020/07/01/image_6RMpCnT.png" width="348" /><br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><b>C&aacute;c thiết bị kiểm tra t&iacute;n hiệu cao tần<i>&nbsp;</i></b></span></span></span></span></span><br />\r\n<img height="409" src="/media/uploads/2020/07/01/image_QQCcNCF.png" width="306" />&nbsp; &nbsp;<img height="399" src="/media/uploads/2020/07/01/image_iISC7UA.png" width="224" /><br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><b>Ph&ograve;ng kiểm định c&aacute;c phương tiện đo điện v&agrave; cơ kh&iacute;&nbsp;</b></span></span></span></span></span>\r\n\r\n<div style="text-align: center;"><img height="849" src="/media/uploads/2020/07/01/image_3fxbv8E.png" width="676" /></div>	2020-07-01 23:10:20.031007+07	cơ_sở_hạ_tầng__BJVSTU7.jpg	14
4	Lịch sử phát triển	<div style="text-align: center;"><img height="379" src="/media/uploads/2020/07/01/image_wznkJBY.png" width="654" /><br />\r\n&nbsp;</div>\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:13.5pt"><span style="font-family:&quot;Myriad-Pro Bold&quot;,serif"><span style="color:#e74c3c">Thực hiện Nghị quyết của Bộ Ch&iacute;nh tri ̣về xây dựng và phát triển công nghiệp quốc phòng để bảo đảm kỹ thuật, công nghệ đồng bộ cho thiết kế, chế tạo các loại tàu quân sự được chủ động, sát với yêu cầu kỹ chiến thuật, điều kiện tác chiến của Quân đội trong sự nghiệp xây dựng bảo vệ Tổ quốc, Bộ trưởng Bộ Quốc phòng đã ký quyết định số 849/QĐ-BQP thành lập Viện Thiết kế tàu quân sự.</span></span></span></b></span></span></span>	2020-07-01 23:14:00.122163+07	lịch_sử_.jpg	10
5	Lãnh đạo viên	<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Viện trưởng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ch&iacute;nh trị Vi&ecirc;n<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{ảnh 3x4} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{ảnh 3x4}<br />\r\nTrung t&aacute; TS.Phạm Quang Chiến&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Thượng t&aacute; Ths. Hồ Văn Ch&acirc;u<br />\r\n<br />\r\n<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ph&oacute; Viện trưởng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ph&oacute; Viện trưởng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ph&oacute; Viện trưởng<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{ảnh 3x4}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{ảnh 3x4}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {ảnh 3x4}<br />\r\nTrung t&aacute; TS. Phạm Th&agrave;nh Trung&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; Đại t&aacute; TS.Lương Lục Quỳnh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Đại t&aacute; Ths. Nguyễn Đ&igrave;nh T&ugrave;ng</span></span></span></span></span><br />\r\n&nbsp;	2020-07-01 23:14:41.85033+07	lãnh_đạo_viện__N0fUzXt.jpg	9
6	AVEVA Initial Design	AVEVA Initial Design là bộ ứng dụng dùng trong giai đoạn thiết kế sơ bộ, bao gồm 5 môđun. Lines xây dựng và chỉnh trơn tuyến h&igrave;nh, Surface tạo bề mặt vỏ tàu, Compartment mô h&igrave;nh hóa khoang két, Hydrostatics, Hydrodynamics t&iacute;nh toán kiểm tra các yếu tố t&iacute;nh năng.	2020-07-01 23:19:40.595899+07	aveva_Jm4Ip1L.png	6
7	AVEVA Hull Structural Design	Hull Structural Design là ứng dụng dùng để xác định sơ bộ và triển khai kết cấu cơ bản của tàu. Nó cho phép xây dựng các bản vẽ tiêu chuẩn, ước lượng sơ bộ vật liệu thân tàu, chiều dài đường hàn, khối lượng tàu và toạ độ trọng tâm tàu	2020-07-01 23:20:45.449469+07	avevahull_D2MauMN.png	6
8	AVEVA Outfitting	Outfiting c&oacute; c&aacute;c m&ocirc;đun d&ugrave;ng để thiết kế, lắp đặt c&aacute;c hệ thống trang thiết bị, ống, HVAC, c&aacute;c kết cấu th&eacute;p đa dạng&nbsp;v&agrave; m&aacute;ng dẫn c&aacute;p điện	2020-07-01 23:22:12.563593+07	avevaout_NUqEK1a.png	6
9	Rhinoceros	Phần mềm cho ph&eacute;p m&ocirc; phỏng nhanh ch&oacute;ng, ch&iacute;nh x&aacute;c h&igrave;nh d&aacute;ng th&acirc;n t&agrave;u, bố tr&iacute; ch&uacute;ng to&agrave;n t&agrave;u trong kh&ocirc;ng gian 3D.&nbsp;M&ocirc; h&igrave;nh 3D giúp người thiết kế kiểm soát được các yếu tố bố tr&iacute; và giúp khách hàng nắm bắt được thiết kế đầy đủ, trực quan hơn.	2020-07-01 23:24:54.892846+07	rhinoceros.png	6
\.


--
-- Data for Name: Product_product; Type: TABLE DATA; Schema: public; Owner: ckcvn
--

COPY public."Product_product" (id, title, image, content, code, created_date, updated_at, category_id) FROM stdin;
2	Tàu Trinh sát 500cv	tàu_trinh_sát_500cv.jpg	Đang cập nhật&nbsp;	500cv	2020-06-07 21:01:52.721362+07	2020-06-07 21:01:52.721403+07	2
3	Tàu trinh sát 40M	tàu_chinh_sát_40m.jpg		40M	2020-06-07 21:26:48.649048+07	2020-06-07 21:26:48.64908+07	2
4	Tàu tuần tra cao tốc 120 tấn	tau_tuần_tra_cao_thốc_120_tấn.jpg		120 tấn	2020-06-07 21:29:25.448689+07	2020-06-07 21:30:15.720721+07	2
5	Tàu tuần tra cao tốc 75 tấn	tàu_tuần_tra_cao_tốc_75_tấn.jpg		75 tấn	2020-06-07 21:32:36.699015+07	2020-06-07 21:32:36.699049+07	2
7	Tàu kéo 1200cv	tàu_kéo_1200cv.jpg		1200cv	2020-06-07 21:38:04.98208+07	2020-06-07 21:38:04.982125+07	2
8	Tàu vận tải đa năng 150 tấn	tàu_vận_tải_đa_năng_150_tấn.jpg		150 tấn	2020-06-07 21:40:14.595718+07	2020-06-07 21:40:14.595756+07	2
9	Xuồng tenner vỏ hợp kim nhôm	xuồng_tender_vỏ_hợp_kim_nhôm.jpg		z114	2020-06-07 21:42:28.673098+07	2020-06-07 21:42:28.673134+07	2
10	Xuồng công tác vỏ hợp kim nhôm	Xuồng_công_tác_vỏ_hợp_kim_nhôm.jpg		z114	2020-06-07 21:44:46.478641+07	2020-06-07 21:44:46.478678+07	2
11	Nhà giàn tự nâng	nhà_giàn_tự_nâng.jpg		z114	2020-06-07 21:47:10.863281+07	2020-06-07 21:47:10.863317+07	3
12	Tàu kéo 1000cv	tàu_kéo_1000_cv.jpg		1000cv	2020-06-07 21:49:24.38117+07	2020-06-07 21:49:24.381208+07	3
13	Tàu trinh sát 24M	tàu_trinh_sát_24_m.jpg		24M	2020-06-07 21:51:15.933484+07	2020-06-07 21:51:15.933521+07	3
14	Tàu trinh sát 29M	tau_trinh_sát_29m.jpg		29M	2020-06-07 21:53:11.863085+07	2020-06-07 21:53:11.863121+07	3
16	Tàu tuần tiễu pháo	tuần_tuần__tiễu_pháo.jpg		z114	2020-06-07 21:57:22.639354+07	2020-06-07 21:57:22.639405+07	3
17	Tàu ngầm 100 tán	tàu_ngầm_100_tấn.jpg		100 tấn	2020-06-07 22:01:00.087806+07	2020-06-07 22:01:00.087856+07	3
18	Tàu 480 khách	tàu_480_khách.jpg		480	2020-06-07 22:03:52.581151+07	2020-06-07 22:03:52.581192+07	3
19	Xuồng cao tốc XVT01	xuồng_cao_tốc_xvt_01.jpg		XVT01	2020-06-07 22:06:05.210291+07	2020-06-07 22:06:05.210326+07	3
15	Tàu tuần tra cao tốc 37 Hải lý/giờ	tàu_tuần_tra_cao_tốc_37_hải_lý_giờ.jpg		37	2020-06-07 21:55:19.27696+07	2020-06-07 22:06:23.408371+07	3
6	Tàu tuần tra cao tốc 130 tấn	tàu_tuần_tra_cao_tốc_130_tấn_.jpg		130 tấn	2020-06-07 21:35:05.099082+07	2020-06-07 22:06:46.104064+07	2
20	Xuồng cứu hộ	xuồng_cứu_hộ.jpg		z114	2020-06-07 22:12:10.762222+07	2020-06-07 22:12:10.76226+07	3
21	Bộ lò xo dạng đĩa côn	bộ_lò_so_dnagj_đĩa_côn.jpg		z114	2020-06-07 22:16:58.998221+07	2020-06-07 22:16:58.998257+07	3
22	Bàn phím Kasu	bàn_phím_kasu.jpg	Chức năng: B&agrave;n ph&iacute;m nhập dữ liệu hệ thống điều khiển KASU (k&yacute; hiệu BP-01) được Viện Thiết kế t&agrave;u qu&acirc;n<br />\r\nsự nghi&ecirc;n cứu, ph&aacute;t triển nhằm thay thế cho b&agrave;n ph&iacute;m КЛ-85 do LB Nga sản xuất đang sử dụng<br />\r\ntr&ecirc;n c&aacute;c t&agrave;u M, Gepard, PS-500. B&agrave;n ph&iacute;m BP-01 c&oacute; h&igrave;nh d&aacute;ng, k&iacute;ch thước v&agrave; số lượng ph&iacute;m<br />\r\ntương đương với b&agrave;n ph&iacute;m КЛ-85, hoạt động ổn định theo đ&uacute;ng chức năng ở c&aacute;c chế độ l&agrave;m<br />\r\nviệc của hệ thống trong c&aacute;c điều kiện khai th&aacute;c sử dụng tr&ecirc;n t&agrave;u, đạt y&ecirc;u cầu kỹ thuật theo ti&ecirc;u<br />\r\nchuẩn qu&acirc;n sự của Bộ Quốc ph&ograve;ng.<br />\r\n&nbsp;	z114	2020-06-07 22:20:57.233423+07	2020-06-07 22:21:23.31945+07	3
23	Máy định vị hàng hải GPS/GLONASS	máy_định_vị_hàng_hải.jpg	Chức năng : Định vị dẫn đường , x&aacute;c định toạ độ t&agrave;u qu&acirc;n sự sử dụng đồng thời hoặc ri&ecirc;ng rẽ hai hệ thống định vị vệ tinh GPS của Mỹ v&agrave; GLONASS của LB Nga .	z114	2020-06-07 22:34:36.197133+07	2020-06-07 22:34:36.19717+07	3
24	Bộ điều khiển hướng dòng cửa hút của động cơ tua bin khí	bộ_điều_khiển_cánh_hướng.jpg	Chức năng: Xoay c&aacute;nh hướng d&ograve;ng đến một g&oacute;c cần thiết theo chế độ c&ocirc;ng suất của động cơ nhằm mục<br />\r\nđ&iacute;ch tạo sự ổn định kh&iacute; động học, n&acirc;ng cao hiệu suất cho buồng đốt v&agrave; giảm ứng suất động cho c&aacute;c<br />\r\nbộ phận của động cơ.<br />\r\n&nbsp;	DR76	2020-06-07 22:39:37.74261+07	2020-06-07 22:39:37.742658+07	3
25	Thiết bị báo nước	thiết_bị_báo_nước.jpg	Chức năng: Kiểm tra, gi&aacute;m s&aacute;t v&agrave; cảnh b&aacute;o khi c&oacute; nước ngập trong c&aacute;c khoang t&agrave;u<br />\r\n&nbsp;	z114	2020-06-07 22:44:17.462205+07	2020-06-07 22:44:17.462247+07	3
\.


--
-- Data for Name: addslide; Type: TABLE DATA; Schema: public; Owner: ckcvn
--

COPY public.addslide (id, name, link, content1, content2, image, active, slide_id) FROM stdin;
18	slide 1	https://stackoverflow.com/	\N	\N	tau_tuan_tra_cao_tốc_120_tan_uUZCWAd.jpg	t	1
\.


--
-- Data for Name: article_category; Type: TABLE DATA; Schema: public; Owner: ckcvn
--

COPY public.article_category (id, image, slug, title, descriptions, created_date, updated_date, lft, rght, tree_id, level, parent_id) FROM stdin;
1		tin-tuc-su-kien	Tin tức & sự kiện	Tin tức & sự kiện	2020-07-09 17:11:21.210093+07	\N	1	8	3	0	\N
2		tin-tuc-su-kien/su-kien-va-hoat-dong	Sự kiện và hoạt động	Sự kiện và hoạt động	2020-07-09 17:11:21.211511+07	\N	4	5	3	1	1
3		tin-tuc-su-kien/tin-tuc-nganh	Tin tức ngành	Tin tức ngành	2020-07-09 17:11:21.212719+07	\N	2	3	3	1	1
4		khoa-hoc-va-cong-nghe	Khoa học và công nghệ	Khoa học và công nghệ	2020-07-09 17:11:21.213898+07	\N	1	12	1	0	\N
6		khoa-hoc-va-cong-nghe/giai-phap-va-cong-nghe-thiet-ke-tau	Giải pháp và công nghệ thiết kế tàu	Giải pháp và công nghệ thiết kế tàu	2020-07-09 17:11:21.215067+07	\N	2	3	1	1	4
7		khoa-hoc-va-cong-nghe/nghien-cuu-trao-doi	Nghiên cứu - trao đổi	Nghiên cứu - trao đổi	2020-07-09 17:11:21.216249+07	\N	4	5	1	1	4
5		khoa-hoc-va-cong-nghe/hoat-dong-hop-tac	Hoạt động hợp tác	Hoạt động hợp tác	2020-07-09 17:11:21.217434+07	\N	6	7	1	1	4
8		gioi-thieu	Giới thiệu	Giới thiệu	2020-07-09 17:11:21.218581+07	\N	1	10	2	0	\N
9		gioi-thieu/lanh-dao-vien	Lãnh đạo viện	Lãnh đạo viện	2020-07-09 17:11:21.219748+07	\N	12	13	2	1	8
10		gioi-thieu/lich-su	Lịch sử phát triển	Lịch sử phát triển	2020-07-09 17:11:21.22092+07	\N	2	3	2	1	8
11		gioi-thieu/chuc-nang-nhiem-vu	Chức năng - Nhiệm vụ	Chức năng - Nhiệm vụ	2020-07-09 17:11:21.222089+07	\N	4	5	2	1	8
12		gioi-thieu/nang-luc	Năng lực	Năng lực	2020-07-09 17:11:21.223324+07	\N	6	1	2	1	8
13		gioi-thieu/nang-luc/nhan-luc	Nhân lực	Nhân lực	2020-07-09 17:11:21.224469+07	\N	9	10	2	2	8
14		gioi-thieu/nang-luc/co-so-ha-tang	Cơ sở hạ tầng	Cơ sở hạ tầng	2020-07-09 17:11:21.225626+07	\N	7	8	2	2	8
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: ckcvnvtk
--

COPY public.auth_group (id, name) FROM stdin;
1	Biên tập viên
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: ckcvnvtk
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	1	1
2	1	34
3	1	33
4	1	58
5	1	25
6	1	26
7	1	59
8	1	60
9	1	57
10	1	32
11	1	35
12	1	36
13	1	37
14	1	38
15	1	39
16	1	40
17	1	45
18	1	46
19	1	47
20	1	48
21	1	53
22	1	54
23	1	55
24	1	56
25	1	27
26	1	28
27	1	29
28	1	30
29	1	31
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: ckcvnvtk
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add user	4	add_user
14	Can change user	4	change_user
15	Can delete user	4	delete_user
16	Can view user	4	view_user
17	Can add content type	5	add_contenttype
18	Can change content type	5	change_contenttype
19	Can delete content type	5	delete_contenttype
20	Can view content type	5	view_contenttype
21	Can add session	6	add_session
22	Can change session	6	change_session
23	Can delete session	6	delete_session
24	Can view session	6	view_session
25	Can add Bài viết	7	add_article
26	Can change Bài viết	7	change_article
27	Can delete Bài viết	7	delete_article
28	Can view Bài viết	7	view_article
29	Can add Bình luận	8	add_comment
30	Can change Bình luận	8	change_comment
31	Can delete Bình luận	8	delete_comment
32	Can view Bình luận	8	view_comment
33	Can add Danh mục	9	add_category
34	Can change Danh mục	9	change_category
35	Can delete Danh mục	9	delete_category
36	Can view Danh mục	9	view_category
37	Can add Danh mục sản phẩm	10	add_category
38	Can change Danh mục sản phẩm	10	change_category
39	Can delete Danh mục sản phẩm	10	delete_category
40	Can view Danh mục sản phẩm	10	view_category
41	Can add Liên hệ	11	add_contact
42	Can change Liên hệ	11	change_contact
43	Can delete Liên hệ	11	delete_contact
44	Can view Liên hệ	11	view_contact
45	Can add Sản phẩm	12	add_product
46	Can change Sản phẩm	12	change_product
47	Can delete Sản phẩm	12	delete_product
48	Can view Sản phẩm	12	view_product
49	Can add Thông số	13	add_producttype
50	Can change Thông số	13	change_producttype
51	Can delete Thông số	13	delete_producttype
52	Can view Thông số	13	view_producttype
53	Can add Hình ảnh	14	add_images
54	Can change Hình ảnh	14	change_images
55	Can delete Hình ảnh	14	delete_images
56	Can view Hình ảnh	14	view_images
57	Can add Tạo nhóm slide	15	add_creategroupslide
58	Can change Tạo nhóm slide	15	change_creategroupslide
59	Can delete Tạo nhóm slide	15	delete_creategroupslide
60	Can view Tạo nhóm slide	15	view_creategroupslide
61	Can add Thêm slide	16	add_addslide
62	Can change Thêm slide	16	change_addslide
63	Can delete Thêm slide	16	delete_addslide
64	Can view Thêm slide	16	view_addslide
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: ckcvnvtk
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$180000$EvAwze2shNjI$JjKE4waPEylj5UsOF1vufszQQHAUPGsczNHT1BNs71E=	2020-08-24 09:07:45.597069+07	t	admin			hongquanvn1998@gmail.com	t	t	2020-07-09 17:16:52.080451+07
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: ckcvnvtk
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: ckcvnvtk
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: contact; Type: TABLE DATA; Schema: public; Owner: ckcvnvtk
--

COPY public.contact (id, full_name, phone, email, address, note, created_date, updated_at) FROM stdin;
1	Nguyễn Viết Thanh	0356288899	vietthanh331698@gmail.com	CT 12A khu đô thị kim văn kim lũ - Hoàng Mai _ Hà Nội	test	2020-07-15 20:15:00.506006+07	2020-07-15 20:15:00.506031+07
2	rxclthab		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:29:59.769531+07	2020-07-17 08:29:59.769554+07
3	rxclthab		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:30:00.692757+07	2020-07-17 08:30:00.69279+07
4	rxclthab		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:30:01.267632+07	2020-07-17 08:30:01.267652+07
5	pfhersag		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:30:03.850532+07	2020-07-17 08:30:03.850554+07
6	pfhersag		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:30:04.941537+07	2020-07-17 08:30:04.941559+07
7	pfhersag		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:30:05.884657+07	2020-07-17 08:30:05.884679+07
8	pfhersag		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:30:07.136183+07	2020-07-17 08:30:07.136205+07
9	pfhersag		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:30:08.140954+07	2020-07-17 08:30:08.140977+07
10	pfhersag		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:30:09.273144+07	2020-07-17 08:30:09.273167+07
11	ttiotxnp		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:33:39.544743+07	2020-07-17 08:33:39.544777+07
12	ttiotxnp		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:33:41.067736+07	2020-07-17 08:33:41.06776+07
13	ttiotxnp		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:33:42.244596+07	2020-07-17 08:33:42.244619+07
14	vqrefolq		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:33:45.463871+07	2020-07-17 08:33:45.463895+07
15	vqrefolq		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:33:47.26441+07	2020-07-17 08:33:47.264433+07
16	vqrefolq		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:33:48.446421+07	2020-07-17 08:33:48.446442+07
17	vqrefolq		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:33:49.633329+07	2020-07-17 08:33:49.633351+07
18	vqrefolq		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:33:50.932722+07	2020-07-17 08:33:50.932744+07
19	vqrefolq		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:33:51.961839+07	2020-07-17 08:33:51.961861+07
20	xuspehek		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:38:02.42162+07	2020-07-17 08:38:02.421656+07
21	xuspehek		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:38:04.777303+07	2020-07-17 08:38:04.777327+07
22	xuspehek		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:38:06.560401+07	2020-07-17 08:38:06.560434+07
23	dbmnptiv		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:38:12.67509+07	2020-07-17 08:38:12.675113+07
24	dbmnptiv		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:38:15.169489+07	2020-07-17 08:38:15.169513+07
25	dbmnptiv		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:38:17.029057+07	2020-07-17 08:38:17.029078+07
26	dbmnptiv		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:38:19.12189+07	2020-07-17 08:38:19.121913+07
27	dbmnptiv		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:38:21.775821+07	2020-07-17 08:38:21.775845+07
28	dbmnptiv		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:38:23.783039+07	2020-07-17 08:38:23.783064+07
29	sctltvie		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:40:00.399819+07	2020-07-17 08:40:00.399842+07
30	sctltvie		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:40:01.716311+07	2020-07-17 08:40:01.716334+07
31	sctltvie		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:40:03.211283+07	2020-07-17 08:40:03.211311+07
32	oocokdtc		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:40:05.788013+07	2020-07-17 08:40:05.788037+07
33	oocokdtc		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:40:07.28312+07	2020-07-17 08:40:07.283155+07
34	oocokdtc		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:40:08.665181+07	2020-07-17 08:40:08.665203+07
35	oocokdtc		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:40:10.300048+07	2020-07-17 08:40:10.300068+07
36	oocokdtc		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:40:11.405516+07	2020-07-17 08:40:11.405537+07
37	oocokdtc		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:40:12.483256+07	2020-07-17 08:40:12.483278+07
38	nfkoqxev		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:42:36.445986+07	2020-07-17 08:42:36.446008+07
39	dxkaescw		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:42:37.326006+07	2020-07-17 08:42:37.326028+07
40	nfkoqxev		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:42:37.527953+07	2020-07-17 08:42:37.528021+07
41	nfkoqxev		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:42:38.646009+07	2020-07-17 08:42:38.64603+07
42	dxkaescw		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:42:38.668049+07	2020-07-17 08:42:38.66807+07
43	dxkaescw		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:42:39.888855+07	2020-07-17 08:42:39.888877+07
44	nfkoqxev		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:42:40.021266+07	2020-07-17 08:42:40.021287+07
45	nfkoqxev		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:42:41.588739+07	2020-07-17 08:42:41.588761+07
46	nfkoqxev		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:42:43.326923+07	2020-07-17 08:42:43.326945+07
47	tvosyifk		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:45:11.428498+07	2020-07-17 08:45:11.428539+07
48	tvosyifk		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:45:13.034445+07	2020-07-17 08:45:13.034469+07
49	kympqtff		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:45:14.07855+07	2020-07-17 08:45:14.078572+07
50	tvosyifk		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:45:14.494185+07	2020-07-17 08:45:14.494208+07
51	kympqtff		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:45:15.314396+07	2020-07-17 08:45:15.314421+07
52	tvosyifk		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:45:16.186585+07	2020-07-17 08:45:16.186608+07
53	kympqtff		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:45:16.312499+07	2020-07-17 08:45:16.312521+07
54	tvosyifk		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:45:17.362075+07	2020-07-17 08:45:17.362099+07
55	tvosyifk		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:45:18.783593+07	2020-07-17 08:45:18.783615+07
56	rrrorkgp		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:47:35.578796+07	2020-07-17 08:47:35.57882+07
57	rrrorkgp		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:47:36.58335+07	2020-07-17 08:47:36.583372+07
58	rrrorkgp		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:47:37.960491+07	2020-07-17 08:47:37.960513+07
59	fqtpbvdm		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:47:45.128594+07	2020-07-17 08:47:45.128618+07
60	fqtpbvdm		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:47:46.224052+07	2020-07-17 08:47:46.224074+07
61	fqtpbvdm		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:47:47.856314+07	2020-07-17 08:47:47.856339+07
62	fqtpbvdm		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:47:49.259258+07	2020-07-17 08:47:49.259288+07
63	fqtpbvdm		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:47:50.527073+07	2020-07-17 08:47:50.527095+07
64	fqtpbvdm		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:47:52.414783+07	2020-07-17 08:47:52.414808+07
65	iqaryvls		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:49:55.359204+07	2020-07-17 08:49:55.35923+07
66	iqaryvls		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:49:56.723033+07	2020-07-17 08:49:56.723057+07
67	iqaryvls		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:49:57.824592+07	2020-07-17 08:49:57.824617+07
68	iqaryvls		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:49:59.511798+07	2020-07-17 08:49:59.511822+07
69	iqaryvls		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:50:01.28643+07	2020-07-17 08:50:01.286454+07
70	dsdarfdh		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:50:02.515027+07	2020-07-17 08:50:02.515058+07
71	iqaryvls		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:50:02.533206+07	2020-07-17 08:50:02.533227+07
72	dsdarfdh		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:50:03.569891+07	2020-07-17 08:50:03.569916+07
73	dsdarfdh		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:50:04.72156+07	2020-07-17 08:50:04.721582+07
74	ymwefyxn		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:52:26.780671+07	2020-07-17 08:52:26.780695+07
75	ymwefyxn		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:52:27.961705+07	2020-07-17 08:52:27.961728+07
76	natnryge		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:52:28.405441+07	2020-07-17 08:52:28.405474+07
77	ymwefyxn		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:52:29.39148+07	2020-07-17 08:52:29.391504+07
78	natnryge		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:52:30.38322+07	2020-07-17 08:52:30.383245+07
79	ymwefyxn		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:52:30.707736+07	2020-07-17 08:52:30.707758+07
80	natnryge		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:52:31.440165+07	2020-07-17 08:52:31.440189+07
81	ymwefyxn		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:52:32.242715+07	2020-07-17 08:52:32.242738+07
82	ymwefyxn		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:52:33.48739+07	2020-07-17 08:52:33.487426+07
83	bfpdrlas		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:54:57.117999+07	2020-07-17 08:54:57.118022+07
84	bfpdrlas		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:54:58.13317+07	2020-07-17 08:54:58.133192+07
85	bfpdrlas		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:54:59.183592+07	2020-07-17 08:54:59.183615+07
86	ogxfhais		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:54:59.805152+07	2020-07-17 08:54:59.805177+07
87	bfpdrlas		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:55:00.56768+07	2020-07-17 08:55:00.567702+07
88	ogxfhais		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:55:01.148622+07	2020-07-17 08:55:01.148644+07
89	bfpdrlas		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:55:01.778381+07	2020-07-17 08:55:01.778405+07
90	ogxfhais		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:55:02.310793+07	2020-07-17 08:55:02.310822+07
91	bfpdrlas		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:55:02.995121+07	2020-07-17 08:55:02.995153+07
92	jqcuslny		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:57:09.07699+07	2020-07-17 08:57:09.077013+07
93	jqcuslny		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:57:10.451062+07	2020-07-17 08:57:10.451084+07
94	jqcuslny		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:57:12.062612+07	2020-07-17 08:57:12.062638+07
95	riwnumwm		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:57:17.662786+07	2020-07-17 08:57:17.662809+07
96	riwnumwm		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:57:18.869646+07	2020-07-17 08:57:18.869671+07
97	riwnumwm		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:57:19.911823+07	2020-07-17 08:57:19.911853+07
98	riwnumwm		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:57:21.062023+07	2020-07-17 08:57:21.062046+07
99	riwnumwm		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:57:22.339138+07	2020-07-17 08:57:22.339161+07
100	riwnumwm		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:57:23.391703+07	2020-07-17 08:57:23.391725+07
101	duglccbc		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:59:36.519958+07	2020-07-17 08:59:36.519979+07
102	duglccbc		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:59:37.698101+07	2020-07-17 08:59:37.698122+07
103	duglccbc		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:59:40.023156+07	2020-07-17 08:59:40.023191+07
104	duglccbc		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:59:41.44668+07	2020-07-17 08:59:41.446701+07
105	kvhebrpy		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:59:42.442164+07	2020-07-17 08:59:42.442194+07
106	duglccbc		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:59:42.675865+07	2020-07-17 08:59:42.675889+07
107	kvhebrpy		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:59:43.689151+07	2020-07-17 08:59:43.689182+07
108	duglccbc		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:59:44.457319+07	2020-07-17 08:59:44.457345+07
109	kvhebrpy		sample@email.tst	3137 Laguna Street	1	2020-07-17 08:59:45.001566+07	2020-07-17 08:59:45.001589+07
110	qcjyoyws		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:01:59.368082+07	2020-07-17 09:01:59.368117+07
111	wbohokdo		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:01:59.402718+07	2020-07-17 09:01:59.402748+07
112	qcjyoyws		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:02:00.484541+07	2020-07-17 09:02:00.484565+07
113	wbohokdo		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:02:00.797855+07	2020-07-17 09:02:00.797877+07
114	qcjyoyws		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:02:01.942833+07	2020-07-17 09:02:01.942856+07
115	wbohokdo		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:02:02.097157+07	2020-07-17 09:02:02.097179+07
116	wbohokdo		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:02:04.033623+07	2020-07-17 09:02:04.033647+07
117	wbohokdo		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:02:05.30704+07	2020-07-17 09:02:05.307061+07
118	wbohokdo		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:02:06.105696+07	2020-07-17 09:02:06.105718+07
119	ulljdydb		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:03:46.099529+07	2020-07-17 09:03:46.099555+07
120	ulljdydb		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:03:46.824465+07	2020-07-17 09:03:46.82449+07
121	ulljdydb		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:03:47.56094+07	2020-07-17 09:03:47.560968+07
122	ulljdydb		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:03:48.377601+07	2020-07-17 09:03:48.377624+07
123	ulljdydb		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:03:49.124028+07	2020-07-17 09:03:49.124054+07
124	ulljdydb		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:03:50.101027+07	2020-07-17 09:03:50.101053+07
125	beykpmal		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:04:00.229659+07	2020-07-17 09:04:00.229683+07
126	beykpmal		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:04:00.805573+07	2020-07-17 09:04:00.805598+07
127	beykpmal		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:04:01.555571+07	2020-07-17 09:04:01.555595+07
128	qrscxvsg		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:05:39.181099+07	2020-07-17 09:05:39.181123+07
129	qrscxvsg		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:05:40.458455+07	2020-07-17 09:05:40.458478+07
130	qrscxvsg		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:05:41.405877+07	2020-07-17 09:05:41.405901+07
131	odhhadgc		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:05:51.505201+07	2020-07-17 09:05:51.505225+07
132	odhhadgc		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:05:53.50323+07	2020-07-17 09:05:53.503254+07
133	odhhadgc		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:05:54.690011+07	2020-07-17 09:05:54.690036+07
134	odhhadgc		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:05:56.046994+07	2020-07-17 09:05:56.047024+07
135	odhhadgc		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:05:57.950546+07	2020-07-17 09:05:57.950576+07
136	odhhadgc		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:05:59.206929+07	2020-07-17 09:05:59.206952+07
137	kdgfuqhi		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:08:13.891947+07	2020-07-17 09:08:13.891977+07
138	kdgfuqhi		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:08:15.864837+07	2020-07-17 09:08:15.864861+07
139	tcxcsxhf		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:08:16.234595+07	2020-07-17 09:08:16.23462+07
140	tcxcsxhf		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:08:18.297471+07	2020-07-17 09:08:18.297497+07
141	kdgfuqhi		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:08:18.376111+07	2020-07-17 09:08:18.376132+07
142	tcxcsxhf		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:08:20.405668+07	2020-07-17 09:08:20.405692+07
143	kdgfuqhi		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:08:20.428696+07	2020-07-17 09:08:20.428719+07
144	kdgfuqhi		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:08:22.6759+07	2020-07-17 09:08:22.675924+07
145	kdgfuqhi		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:08:24.870458+07	2020-07-17 09:08:24.87048+07
146	rcxbgrla		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:10:25.860422+07	2020-07-17 09:10:25.860445+07
147	rcxbgrla		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:10:28.870794+07	2020-07-17 09:10:28.870818+07
148	rcxbgrla		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:10:31.637696+07	2020-07-17 09:10:31.637719+07
149	rcxbgrla		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:10:33.530656+07	2020-07-17 09:10:33.530678+07
150	rcxbgrla		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:10:35.850164+07	2020-07-17 09:10:35.850188+07
151	rcxbgrla		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:10:38.067679+07	2020-07-17 09:10:38.067702+07
152	abxdpqya		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:10:44.226187+07	2020-07-17 09:10:44.226218+07
153	abxdpqya		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:10:46.155942+07	2020-07-17 09:10:46.155965+07
154	abxdpqya		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:10:47.989378+07	2020-07-17 09:10:47.989401+07
155	ocsqjnvf		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:13:17.925136+07	2020-07-17 09:13:17.925162+07
156	ocsqjnvf		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:13:19.694977+07	2020-07-17 09:13:19.695001+07
157	ocsqjnvf		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:13:21.366653+07	2020-07-17 09:13:21.366675+07
158	tgpxuwwc		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:13:22.29114+07	2020-07-17 09:13:22.291165+07
159	tgpxuwwc		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:13:24.313005+07	2020-07-17 09:13:24.313029+07
160	tgpxuwwc		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:13:26.585569+07	2020-07-17 09:13:26.585603+07
161	tgpxuwwc		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:13:28.491212+07	2020-07-17 09:13:28.491236+07
162	tgpxuwwc		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:13:30.866748+07	2020-07-17 09:13:30.866784+07
163	tgpxuwwc		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:13:32.902327+07	2020-07-17 09:13:32.90235+07
164	ggpxmkhs		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:15:59.873867+07	2020-07-17 09:15:59.873892+07
165	ggpxmkhs		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:16:02.416735+07	2020-07-17 09:16:02.416782+07
166	ggpxmkhs		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:16:03.986542+07	2020-07-17 09:16:03.986566+07
167	mviddhdl		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:16:10.471015+07	2020-07-17 09:16:10.471043+07
168	mviddhdl		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:16:14.692388+07	2020-07-17 09:16:14.692412+07
169	mviddhdl		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:16:17.495552+07	2020-07-17 09:16:17.495579+07
170	mviddhdl		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:16:19.320404+07	2020-07-17 09:16:19.320426+07
171	mviddhdl		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:16:21.798996+07	2020-07-17 09:16:21.79903+07
172	mviddhdl		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:16:24.201714+07	2020-07-17 09:16:24.201743+07
173	pncgflvs		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:18:24.879341+07	2020-07-17 09:18:24.879364+07
174	pncgflvs		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:18:26.686495+07	2020-07-17 09:18:26.68652+07
175	pncgflvs		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:18:29.000735+07	2020-07-17 09:18:29.000758+07
176	xaajtmcp		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:18:45.746899+07	2020-07-17 09:18:45.746923+07
177	xaajtmcp		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:18:48.25045+07	2020-07-17 09:18:48.250484+07
178	xaajtmcp		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:18:50.417108+07	2020-07-17 09:18:50.417131+07
179	xaajtmcp		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:18:52.970037+07	2020-07-17 09:18:52.970061+07
180	xaajtmcp		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:18:55.094036+07	2020-07-17 09:18:55.094059+07
181	xaajtmcp		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:18:58.29064+07	2020-07-17 09:18:58.290662+07
182	nleaufsn		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:21:13.458104+07	2020-07-17 09:21:13.458137+07
183	nleaufsn		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:21:14.898115+07	2020-07-17 09:21:14.898144+07
184	nleaufsn		sample@email.tst	3137 Laguna Street	1	2020-07-17 09:21:17.042761+07	2020-07-17 09:21:17.042797+07
\.


--
-- Data for Name: create_group_slide; Type: TABLE DATA; Schema: public; Owner: ckcvn
--

COPY public.create_group_slide (id, name, active) FROM stdin;
1	nhom 1	t
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: ckcvnvtk
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2020-07-09 20:48:33.128669+07	1	CreateGroupSlide object (1)	2	[{"added": {"name": "Th\\u00eam slide", "object": "AddSlide object (18)"}}]	15	1
2	2020-07-15 18:48:16.25334+07	1	Biên tập viên	1	[{"added": {}}]	3	1
3	2020-07-15 18:53:36.027966+07	2	thanh	1	[{"added": {}}]	4	1
4	2020-07-15 19:25:52.99028+07	1	Biên tập viên	2	[{"changed": {"fields": ["Permissions"]}}]	3	1
5	2020-07-15 19:27:17.777539+07	2	thanh	3		4	1
6	2020-07-15 19:32:15.496755+07	3	demo	1	[{"added": {}}]	4	1
7	2020-07-15 19:39:40.400013+07	3	demo	2	[{"changed": {"fields": ["First name", "Last name", "Email address", "Last login"]}}]	4	1
8	2020-07-15 19:42:59.77144+07	3	demo	2	[{"changed": {"fields": ["password"]}}]	4	1
9	2020-07-15 19:44:52.350764+07	3	demo	2	[{"changed": {"fields": ["password"]}}]	4	1
10	2020-07-15 19:45:19.056941+07	3	demo	2	[{"changed": {"fields": ["Groups", "Last login"]}}]	4	1
11	2020-07-15 19:49:35.815685+07	3	demo	2	[{"changed": {"fields": ["Staff status", "Superuser status", "Last login"]}}]	4	1
12	2020-07-15 19:52:24.114237+07	2	test	1	[{"added": {}}]	3	1
13	2020-07-15 19:52:34.433014+07	3	demo	2	[{"changed": {"fields": ["First name", "Groups"]}}]	4	1
15	2020-07-15 19:55:03.775307+07	3	demo	2	[{"changed": {"fields": ["Staff status", "Superuser status"]}}]	4	1
16	2020-07-15 19:55:37.939809+07	3	demo	2	[{"changed": {"fields": ["Staff status"]}}]	4	1
17	2020-07-15 20:02:51.151719+07	2	test	3		3	1
18	2020-07-15 20:03:02.535256+07	3	demo	3		4	1
19	2020-07-15 20:38:25.124235+07	1	CreateGroupSlide object (1)	2	[{"changed": {"name": "Th\\u00eam slide", "object": "AddSlide object (18)", "fields": ["N\\u1ed9i dung tr\\u00ean \\u1ea3nh", "N\\u1ed9i dung d\\u01b0\\u1edbi \\u1ea3nh"]}}]	15	1
20	2020-07-15 20:38:55.778129+07	1	CreateGroupSlide object (1)	2	[{"changed": {"name": "Th\\u00eam slide", "object": "AddSlide object (18)", "fields": ["N\\u1ed9i dung tr\\u00ean \\u1ea3nh", "N\\u1ed9i dung d\\u01b0\\u1edbi \\u1ea3nh"]}}]	15	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: ckcvnvtk
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	article	article
8	article	comment
9	article	category
10	product	category
11	product	contact
12	product	product
13	product	producttype
14	product	images
15	slideshow	creategroupslide
16	slideshow	addslide
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: ckcvnvtk
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2020-07-09 17:11:21.041048+07
2	auth	0001_initial	2020-07-09 17:11:21.090506+07
3	admin	0001_initial	2020-07-09 17:11:21.149653+07
4	admin	0002_logentry_remove_auto_add	2020-07-09 17:11:21.164162+07
5	admin	0003_logentry_add_action_flag_choices	2020-07-09 17:11:21.172485+07
6	article	0001_initial	2020-07-09 17:11:21.247208+07
7	contenttypes	0002_remove_content_type_name	2020-07-09 17:11:21.291728+07
8	auth	0002_alter_permission_name_max_length	2020-07-09 17:11:21.297043+07
9	auth	0003_alter_user_email_max_length	2020-07-09 17:11:21.30979+07
10	auth	0004_alter_user_username_opts	2020-07-09 17:11:21.318616+07
11	auth	0005_alter_user_last_login_null	2020-07-09 17:11:21.328053+07
12	auth	0006_require_contenttypes_0002	2020-07-09 17:11:21.329897+07
13	auth	0007_alter_validators_add_error_messages	2020-07-09 17:11:21.339385+07
14	auth	0008_alter_user_username_max_length	2020-07-09 17:11:21.351411+07
15	auth	0009_alter_user_last_name_max_length	2020-07-09 17:11:21.369283+07
16	auth	0010_alter_group_name_max_length	2020-07-09 17:11:21.382092+07
17	auth	0011_update_proxy_permissions	2020-07-09 17:11:21.398763+07
18	product	0001_initial	2020-07-09 17:11:21.483982+07
19	sessions	0001_initial	2020-07-09 17:11:21.509317+07
20	slideshow	0001_initial	2020-07-09 17:11:21.527384+07
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: ckcvnvtk
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
fgfm7m1x472lbu82yny1a2oh5k2y2xr4	N2FkYmRhYzJlZDczYWE0MTI4MmQ1NzU0MjE5NzJlMmJkMDlkM2Y4OTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4N2MxNWU0YTAxYzM3ZmRkMGQyMzk0YWE2NWQ1ODEwMjdkYTMzNWVmIn0=	2020-07-23 20:39:28.610311+07
wlz0jgv55s5q7s46wx6i69w1vtmyxdp1	N2FkYmRhYzJlZDczYWE0MTI4MmQ1NzU0MjE5NzJlMmJkMDlkM2Y4OTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4N2MxNWU0YTAxYzM3ZmRkMGQyMzk0YWE2NWQ1ODEwMjdkYTMzNWVmIn0=	2020-07-29 19:44:52.358018+07
b5wwfi8bd3k9kkwy2m59316xbvrx5eke	Y2M4ZTBkYTY1YTA1YTFkMDViMjM4NjIyYjVkNDMxZDQxMjAxMDM3NDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlOTEwOWNkMWRmM2NkMDZhNmIyM2RlYWMzZmZlZTVjOTdkMTEyNWJkIn0=	2020-07-29 19:55:44.024985+07
quqmyi2mf70pyjx8cbfzgtggjt7zf70e	NDRkMTVhNTRkYzUzYTYzOThjODdiMTU1OTFjY2RiZmY2YjA2MzMxODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0NGJmYjc1MTIwYjU1MjFlNjg4ZGE1ZjdlMGNjODg5YTNkMDBkZjVmIn0=	2020-08-03 09:07:05.54087+07
4aqm8lh7rpnzqp33qjvtyvzpgx8ggk1l	NDRkMTVhNTRkYzUzYTYzOThjODdiMTU1OTFjY2RiZmY2YjA2MzMxODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0NGJmYjc1MTIwYjU1MjFlNjg4ZGE1ZjdlMGNjODg5YTNkMDBkZjVmIn0=	2020-09-07 08:33:42.126623+07
o6ctd4g0bhileqraqs6j89ln39o5kts1	NDRkMTVhNTRkYzUzYTYzOThjODdiMTU1OTFjY2RiZmY2YjA2MzMxODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0NGJmYjc1MTIwYjU1MjFlNjg4ZGE1ZjdlMGNjODg5YTNkMDBkZjVmIn0=	2020-09-07 08:51:05.967718+07
lb23qi3vpo9qq59kyahpwl1573tye5vt	NDRkMTVhNTRkYzUzYTYzOThjODdiMTU1OTFjY2RiZmY2YjA2MzMxODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0NGJmYjc1MTIwYjU1MjFlNjg4ZGE1ZjdlMGNjODg5YTNkMDBkZjVmIn0=	2020-09-07 09:07:45.613672+07
\.


--
-- Data for Name: product_category; Type: TABLE DATA; Schema: public; Owner: ckcvn
--

COPY public.product_category (id, image, slug, title, descriptions, created_date, updated_date, lft, rght, tree_id, level, parent_id) FROM stdin;
1		san-pham-dich-vu	Sản phẩm & dịch vụ	Sản phẩm & dịch vụ	2020-07-09 17:11:21.426867+07	\N	1	8	1	0	\N
2		san-pham-dich-vu/san-pham-khac	Sản phẩm khác	Sản phẩm khác	2020-07-09 17:11:21.42863+07	\N	6	7	1	1	1
5		san-pham-dich-vu/san-pham-khac/bien-soan-tai-lieu	Biên soạn tài liệu	Biên soạn tài liệu	2020-07-09 17:11:21.430064+07	\N	13	14	1	2	2
6		san-pham-dich-vu/san-pham-khac/thi-cong-dong-moi-sua-chua-hien-dai-hoa-tau	Giám sát thi công, đóng mới, sửa chữa, hiện đại hóa tàu	Giám sát thi công, đóng mới, sửa chữa, hiện đại hóa tàu	2020-07-09 17:11:21.431434+07	\N	7	8	1	2	2
7		san-pham-dich-vu/san-pham-khac/bao-dam-ky-thuat-tau	Bảo đảm kỹ thuật tàu	Bảo đảm kỹ thuật tàu	2020-07-09 17:11:21.432823+07	\N	9	10	1	2	2
8		san-pham-dich-vu/san-pham-khac/khai-thac-su-dung-va-dam-bao-ky-thuat	Khai thác sử dụng và đảm bảo kỹ thuật	Khai thác sử dụng và đảm bảo kỹ thuật	2020-07-09 17:11:21.43414+07	\N	11	12	1	2	2
3		san-pham-dich-vu/san-pham-nghien-cuu	Sản phẩm nghiên cứu khoa học	Sản phẩm nghiên cứu khoa học	2020-07-09 17:11:21.435257+07	\N	2	3	1	1	1
4		san-pham-dich-vu/san-pham-thiet-ke	Sản phẩm thiết kế tàu	Sản phẩm thiết kế tàu	2020-07-09 17:11:21.436247+07	\N	4	5	1	1	1
9		san-pham-dich-vu/tieu-chuan-do-luong-chat-luong	TIÊU CHUẨN - ĐO LƯỜNG - CHẤT LƯỢNG	TIÊU CHUẨN - ĐO LƯỜNG - CHẤT LƯỢNG	2020-07-09 17:11:21.437232+07	\N	6	11	1	1	1
10		san-pham-dich-vu/tieu-chuan-do-luong-chat-luong/cong-tac-do-luong-thu-nghiem	Công tác Đo lường, thử nghiệm	Công tác Đo lường, thử nghiệm	2020-07-09 17:11:21.438221+07	\N	9	10	1	2	9
11		san-pham-dich-vu/tieu-chuan-do-luong-chat-luong/tieu-chuan-van-ban-quy-pham-nganh-dong-tau	Tiêu chuẩn, văn bản quy phạm ngành đóng tàu	Tiêu chuẩn, văn bản quy phạm ngành đóng tàu	2020-07-09 17:11:21.439238+07	\N	7	8	1	2	9
\.


--
-- Data for Name: products_feature; Type: TABLE DATA; Schema: public; Owner: ckcvnvtk
--

COPY public.products_feature (id, name, unit, value, product_id) FROM stdin;
\.


--
-- Data for Name: products_images; Type: TABLE DATA; Schema: public; Owner: ckcvnvtk
--

COPY public.products_images (id, image, post_id) FROM stdin;
\.


--
-- Name: Article_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvnvtk
--

SELECT pg_catalog.setval('public."Article_comment_id_seq"', 1, false);


--
-- Name: Article_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvn
--

SELECT pg_catalog.setval('public."Article_post_id_seq"', 9, true);


--
-- Name: Product_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvn
--

SELECT pg_catalog.setval('public."Product_product_id_seq"', 25, true);


--
-- Name: addslide_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvn
--

SELECT pg_catalog.setval('public.addslide_id_seq', 18, true);


--
-- Name: article_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvn
--

SELECT pg_catalog.setval('public.article_category_id_seq', 1, false);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvnvtk
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 2, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvnvtk
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 33, true);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvnvtk
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 64, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvnvtk
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 2, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvnvtk
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 3, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvnvtk
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: contact_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvnvtk
--

SELECT pg_catalog.setval('public.contact_id_seq', 184, true);


--
-- Name: create_group_slide_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvn
--

SELECT pg_catalog.setval('public.create_group_slide_id_seq', 1, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvnvtk
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 20, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvnvtk
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 16, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvnvtk
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 20, true);


--
-- Name: product_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvn
--

SELECT pg_catalog.setval('public.product_category_id_seq', 1, false);


--
-- Name: products_feature_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvnvtk
--

SELECT pg_catalog.setval('public.products_feature_id_seq', 1, false);


--
-- Name: products_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvnvtk
--

SELECT pg_catalog.setval('public.products_images_id_seq', 1, false);


--
-- Name: Article_comment Article_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public."Article_comment"
    ADD CONSTRAINT "Article_comment_pkey" PRIMARY KEY (id);


--
-- Name: Article_post Article_post_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public."Article_post"
    ADD CONSTRAINT "Article_post_pkey" PRIMARY KEY (id);


--
-- Name: Product_product Product_product_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public."Product_product"
    ADD CONSTRAINT "Product_product_pkey" PRIMARY KEY (id);


--
-- Name: addslide addslide_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.addslide
    ADD CONSTRAINT addslide_pkey PRIMARY KEY (id);


--
-- Name: article_category article_category_parent_id_slug_2e115ea3_uniq; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.article_category
    ADD CONSTRAINT article_category_parent_id_slug_2e115ea3_uniq UNIQUE (parent_id, slug);


--
-- Name: article_category article_category_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.article_category
    ADD CONSTRAINT article_category_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: contact contact_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (id);


--
-- Name: create_group_slide create_group_slide_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.create_group_slide
    ADD CONSTRAINT create_group_slide_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: product_category product_category_parent_id_slug_4e7c48ae_uniq; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.product_category
    ADD CONSTRAINT product_category_parent_id_slug_4e7c48ae_uniq UNIQUE (parent_id, slug);


--
-- Name: product_category product_category_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.product_category
    ADD CONSTRAINT product_category_pkey PRIMARY KEY (id);


--
-- Name: products_feature products_feature_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.products_feature
    ADD CONSTRAINT products_feature_pkey PRIMARY KEY (id);


--
-- Name: products_images products_images_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.products_images
    ADD CONSTRAINT products_images_pkey PRIMARY KEY (id);


--
-- Name: Article_comment_article_id_03734080; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX "Article_comment_article_id_03734080" ON public."Article_comment" USING btree (article_id);


--
-- Name: Article_comment_author_id_df13c4c9; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX "Article_comment_author_id_df13c4c9" ON public."Article_comment" USING btree (author_id);


--
-- Name: Article_post_category_id_bf48cd76; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX "Article_post_category_id_bf48cd76" ON public."Article_post" USING btree (category_id);


--
-- Name: Product_product_category_id_ded7ea18; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX "Product_product_category_id_ded7ea18" ON public."Product_product" USING btree (category_id);


--
-- Name: addslide_slide_id_7b45bab2; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX addslide_slide_id_7b45bab2 ON public.addslide USING btree (slide_id);


--
-- Name: article_cat_parent__86e9c6_idx; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX article_cat_parent__86e9c6_idx ON public.article_category USING btree (parent_id);


--
-- Name: article_cat_title_46e0c7_idx; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX article_cat_title_46e0c7_idx ON public.article_category USING btree (title);


--
-- Name: article_category_parent_id_7918a69e; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX article_category_parent_id_7918a69e ON public.article_category USING btree (parent_id);


--
-- Name: article_category_slug_c9738fba; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX article_category_slug_c9738fba ON public.article_category USING btree (slug);


--
-- Name: article_category_slug_c9738fba_like; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX article_category_slug_c9738fba_like ON public.article_category USING btree (slug varchar_pattern_ops);


--
-- Name: article_category_tree_id_3576cbd5; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX article_category_tree_id_3576cbd5 ON public.article_category USING btree (tree_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: product_cat_parent__b54811_idx; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX product_cat_parent__b54811_idx ON public.product_category USING btree (parent_id);


--
-- Name: product_cat_title_74c9a5_idx; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX product_cat_title_74c9a5_idx ON public.product_category USING btree (title);


--
-- Name: product_category_parent_id_f6860923; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX product_category_parent_id_f6860923 ON public.product_category USING btree (parent_id);


--
-- Name: product_category_slug_e1f8ccc4; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX product_category_slug_e1f8ccc4 ON public.product_category USING btree (slug);


--
-- Name: product_category_slug_e1f8ccc4_like; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX product_category_slug_e1f8ccc4_like ON public.product_category USING btree (slug varchar_pattern_ops);


--
-- Name: product_category_tree_id_f3c46461; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX product_category_tree_id_f3c46461 ON public.product_category USING btree (tree_id);


--
-- Name: products_feature_product_id_eaac053b; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX products_feature_product_id_eaac053b ON public.products_feature USING btree (product_id);


--
-- Name: products_images_post_id_99ca5974; Type: INDEX; Schema: public; Owner: ckcvnvtk
--

CREATE INDEX products_images_post_id_99ca5974 ON public.products_images USING btree (post_id);


--
-- Name: Article_comment Article_comment_article_id_03734080_fk_Article_post_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public."Article_comment"
    ADD CONSTRAINT "Article_comment_article_id_03734080_fk_Article_post_id" FOREIGN KEY (article_id) REFERENCES public."Article_post"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Article_comment Article_comment_author_id_df13c4c9_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public."Article_comment"
    ADD CONSTRAINT "Article_comment_author_id_df13c4c9_fk_auth_user_id" FOREIGN KEY (author_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Article_post Article_post_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public."Article_post"
    ADD CONSTRAINT "Article_post_category_id_fkey" FOREIGN KEY (category_id) REFERENCES public.article_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Product_product Product_product_category_id_ded7ea18_fk_product_category_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public."Product_product"
    ADD CONSTRAINT "Product_product_category_id_ded7ea18_fk_product_category_id" FOREIGN KEY (category_id) REFERENCES public.product_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: addslide addslide_slide_id_7b45bab2_fk_create_group_slide_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.addslide
    ADD CONSTRAINT addslide_slide_id_7b45bab2_fk_create_group_slide_id FOREIGN KEY (slide_id) REFERENCES public.create_group_slide(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: article_category article_category_parent_id_7918a69e_fk_article_category_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.article_category
    ADD CONSTRAINT article_category_parent_id_7918a69e_fk_article_category_id FOREIGN KEY (parent_id) REFERENCES public.article_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_category product_category_parent_id_f6860923_fk_product_category_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.product_category
    ADD CONSTRAINT product_category_parent_id_f6860923_fk_product_category_id FOREIGN KEY (parent_id) REFERENCES public.product_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_feature products_feature_product_id_eaac053b_fk_Product_product_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.products_feature
    ADD CONSTRAINT "products_feature_product_id_eaac053b_fk_Product_product_id" FOREIGN KEY (product_id) REFERENCES public."Product_product"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_images products_images_post_id_99ca5974_fk_Product_product_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvnvtk
--

ALTER TABLE ONLY public.products_images
    ADD CONSTRAINT "products_images_post_id_99ca5974_fk_Product_product_id" FOREIGN KEY (post_id) REFERENCES public."Product_product"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

