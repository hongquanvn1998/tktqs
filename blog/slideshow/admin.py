from django.contrib import admin
from .models import AddSlide, CreateGroupSlide
# Register your models here.
class AddSlideAdmin(admin.StackedInline):
    model = AddSlide
    fieldsets = (
        ('', { 
            'fields': (
                ('name','link'),
                ('content1','content2'),
                ('image'),
                ('active'),
                )
        }),    
 
    )
    extra = 1

@admin.register(CreateGroupSlide)
class CreateGroupSlideAdmin(admin.ModelAdmin):
    list_display = ('name','active')
    inlines = [AddSlideAdmin] 




