from django.db import models
from django.utils import timezone




class CreateGroupSlide(models.Model):
    name = models.CharField(blank = True, null = True,max_length=100,verbose_name="Tên nhóm slide")
    active = models.BooleanField(default=True,verbose_name="Kích hoạt")
    class Meta:
        db_table = 'create_group_slide'
        verbose_name = 'Tạo nhóm slide'
        verbose_name_plural = 'Tạo nhóm slide'

class AddSlide(models.Model):
    slide = models.ForeignKey(CreateGroupSlide,on_delete=models.CASCADE,verbose_name="Thuộc nhóm")
    name = models.CharField(max_length=100,verbose_name="Tên slide",blank = True, null = True)
    link = models.CharField(max_length=500,verbose_name="Đường dẫn",blank = True, null = True)
    content1 = models.CharField(max_length=500,verbose_name="Nội dung trên ảnh",blank = True, null = True)
    content2 = models.CharField(max_length=500,verbose_name="Nội dung dưới ảnh",blank = True, null = True)
    image = models.ImageField(verbose_name="Ảnh slide")
    active = models.BooleanField(default=True,verbose_name="Kích hoạt")
    class Meta:
        db_table = 'addslide'
        verbose_name = 'Thêm slide'
        verbose_name_plural = 'Thêm slide'
# class AddContentSlide(models.Model):
    
#     slide = models.ForeignKey(AddSlide,verbose_name="Slide liên quan",on_delete = models.CASCADE,default=None)

#     class Meta:
#         db_table = 'addslide_addcontent'
#         managed = True
#         verbose_name = 'Thêm nội dung'
#         verbose_name_plural = 'Thêm nội dung'