--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3 (Debian 12.3-1.pgdg100+1)
-- Dumped by pg_dump version 12.3 (Debian 12.3-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Article_post; Type: TABLE; Schema: public; Owner: ckcvn
--

CREATE TABLE public."Article_post" (
    id integer NOT NULL,
    title character varying(500) NOT NULL,
    content text,
    created_date timestamp with time zone NOT NULL,
    image character varying(100) NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public."Article_post" OWNER TO ckcvn;

--
-- Name: Article_post_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvn
--

CREATE SEQUENCE public."Article_post_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Article_post_id_seq" OWNER TO ckcvn;

--
-- Name: Article_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvn
--

ALTER SEQUENCE public."Article_post_id_seq" OWNED BY public."Article_post".id;


--
-- Name: Product_product; Type: TABLE; Schema: public; Owner: ckcvn
--

CREATE TABLE public."Product_product" (
    id integer NOT NULL,
    title character varying(500) NOT NULL,
    image character varying(100) NOT NULL,
    content text,
    code character varying(100),
    created_date timestamp with time zone,
    updated_at timestamp with time zone NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public."Product_product" OWNER TO ckcvn;

--
-- Name: Product_product_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvn
--

CREATE SEQUENCE public."Product_product_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Product_product_id_seq" OWNER TO ckcvn;

--
-- Name: Product_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvn
--

ALTER SEQUENCE public."Product_product_id_seq" OWNED BY public."Product_product".id;


--
-- Name: addslide; Type: TABLE; Schema: public; Owner: ckcvn
--

CREATE TABLE public.addslide (
    id integer NOT NULL,
    name character varying(100),
    link character varying(500),
    content1 character varying(500),
    content2 character varying(500),
    image character varying(100) NOT NULL,
    active boolean NOT NULL,
    slide_id integer NOT NULL
);


ALTER TABLE public.addslide OWNER TO ckcvn;

--
-- Name: addslide_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvn
--

CREATE SEQUENCE public.addslide_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.addslide_id_seq OWNER TO ckcvn;

--
-- Name: addslide_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvn
--

ALTER SEQUENCE public.addslide_id_seq OWNED BY public.addslide.id;


--
-- Name: article_category; Type: TABLE; Schema: public; Owner: ckcvn
--

CREATE TABLE public.article_category (
    id integer NOT NULL,
    image character varying(100),
    slug character varying(500) NOT NULL,
    title character varying(255) NOT NULL,
    descriptions character varying(100),
    created_date timestamp with time zone NOT NULL,
    updated_date timestamp with time zone,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    parent_id integer,
    CONSTRAINT article_category_level_check CHECK ((level >= 0)),
    CONSTRAINT article_category_lft_check CHECK ((lft >= 0)),
    CONSTRAINT article_category_rght_check CHECK ((rght >= 0)),
    CONSTRAINT article_category_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.article_category OWNER TO ckcvn;

--
-- Name: article_category_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvn
--

CREATE SEQUENCE public.article_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.article_category_id_seq OWNER TO ckcvn;

--
-- Name: article_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvn
--

ALTER SEQUENCE public.article_category_id_seq OWNED BY public.article_category.id;


--
-- Name: create_group_slide; Type: TABLE; Schema: public; Owner: ckcvn
--

CREATE TABLE public.create_group_slide (
    id integer NOT NULL,
    name character varying(100),
    active boolean NOT NULL
);


ALTER TABLE public.create_group_slide OWNER TO ckcvn;

--
-- Name: create_group_slide_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvn
--

CREATE SEQUENCE public.create_group_slide_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.create_group_slide_id_seq OWNER TO ckcvn;

--
-- Name: create_group_slide_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvn
--

ALTER SEQUENCE public.create_group_slide_id_seq OWNED BY public.create_group_slide.id;


--
-- Name: product_category; Type: TABLE; Schema: public; Owner: ckcvn
--

CREATE TABLE public.product_category (
    id integer NOT NULL,
    image character varying(100),
    slug character varying(500) NOT NULL,
    title character varying(255) NOT NULL,
    descriptions character varying(100),
    created_date timestamp with time zone,
    updated_date timestamp with time zone,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    parent_id integer,
    CONSTRAINT product_category_level_check CHECK ((level >= 0)),
    CONSTRAINT product_category_lft_check CHECK ((lft >= 0)),
    CONSTRAINT product_category_rght_check CHECK ((rght >= 0)),
    CONSTRAINT product_category_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.product_category OWNER TO ckcvn;

--
-- Name: product_category_id_seq; Type: SEQUENCE; Schema: public; Owner: ckcvn
--

CREATE SEQUENCE public.product_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_category_id_seq OWNER TO ckcvn;

--
-- Name: product_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckcvn
--

ALTER SEQUENCE public.product_category_id_seq OWNED BY public.product_category.id;


--
-- Name: Article_post id; Type: DEFAULT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public."Article_post" ALTER COLUMN id SET DEFAULT nextval('public."Article_post_id_seq"'::regclass);


--
-- Name: Product_product id; Type: DEFAULT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public."Product_product" ALTER COLUMN id SET DEFAULT nextval('public."Product_product_id_seq"'::regclass);


--
-- Name: addslide id; Type: DEFAULT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.addslide ALTER COLUMN id SET DEFAULT nextval('public.addslide_id_seq'::regclass);


--
-- Name: article_category id; Type: DEFAULT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.article_category ALTER COLUMN id SET DEFAULT nextval('public.article_category_id_seq'::regclass);


--
-- Name: create_group_slide id; Type: DEFAULT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.create_group_slide ALTER COLUMN id SET DEFAULT nextval('public.create_group_slide_id_seq'::regclass);


--
-- Name: product_category id; Type: DEFAULT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.product_category ALTER COLUMN id SET DEFAULT nextval('public.product_category_id_seq'::regclass);


--
-- Data for Name: Article_post; Type: TABLE DATA; Schema: public; Owner: ckcvn
--

COPY public."Article_post" (id, title, content, created_date, image, category_id) FROM stdin;
1	Chức năng - Nhiệm vụ	<div style="text-align: center;"><span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif">CHỨC NĂNG</span></span></b></span></span></span></div>\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Viện Thiết kế t&agrave;u qu&acirc;n sự trực thuộc Tổng cục C&ocirc;ng nghiệp quốc ph&ograve;ng; l&agrave; cơ sở nghi&ecirc;n cứu, thiết kế t&agrave;u ng&agrave;nh trong lĩnh vực đ&oacute;ng t&agrave;u qu&acirc;n sự.</span></span></span></span></span>\r\n\r\n<div style="text-align: center;"><span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif">NHIỆM VỤ</span></span></b></span></span></span></div>\r\n\r\n<ul>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Thiết kế đ&oacute;ng mới (đ&oacute;ng mới v&agrave; ho&aacute;n cải), nghi&ecirc;n cứu lắp đặt, hiệu chỉnh v&agrave; t&iacute;ch hợp hệ thống vũ kh&iacute;, kh&iacute; t&agrave;i tr&ecirc;n t&agrave;u. Tham gia thiết kế đ&oacute;ng mới t&agrave;u, phương tiện đường thủy phục vụ nhu cầu d&acirc;n sinh, xuất khẩu.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Nghi&ecirc;n cứu, tư vấn gi&uacute;p Tổng cục C&ocirc;ng nghiện quốc ph&ograve;ng đề xuất với Bộ Quốc ph&ograve;ng về định hướng quy hoạch ph&aacute;t triển ng&agrave;nh đ&oacute;ng t&agrave;u qu&acirc;n sự v&agrave; định hướng ph&aacute;t triển ng&agrave;nh đ&oacute;ng t&agrave;u qu&acirc;n sự v&agrave; định hướng ph&aacute;t triển t&agrave;u qu&acirc;n sự, vũ kh&iacute;, trang bị kỹ thuật tr&ecirc;n t&agrave;u; nghi&ecirc;n cứu, đề xuất Tổng cục định hướng ph&aacute;t triển, n&acirc;ng cao tiềm lực c&aacute;c nh&agrave; m&aacute;y đ&oacute;ng mới, sửa chữa t&agrave;u qu&acirc;n sự.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Quản l&yacute; lưu trữ t&agrave;u liệu thiết kế c&aacute;c t&agrave;u qu&acirc;n sự. X&acirc;y dựng, ban h&agrave;nh v&agrave; tr&igrave;nh c&aacute;c cấp c&oacute; thẩm quyền ban h&agrave;nh c&aacute;c ti&ecirc;u chuẩn ng&agrave;nh đ&oacute;ng t&agrave;u qu&acirc;n sự.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Tư vấn v&agrave; tham gia thẩm định c&aacute;c dự &aacute;n đầu tư đ&oacute;ng mới, sửa chữa t&agrave;u qu&acirc;n sự.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Gi&aacute;m s&aacute;t thiết kế trong qu&aacute; tr&igrave;nh thi c&ocirc;ng v&agrave; tham gia nghiệm thu đối với c&aacute;c sản phẩm t&agrave;u do Viện thiết kế; gi&aacute;m s&aacute;t thi c&ocirc;ng đ&oacute;ng mới, sửa chữa t&agrave;u theo y&ecirc;u cầu nhiệm vụ.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">X&acirc;y dựng v&agrave; ph&aacute;t triển tiềm lực khoa học c&ocirc;ng nghệ của Viện.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Tham gia đ&agrave;o tạo nguồn nh&acirc;n lực cho ng&agrave;nh đ&oacute;ng t&agrave;u qu&acirc;n sự ph&acirc;n c&ocirc;ng của Nh&agrave; nước , Bộ Quốc ph&ograve;ng v&agrave; Tổng cục C&ocirc;ng nghiệp quốc ph&ograve;ng.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Tổ chức hợp t&aacute;c về khoa học c&ocirc;ng nghệ, chuyển giao c&ocirc;ng nghệ với đối t&aacute;c trong, ngo&agrave;i nước thuộc lĩnh vực&nbsp;đ&oacute;ng t&agrave;u qu&acirc;n sự theo quy định của ph&aacute;p luật v&agrave; của Bộ Quốc ph&ograve;ng.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Hoạt động kinh tế, dịch vụ khoa học c&ocirc;ng nghệ trong lĩnh vực đ&oacute;ng t&agrave;u v&agrave; thực hiện c&aacute;c nhiệm vụ kh&aacute;c do cấp tr&ecirc;n giao.</span></span></span></span></span></span></li>\r\n\t<li style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:normal"><span style="tab-stops:list .5in"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Kiểm định phương tiện đo, thử nghiệm vũ kh&iacute; trang bị.</span></span></span></span></span></span></li>\r\n</ul>	2020-07-01 15:58:14.068343+00	Chức_năng_-_nhiệm_vụ_21lbNNj.jpg	11
2	Đội ngũ cán bộ	<span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Đội ngũ c&aacute;n bộ, kỹ sư của Viện tr&ecirc;n 70 người, trong đ&oacute; c&oacute; tr&ecirc;n 70% c&oacute; tr&igrave;nh độ đại học trở l&ecirc;n. Phần lớn c&aacute;n bộ nghi&ecirc;n cứu của Viện được đ&agrave;o tạo cơ bản v&agrave; chuy&ecirc;n s&acirc;u trong c&aacute;c lĩnh vực đ&oacute;ng t&agrave;u, cơ kh&iacute; động lực, vũ kh&iacute; - kh&iacute; t&agrave;i c&aacute;c trường đại học của Nga, Ba Lan, Ucraina, Belarus, Australia, Đại học H&agrave;ng hải Việt Nam, Học viện kỹ thuật qu&acirc;n sự, Đại học b&aacute;ch khoa H&agrave; Nội.</span></span>\r\n<div>&nbsp;</div>\r\n<img height="353" src="/media/uploads/2020/07/01/nhan-luc.jpg" width="522" /><br />\r\n<span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Nhiều&nbsp;c&aacute;n bộ từng tham gia nhiều dự &aacute;n thiết kế t&agrave;u cho qu&acirc;n đội v&agrave; d&acirc;n sự; gi&aacute;m s&aacute;t thi c&ocirc;ng đ&oacute;ng mới, sửa chữa t&agrave;u; c&aacute;c dự &aacute;n chuyển giao c&ocirc;ng nghệ đ&oacute;ng t&agrave;u.<br />\r\n<br />\r\nNhiều c&aacute;n bộ của Viện từng c&ocirc;ng t&aacute;c nhiều năm tại c&aacute;c đơn vị thiết kế, đ&oacute;ng t&agrave;u trong v&agrave; ngo&agrave;i Qu&acirc;n đội tr&ecirc;n c&aacute;c cương vị chuy&ecirc;n gia lập dự &aacute;n, thiết kế gi&aacute;m s&aacute;t thi c&ocirc;ng đ&oacute;ng mới, sửa chữa c&aacute;c loại t&agrave;u, thuyền kh&aacute;c nhau.</span></span><br />\r\n&nbsp;	2020-07-01 16:01:56.478413+00	nhân_lực_0PNHpcz.jpg	13
3	Cơ sở vật chất - Hạ tầng	<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><b><i><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Trụ sở l&agrave;m việc của Viện được x&acirc;y dựng tr&ecirc;n khu&ocirc;n vi&ecirc;n rộng 1,65 ha tại Dương X&aacute;, Gia L&acirc;m, H&agrave; Nội gồm 01 nh&agrave; l&agrave;m việc trung t&acirc;m 9 tầng, 01 nh&agrave; đa năng 5 tầng, 01 nh&agrave; xưởng được thiết kế hiện đại phục vụ c&ocirc;ng t&aacute;c nghi&ecirc;n cứu, thiết kế, chế thử.</span></span></i></b></span></span></span><br />\r\n<br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Hệ thống m&aacute;y t&iacute;nh chuy&ecirc;n dụng được lắp đặt đồng bộ phục vụ c&ocirc;ng t&aacute;c thiết kế t&agrave;u tr&ecirc;n c&aacute;c phần mềm như AVEVA Marine, FINE Marine, ... bao gồm: 02 m&aacute;y chủ (Server), 32 m&aacute;y trạm l&agrave;m việc (WordStation), 03 m&aacute;y trạm l&agrave;m việc di động (Mobile WorkStation) v&agrave; hơn 60 bộ m&aacute;y t&iacute;nh. Trao đổi dữ liệu thiết kế được thực hiện đơn giản, nhanh ch&oacute;ng th&ocirc;ng qua hệ thống mạng LAN.</span></span></span></span></span>\r\n<div style="text-align: center;"><img height="188" src="/media/uploads/2020/07/01/image.png" width="336" /></div>\r\n<br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Hệ thống ph&ograve;ng hội thảo quốc tế hiện đại</span></span></span></span></span>\r\n\r\n<div style="text-align: center;"><img height="198" src="/media/uploads/2020/07/01/image_s5B22a4.png" width="299" /><br />\r\n<img height="199" src="/media/uploads/2020/07/01/image_8VDE9lq.png" width="298" /></div>\r\n<br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">C&aacute;c m&aacute;y in, m&aacute;y scan hiện đại từ khổ A0-A4 đảm bảo việc in ấn bản vẽ, t&agrave;i liệu thiết kế nhanh ch&oacute;ng, chất lượng.</span></span></span></span></span><br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Hệ thống&nbsp;thư viện điện tử vớ i trên 1000 đầu&nbsp;sách kỹ thuật chuyên ngành. Cơ sở dữ liệu về&nbsp;các mẫu tàu phong phú , đa dạng và cập nhập nhật liên tục.</span></span><br />\r\n<br />\r\n<b><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Thiết bị gia c&ocirc;ng cơ kh&iacute;</span></span></b></span></span></span><br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Viện c&oacute; 01 xưởng gia c&ocirc;ng cơ kh&iacute; c&oacute; diện t&iacute;ch 916 m2 với hệ thống m&aacute;y gia c&ocirc;ng hiện đại phục vụ nghi&ecirc;n cứu, chế thử c&aacute;c sản phẩm đề t&agrave;i NCKH v&agrave; tham gia dịch vụ KHCN:</span></span></span></span></span>\r\n\r\n<div style="text-align: center;"><img height="534" src="/media/uploads/2020/07/01/image_HcIaDlS.png" width="680" /></div>\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><v:rect alt="/media/uploads/2020/06/09/6_70wTkCA.jpg" filled="f" id="Rectangle_x0020_10" o:gfxdata="UEsDBBQABgAIAAAAIQC75UiUBQEAAB4CAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbKSRvU7DMBSF\r\ndyTewfKKEqcMCKEmHfgZgaE8wMW+SSwc27JvS/v23KTJgkoXFsu+P+c7Ol5vDoMTe0zZBl/LVVlJ\r\ngV4HY31Xy4/tS3EvRSbwBlzwWMsjZrlprq/W22PELHjb51r2RPFBqax7HCCXIaLnThvSAMTP1KkI\r\n+gs6VLdVdad08ISeCho1ZLN+whZ2jsTzgcsnJwldluLxNDiyagkxOquB2Knae/OLUsyEkjenmdzb\r\nmG/YhlRnCWPnb8C898bRJGtQvEOiVxjYhtLOxs8AySiT4JuDystlVV4WPeM6tK3VaILeDZxIOSsu\r\nti/jidNGNZ3/J08yC1dNv9v8AAAA//8DAFBLAwQUAAYACAAAACEArTA/8cEAAAAyAQAACwAAAF9y\r\nZWxzLy5yZWxzhI/NCsIwEITvgu8Q9m7TehCRpr2I4FX0AdZk2wbbJGTj39ubi6AgeJtl2G9m6vYx\r\njeJGka13CqqiBEFOe2Ndr+B03C3WIDihMzh6RwqexNA281l9oBFTfuLBBhaZ4ljBkFLYSMl6oAm5\r\n8IFcdjofJ0z5jL0MqC/Yk1yW5UrGTwY0X0yxNwri3lQgjs+Qk/+zfddZTVuvrxO59CNCmoj3vCwj\r\nMfaUFOjRhrPHaN4Wv0VV5OYgm1p+LW1eAAAA//8DAFBLAwQUAAYACAAAACEAdd2zxs0DAACNCAAA\r\nHwAAAGNsaXBib2FyZC9kcmF3aW5ncy9kcmF3aW5nMS54bWy0Vttu4zYQfS/QfyD4rohy5IuMVRaO\r\nHS0KpLtBnH0uaIqy2KVIlaRvW/Rf+i37ZR1ScnxJ0YddVAFockgenjkzQ+bd+30j0ZYbK7TKcXJD\r\nMOKK6VKodY4/vxTRBCPrqCqp1Irn+MAtfn/380/v6HRtaFsLhgBB2SnNce1cO41jy2reUHujW65g\r\nrtKmoQ6GZh2Xhu4AuZHxgJBR3FCh8N0JakEdRRsjvgNKavaFl3OqttQCpGTTc0vPUbIfR6ZTtf1g\r\n2mX7ZDxz9nH7ZJAocwzKKdqARDjuJ/plMIyvdq1PAPvKNH69riq0DygH3wYMvneIgfGWpBMC+Aym\r\n+n53Rv3pX3ax+uE/9wGZ7lDonBGxraehtm89S15de+YMcmEtOfK2kltmchw3vBQ03rRS09JCZAck\r\nhuCSLB79Nia7ly/z2c3v7fpVluMBtn2EoFmk9LwGUD6zLcBDEoKQR5MxeldzgPXmTkhQvEMIop7A\r\nIAyr3a+6hAjQjdMhr75f3FeR6LQ11n3gukG+k2MDJAM43T5a13E6LgkK6kJIGeIj1YUBMDsLxBW2\r\n+jkf4ZDwf2Yke5g8TNIoHYweopQsFtGsmKfRqEjGw8XtYj5fJH/5c5N0Wouy5Mofcyy+JH2T2Y1g\r\nRltduRummxjSSzB+LEAIUkJO5We1FKWH85SsWa/m0qAtlTkuwtcrf7YsvqQRMhx8uXIpGaTkfpBF\r\nxWgyjtIiHUbZmEwikmT32YikWbooLl16FIr/uEtol+NsOBiGKJ2RvvKNhO+tb3TaCMcNkqLJMdQd\r\nfF3u+kR8UGUIraNCdv0zKTz9kxQQ7mOgoWv7C8Ptl6HQ3P5elwcv2Ap+IXmNhuSCqoLLGDq1Nl8x\r\n2sEVm2P7x4YajpH8RUEdZEmawjIXBulwPICBOZ9Znc9QxQAqxw6jrjt3MIItm9aIdQ0nJUEmpWdQ\r\nNJXoE7rj5NlJ65buIHnwOjAHR1BDzWOAgc5z6FC5hgeEORPgpFq2LGRTy56Y65IpIeNey1AJpxX3\r\nvDqudbZbGyQPsrHT7KwKGWYB87jO34tdDYI1zMOmXmrjQQ2wlXC95Jir6PMSXrOv4HMC2/wsryoo\r\n566OwVfqhELu0PKKMrhH5lSKlREYtVRpCwa42goyhNb/peTWtzArHKsL2gjp72cwsJoay0M4g2yc\r\n/g+gzJ6BvoiGW/SR79Czbqi6YDwgI2A6BL6eObwfV4wTeOIvGYOEIJvXx919+9u/XqC7N4YWksBb\r\nXvN3Y/my9S9DF4cuwWGFf1Diq4c4bO3/cfCv/fn47h8AAAD//wMAUEsDBBQABgAIAAAAIQCRLWpJ\r\nWAYAAA8aAAAaAAAAY2xpcGJvYXJkL3RoZW1lL3RoZW1lMS54bWzsWUtvGzcQvhfof1jsvbHeio3I\r\nga1H3MROgkhJkSOlpXYZc5cLkrKjW5GceilQIC16aIDeeiiKBmiABr30xxhw0KY/okPuQ6RExQ+4\r\nQFDEAozd2W+Gw5nZb0jujZtPY+odYS4ISzp+9VrF93AyYQFJwo7/cDT47LrvCYmSAFGW4I4/x8K/\r\nuf3pJzfQ1oSSdMwQD0YRjrEHhhKxhTp+JGW6tbEhJiBG4hpLcQLPpozHSMItDzcCjo5hgJhu1CqV\r\n1kaMSOJvg0WpDPUp/EukUIIJ5UNlBnsJimH0e9MpmWCNDQ6rCiHmoku5d4RoxwebATse4afS9ygS\r\nEh50/Ir+8ze2b2ygrVyJyjW6ht5A/+V6uUJwWNNj8nBcDtpoNButndK+BlC5iuu3+61+q7SnAWgy\r\ngZlmvpg2m7ubu71mjjVA2aXDdq/dq1ctvGG/vuLzTlP9LLwGZfYbK/jBoAtRtPAalOGbK/hGo13r\r\nNiy8BmX41gq+XdnpNdoWXoMiSpLDFXSl2ap3i9mWkCmje074ZrMxaNdy4wsUVENZXWqIKUvkulqL\r\n0RPGBwBQQIokSTw5T/EUTaAmu4iSMSfePgkjKLwUJUyAuFKrDCp1+K9+DX2lI4K2MDK0lV/giVgR\r\nKX88MeEklR3/Nlj1Dcjpmzcnz16fPPv95Pnzk2e/5mNrU5beHkpCU+/dT9/88/JL7+/ffnz34tts\r\n6GW8MPFvf/nq7R9/vs88zHgRitPvXr19/er0+6//+vmFw/oOR2MTPiIxFt5dfOw9YDFM0OE/HvOL\r\naYwiREyNnSQUKEFqFIf9vows9N05osiB28V2HB9xoBoX8NbsieXwMOIzSRwW70SxBTxgjO4y7ozC\r\nHTWWEebRLAndg/OZiXuA0JFr7C5KrCz3ZylwLHGZ7EbYcvM+RYlEIU6w9NQzdoixY3aPCbHiekAm\r\nnAk2ld5j4u0i4gzJiIytaloo7ZEY8jJ3OQj5tmJz8MjbZdQ16x4+spHwbiDqcH6EqRXGW2gmUewy\r\nOUIxNQO+j2TkcnI45xMT1xcSMh1iyrx+gIVw6dzjMF8j6XeAZtxpP6Dz2EZySQ5dNvcRYyayxw67\r\nEYpTF3ZIksjEfi4OoUSRd59JF/yA2W+Iuoc8oGRtuh8RbKX7bDZ4CAxrurQoEPVkxh25vIWZVb/D\r\nOZ0irKkGGoDF6zFJziT5JXpv/nf0DiR6+sNLx4yuhtLdhq18XJDMdzhxvk17SxS+DrdM3F3GA/Lh\r\n83YPzZL7GF6V1eb1kbY/0rb/v6ftde/z1ZP1gp+ButWyNVuu68V7vHbtPiWUDuWc4n2hl+8CulIw\r\nAKHS03tUXO7l0ggu1ZsMA1i4kCOt43EmvyAyGkYohTV+1VdGQpGbDoWXMgFLfy122lZ4OosPWJBt\r\nWatVtT3NyEMguZBXmqUcthsyQ7fai21YaV57G+rtcuGA0r2IE8ZgthN1hxPtQqiCpDfnEDSHE3pm\r\nV+LFpsOL68p8kaoVL8C1MiuwbPJgsdXxmw1QASXYVSGKA5WnLNVFdnUyrzLT64JpVQCsIYoKWGR6\r\nU/m6dnpqdlmpnSPTlhNGudlO6MjoHiYiFOC8OpX0PG5cNNebi5Ra7qlQ6PGgtBZutK+/z4vL5hr0\r\nlrmBJiZT0MQ77vitehNKZoLSjj+FrT9cxinUjlDLXURDODSbSJ698JdhlpQL2UMiygKuSSdjg5hI\r\nzD1K4o6vpl+mgSaaQ7Rv1RoQwgfr3CbQyofmHCTdTjKeTvFEmmk3JCrS2S0wfMYVzqda/fJgpclm\r\nkO5hFBx7YzrjDxCUWLNdVQEMiIAToGoWzYDAkWZJZIv6W2pMOe2aZ4q6hjI5ommE8o5iknkG11Re\r\nuqPvyhgYd/mcIaBGSPJGOA5VgzWDanXTsmtkPqztumcrqcgZpLnomRarqK7pZjFrhKINLMXyck3e\r\n8KoIMXCa2eEz6l6m3M2C65bWCWWXgICX8XN03XM0BMO1xWCWa8rjVRpWnJ1L7d5RTPAM187TJAzW\r\nbxVml+JW9gjncCC8VOcHveWqBdG0WFfqSLs+Txyg1BuH1Y4PnwjgbOIpXMFHBh9kNSWrKRlcwZcD\r\naBfZcX/Hzy8KCTzPJCWmXkjqBaZRSBqFpFlImoWkVUhavqfPxeFbjDoS973i2Bt6WH5Mnq8t7G84\r\n2/8CAAD//wMAUEsDBBQABgAIAAAAIQCcZkZBuwAAACQBAAAqAAAAY2xpcGJvYXJkL2RyYXdpbmdz\r\nL19yZWxzL2RyYXdpbmcxLnhtbC5yZWxzhI/NCsIwEITvgu8Q9m7SehCRJr2I0KvUBwjJNi02PyRR\r\n7Nsb6EVB8LIws+w3s037sjN5YkyTdxxqWgFBp7yenOFw6y+7I5CUpdNy9g45LJigFdtNc8VZ5nKU\r\nxikkUigucRhzDifGkhrRykR9QFc2g49W5iKjYUGquzTI9lV1YPGTAeKLSTrNIXa6BtIvoST/Z/th\r\nmBSevXpYdPlHBMulFxagjAYzB0pXZ501LV2BiYZ9/SbeAAAA//8DAFBLAQItABQABgAIAAAAIQC7\r\n5UiUBQEAAB4CAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAG\r\nAAgAAAAhAK0wP/HBAAAAMgEAAAsAAAAAAAAAAAAAAAAANgEAAF9yZWxzLy5yZWxzUEsBAi0AFAAG\r\nAAgAAAAhAHXds8bNAwAAjQgAAB8AAAAAAAAAAAAAAAAAIAIAAGNsaXBib2FyZC9kcmF3aW5ncy9k\r\ncmF3aW5nMS54bWxQSwECLQAUAAYACAAAACEAkS1qSVgGAAAPGgAAGgAAAAAAAAAAAAAAAAAqBgAA\r\nY2xpcGJvYXJkL3RoZW1lL3RoZW1lMS54bWxQSwECLQAUAAYACAAAACEAnGZGQbsAAAAkAQAAKgAA\r\nAAAAAAAAAAAAAAC6DAAAY2xpcGJvYXJkL2RyYXdpbmdzL19yZWxzL2RyYXdpbmcxLnhtbC5yZWxz\r\nUEsFBgAAAAAFAAUAZwEAAL0NAAAAAA==\r\n" stroked="f" style="width:24pt; height:24pt; v-text-anchor:top"> <o:lock aspectratio="t" v:ext="edit"> <v:textbox>\r\n<table width="100%">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<div>&nbsp;</div>\r\n\r\n\t\t\t<div style="text-align: center;">&nbsp;</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</v:textbox></o:lock></v:rect></span></span></span><img height="188" src="/media/uploads/2020/07/01/image_q8lBbKA.png" width="284" />&nbsp;<img height="189" src="/media/uploads/2020/07/01/image_ejCJ0bl.png" width="287" /><span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><v:rect alt="/media/uploads/2020/06/09/6_70wTkCA.jpg" filled="f" o:gfxdata="UEsDBBQABgAIAAAAIQC75UiUBQEAAB4CAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbKSRvU7DMBSF\r\ndyTewfKKEqcMCKEmHfgZgaE8wMW+SSwc27JvS/v23KTJgkoXFsu+P+c7Ol5vDoMTe0zZBl/LVVlJ\r\ngV4HY31Xy4/tS3EvRSbwBlzwWMsjZrlprq/W22PELHjb51r2RPFBqax7HCCXIaLnThvSAMTP1KkI\r\n+gs6VLdVdad08ISeCho1ZLN+whZ2jsTzgcsnJwldluLxNDiyagkxOquB2Knae/OLUsyEkjenmdzb\r\nmG/YhlRnCWPnb8C898bRJGtQvEOiVxjYhtLOxs8AySiT4JuDystlVV4WPeM6tK3VaILeDZxIOSsu\r\nti/jidNGNZ3/J08yC1dNv9v8AAAA//8DAFBLAwQUAAYACAAAACEArTA/8cEAAAAyAQAACwAAAF9y\r\nZWxzLy5yZWxzhI/NCsIwEITvgu8Q9m7TehCRpr2I4FX0AdZk2wbbJGTj39ubi6AgeJtl2G9m6vYx\r\njeJGka13CqqiBEFOe2Ndr+B03C3WIDihMzh6RwqexNA281l9oBFTfuLBBhaZ4ljBkFLYSMl6oAm5\r\n8IFcdjofJ0z5jL0MqC/Yk1yW5UrGTwY0X0yxNwri3lQgjs+Qk/+zfddZTVuvrxO59CNCmoj3vCwj\r\nMfaUFOjRhrPHaN4Wv0VV5OYgm1p+LW1eAAAA//8DAFBLAwQUAAYACAAAACEAdd2zxs0DAACNCAAA\r\nHwAAAGNsaXBib2FyZC9kcmF3aW5ncy9kcmF3aW5nMS54bWy0Vttu4zYQfS/QfyD4rohy5IuMVRaO\r\nHS0KpLtBnH0uaIqy2KVIlaRvW/Rf+i37ZR1ScnxJ0YddVAFockgenjkzQ+bd+30j0ZYbK7TKcXJD\r\nMOKK6VKodY4/vxTRBCPrqCqp1Irn+MAtfn/380/v6HRtaFsLhgBB2SnNce1cO41jy2reUHujW65g\r\nrtKmoQ6GZh2Xhu4AuZHxgJBR3FCh8N0JakEdRRsjvgNKavaFl3OqttQCpGTTc0vPUbIfR6ZTtf1g\r\n2mX7ZDxz9nH7ZJAocwzKKdqARDjuJ/plMIyvdq1PAPvKNH69riq0DygH3wYMvneIgfGWpBMC+Aym\r\n+n53Rv3pX3ax+uE/9wGZ7lDonBGxraehtm89S15de+YMcmEtOfK2kltmchw3vBQ03rRS09JCZAck\r\nhuCSLB79Nia7ly/z2c3v7fpVluMBtn2EoFmk9LwGUD6zLcBDEoKQR5MxeldzgPXmTkhQvEMIop7A\r\nIAyr3a+6hAjQjdMhr75f3FeR6LQ11n3gukG+k2MDJAM43T5a13E6LgkK6kJIGeIj1YUBMDsLxBW2\r\n+jkf4ZDwf2Yke5g8TNIoHYweopQsFtGsmKfRqEjGw8XtYj5fJH/5c5N0Wouy5Mofcyy+JH2T2Y1g\r\nRltduRummxjSSzB+LEAIUkJO5We1FKWH85SsWa/m0qAtlTkuwtcrf7YsvqQRMhx8uXIpGaTkfpBF\r\nxWgyjtIiHUbZmEwikmT32YikWbooLl16FIr/uEtol+NsOBiGKJ2RvvKNhO+tb3TaCMcNkqLJMdQd\r\nfF3u+kR8UGUIraNCdv0zKTz9kxQQ7mOgoWv7C8Ptl6HQ3P5elwcv2Ap+IXmNhuSCqoLLGDq1Nl8x\r\n2sEVm2P7x4YajpH8RUEdZEmawjIXBulwPICBOZ9Znc9QxQAqxw6jrjt3MIItm9aIdQ0nJUEmpWdQ\r\nNJXoE7rj5NlJ65buIHnwOjAHR1BDzWOAgc5z6FC5hgeEORPgpFq2LGRTy56Y65IpIeNey1AJpxX3\r\nvDqudbZbGyQPsrHT7KwKGWYB87jO34tdDYI1zMOmXmrjQQ2wlXC95Jir6PMSXrOv4HMC2/wsryoo\r\n566OwVfqhELu0PKKMrhH5lSKlREYtVRpCwa42goyhNb/peTWtzArHKsL2gjp72cwsJoay0M4g2yc\r\n/g+gzJ6BvoiGW/SR79Czbqi6YDwgI2A6BL6eObwfV4wTeOIvGYOEIJvXx919+9u/XqC7N4YWksBb\r\nXvN3Y/my9S9DF4cuwWGFf1Diq4c4bO3/cfCv/fn47h8AAAD//wMAUEsDBBQABgAIAAAAIQCRLWpJ\r\nWAYAAA8aAAAaAAAAY2xpcGJvYXJkL3RoZW1lL3RoZW1lMS54bWzsWUtvGzcQvhfof1jsvbHeio3I\r\nga1H3MROgkhJkSOlpXYZc5cLkrKjW5GceilQIC16aIDeeiiKBmiABr30xxhw0KY/okPuQ6RExQ+4\r\nQFDEAozd2W+Gw5nZb0jujZtPY+odYS4ISzp+9VrF93AyYQFJwo7/cDT47LrvCYmSAFGW4I4/x8K/\r\nuf3pJzfQ1oSSdMwQD0YRjrEHhhKxhTp+JGW6tbEhJiBG4hpLcQLPpozHSMItDzcCjo5hgJhu1CqV\r\n1kaMSOJvg0WpDPUp/EukUIIJ5UNlBnsJimH0e9MpmWCNDQ6rCiHmoku5d4RoxwebATse4afS9ygS\r\nEh50/Ir+8ze2b2ygrVyJyjW6ht5A/+V6uUJwWNNj8nBcDtpoNButndK+BlC5iuu3+61+q7SnAWgy\r\ngZlmvpg2m7ubu71mjjVA2aXDdq/dq1ctvGG/vuLzTlP9LLwGZfYbK/jBoAtRtPAalOGbK/hGo13r\r\nNiy8BmX41gq+XdnpNdoWXoMiSpLDFXSl2ap3i9mWkCmje074ZrMxaNdy4wsUVENZXWqIKUvkulqL\r\n0RPGBwBQQIokSTw5T/EUTaAmu4iSMSfePgkjKLwUJUyAuFKrDCp1+K9+DX2lI4K2MDK0lV/giVgR\r\nKX88MeEklR3/Nlj1Dcjpmzcnz16fPPv95Pnzk2e/5mNrU5beHkpCU+/dT9/88/JL7+/ffnz34tts\r\n6GW8MPFvf/nq7R9/vs88zHgRitPvXr19/er0+6//+vmFw/oOR2MTPiIxFt5dfOw9YDFM0OE/HvOL\r\naYwiREyNnSQUKEFqFIf9vows9N05osiB28V2HB9xoBoX8NbsieXwMOIzSRwW70SxBTxgjO4y7ozC\r\nHTWWEebRLAndg/OZiXuA0JFr7C5KrCz3ZylwLHGZ7EbYcvM+RYlEIU6w9NQzdoixY3aPCbHiekAm\r\nnAk2ld5j4u0i4gzJiIytaloo7ZEY8jJ3OQj5tmJz8MjbZdQ16x4+spHwbiDqcH6EqRXGW2gmUewy\r\nOUIxNQO+j2TkcnI45xMT1xcSMh1iyrx+gIVw6dzjMF8j6XeAZtxpP6Dz2EZySQ5dNvcRYyayxw67\r\nEYpTF3ZIksjEfi4OoUSRd59JF/yA2W+Iuoc8oGRtuh8RbKX7bDZ4CAxrurQoEPVkxh25vIWZVb/D\r\nOZ0irKkGGoDF6zFJziT5JXpv/nf0DiR6+sNLx4yuhtLdhq18XJDMdzhxvk17SxS+DrdM3F3GA/Lh\r\n83YPzZL7GF6V1eb1kbY/0rb/v6ftde/z1ZP1gp+ButWyNVuu68V7vHbtPiWUDuWc4n2hl+8CulIw\r\nAKHS03tUXO7l0ggu1ZsMA1i4kCOt43EmvyAyGkYohTV+1VdGQpGbDoWXMgFLfy122lZ4OosPWJBt\r\nWatVtT3NyEMguZBXmqUcthsyQ7fai21YaV57G+rtcuGA0r2IE8ZgthN1hxPtQqiCpDfnEDSHE3pm\r\nV+LFpsOL68p8kaoVL8C1MiuwbPJgsdXxmw1QASXYVSGKA5WnLNVFdnUyrzLT64JpVQCsIYoKWGR6\r\nU/m6dnpqdlmpnSPTlhNGudlO6MjoHiYiFOC8OpX0PG5cNNebi5Ra7qlQ6PGgtBZutK+/z4vL5hr0\r\nlrmBJiZT0MQ77vitehNKZoLSjj+FrT9cxinUjlDLXURDODSbSJ698JdhlpQL2UMiygKuSSdjg5hI\r\nzD1K4o6vpl+mgSaaQ7Rv1RoQwgfr3CbQyofmHCTdTjKeTvFEmmk3JCrS2S0wfMYVzqda/fJgpclm\r\nkO5hFBx7YzrjDxCUWLNdVQEMiIAToGoWzYDAkWZJZIv6W2pMOe2aZ4q6hjI5ommE8o5iknkG11Re\r\nuqPvyhgYd/mcIaBGSPJGOA5VgzWDanXTsmtkPqztumcrqcgZpLnomRarqK7pZjFrhKINLMXyck3e\r\n8KoIMXCa2eEz6l6m3M2C65bWCWWXgICX8XN03XM0BMO1xWCWa8rjVRpWnJ1L7d5RTPAM187TJAzW\r\nbxVml+JW9gjncCC8VOcHveWqBdG0WFfqSLs+Txyg1BuH1Y4PnwjgbOIpXMFHBh9kNSWrKRlcwZcD\r\naBfZcX/Hzy8KCTzPJCWmXkjqBaZRSBqFpFlImoWkVUhavqfPxeFbjDoS973i2Bt6WH5Mnq8t7G84\r\n2/8CAAD//wMAUEsDBBQABgAIAAAAIQCcZkZBuwAAACQBAAAqAAAAY2xpcGJvYXJkL2RyYXdpbmdz\r\nL19yZWxzL2RyYXdpbmcxLnhtbC5yZWxzhI/NCsIwEITvgu8Q9m7SehCRJr2I0KvUBwjJNi02PyRR\r\n7Nsb6EVB8LIws+w3s037sjN5YkyTdxxqWgFBp7yenOFw6y+7I5CUpdNy9g45LJigFdtNc8VZ5nKU\r\nxikkUigucRhzDifGkhrRykR9QFc2g49W5iKjYUGquzTI9lV1YPGTAeKLSTrNIXa6BtIvoST/Z/th\r\nmBSevXpYdPlHBMulFxagjAYzB0pXZ501LV2BiYZ9/SbeAAAA//8DAFBLAQItABQABgAIAAAAIQC7\r\n5UiUBQEAAB4CAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAG\r\nAAgAAAAhAK0wP/HBAAAAMgEAAAsAAAAAAAAAAAAAAAAANgEAAF9yZWxzLy5yZWxzUEsBAi0AFAAG\r\nAAgAAAAhAHXds8bNAwAAjQgAAB8AAAAAAAAAAAAAAAAAIAIAAGNsaXBib2FyZC9kcmF3aW5ncy9k\r\ncmF3aW5nMS54bWxQSwECLQAUAAYACAAAACEAkS1qSVgGAAAPGgAAGgAAAAAAAAAAAAAAAAAqBgAA\r\nY2xpcGJvYXJkL3RoZW1lL3RoZW1lMS54bWxQSwECLQAUAAYACAAAACEAnGZGQbsAAAAkAQAAKgAA\r\nAAAAAAAAAAAAAAC6DAAAY2xpcGJvYXJkL2RyYXdpbmdzL19yZWxzL2RyYXdpbmcxLnhtbC5yZWxz\r\nUEsFBgAAAAAFAAUAZwEAAL0NAAAAAA==\r\n" stroked="f" style="width:24pt; height:24pt; v-text-anchor:top"><o:lock aspectratio="t" v:ext="edit"><v:textbox>\r\n<table width="100%">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<div style="text-align: center;">&nbsp;</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</v:textbox></o:lock></v:rect></span></span></span>\r\n\r\n<div style="text-align: center;">&nbsp;</div>\r\n\r\n<div style="text-align: center;">&nbsp;</div>\r\n\r\n<div style="text-align: center;">&nbsp;</div>\r\n\r\n<div style="text-align: center;">&nbsp;</div>\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Cơ sở đo lường - Thử nghiệm</span></span></b></span></span></span><br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Ng&agrave;y 03 th&aacute;ng 8 năm 2018, Viện Thiết kế t&agrave;u qu&acirc;n sựu được Cục TC-ĐL-CL/BTTM c&ocirc;ng nhận năng lực kiểm định phương tiện đo, thử nghiệm vũ kh&iacute; trang bị (Bao gồm 05 lĩnh vực kiểm định; 11 lĩnh vực thử nghiệm).</span></span></span></span></span><br />\r\n<img height="261" src="/media/uploads/2020/07/01/image_Zx1uFwH.png" width="346" />&nbsp; &nbsp; &nbsp;<img height="257" src="/media/uploads/2020/07/01/image_6RMpCnT.png" width="348" /><br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><b>C&aacute;c thiết bị kiểm tra t&iacute;n hiệu cao tần<i>&nbsp;</i></b></span></span></span></span></span><br />\r\n<img height="409" src="/media/uploads/2020/07/01/image_QQCcNCF.png" width="306" />&nbsp; &nbsp;<img height="399" src="/media/uploads/2020/07/01/image_iISC7UA.png" width="224" /><br />\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><b>Ph&ograve;ng kiểm định c&aacute;c phương tiện đo điện v&agrave; cơ kh&iacute;&nbsp;</b></span></span></span></span></span>\r\n\r\n<div style="text-align: center;"><img height="849" src="/media/uploads/2020/07/01/image_3fxbv8E.png" width="676" /></div>	2020-07-01 16:10:20.031007+00	cơ_sở_hạ_tầng__BJVSTU7.jpg	14
4	Lịch sử phát triển	<div style="text-align: center;"><img height="379" src="/media/uploads/2020/07/01/image_wznkJBY.png" width="654" /><br />\r\n&nbsp;</div>\r\n<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:13.5pt"><span style="font-family:&quot;Myriad-Pro Bold&quot;,serif"><span style="color:#e74c3c">Thực hiện Nghị quyết của Bộ Ch&iacute;nh tri ̣về xây dựng và phát triển công nghiệp quốc phòng để bảo đảm kỹ thuật, công nghệ đồng bộ cho thiết kế, chế tạo các loại tàu quân sự được chủ động, sát với yêu cầu kỹ chiến thuật, điều kiện tác chiến của Quân đội trong sự nghiệp xây dựng bảo vệ Tổ quốc, Bộ trưởng Bộ Quốc phòng đã ký quyết định số 849/QĐ-BQP thành lập Viện Thiết kế tàu quân sự.</span></span></span></b></span></span></span>	2020-07-01 16:14:00.122163+00	lịch_sử_.jpg	10
5	Lãnh đạo viên	<span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Viện trưởng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ch&iacute;nh trị Vi&ecirc;n<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{ảnh 3x4} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{ảnh 3x4}<br />\r\nTrung t&aacute; TS.Phạm Quang Chiến&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Thượng t&aacute; Ths. Hồ Văn Ch&acirc;u<br />\r\n<br />\r\n<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ph&oacute; Viện trưởng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ph&oacute; Viện trưởng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ph&oacute; Viện trưởng<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{ảnh 3x4}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{ảnh 3x4}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {ảnh 3x4}<br />\r\nTrung t&aacute; TS. Phạm Th&agrave;nh Trung&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; Đại t&aacute; TS.Lương Lục Quỳnh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Đại t&aacute; Ths. Nguyễn Đ&igrave;nh T&ugrave;ng</span></span></span></span></span><br />\r\n&nbsp;	2020-07-01 16:14:41.85033+00	lãnh_đạo_viện__N0fUzXt.jpg	9
6	AVEVA Initial Design	AVEVA Initial Design là bộ ứng dụng dùng trong giai đoạn thiết kế sơ bộ, bao gồm 5 môđun. Lines xây dựng và chỉnh trơn tuyến h&igrave;nh, Surface tạo bề mặt vỏ tàu, Compartment mô h&igrave;nh hóa khoang két, Hydrostatics, Hydrodynamics t&iacute;nh toán kiểm tra các yếu tố t&iacute;nh năng.	2020-07-01 16:19:40.595899+00	aveva_Jm4Ip1L.png	6
7	AVEVA Hull Structural Design	Hull Structural Design là ứng dụng dùng để xác định sơ bộ và triển khai kết cấu cơ bản của tàu. Nó cho phép xây dựng các bản vẽ tiêu chuẩn, ước lượng sơ bộ vật liệu thân tàu, chiều dài đường hàn, khối lượng tàu và toạ độ trọng tâm tàu	2020-07-01 16:20:45.449469+00	avevahull_D2MauMN.png	6
8	AVEVA Outfitting	Outfiting c&oacute; c&aacute;c m&ocirc;đun d&ugrave;ng để thiết kế, lắp đặt c&aacute;c hệ thống trang thiết bị, ống, HVAC, c&aacute;c kết cấu th&eacute;p đa dạng&nbsp;v&agrave; m&aacute;ng dẫn c&aacute;p điện	2020-07-01 16:22:12.563593+00	avevaout_NUqEK1a.png	6
9	Rhinoceros	Phần mềm cho ph&eacute;p m&ocirc; phỏng nhanh ch&oacute;ng, ch&iacute;nh x&aacute;c h&igrave;nh d&aacute;ng th&acirc;n t&agrave;u, bố tr&iacute; ch&uacute;ng to&agrave;n t&agrave;u trong kh&ocirc;ng gian 3D.&nbsp;M&ocirc; h&igrave;nh 3D giúp người thiết kế kiểm soát được các yếu tố bố tr&iacute; và giúp khách hàng nắm bắt được thiết kế đầy đủ, trực quan hơn.	2020-07-01 16:24:54.892846+00	rhinoceros.png	6
\.


--
-- Data for Name: Product_product; Type: TABLE DATA; Schema: public; Owner: ckcvn
--

COPY public."Product_product" (id, title, image, content, code, created_date, updated_at, category_id) FROM stdin;
2	Tàu Trinh sát 500cv	tàu_trinh_sát_500cv.jpg	Đang cập nhật&nbsp;	500cv	2020-06-07 14:01:52.721362+00	2020-06-07 14:01:52.721403+00	2
3	Tàu trinh sát 40M	tàu_chinh_sát_40m.jpg		40M	2020-06-07 14:26:48.649048+00	2020-06-07 14:26:48.64908+00	2
4	Tàu tuần tra cao tốc 120 tấn	tau_tuần_tra_cao_thốc_120_tấn.jpg		120 tấn	2020-06-07 14:29:25.448689+00	2020-06-07 14:30:15.720721+00	2
5	Tàu tuần tra cao tốc 75 tấn	tàu_tuần_tra_cao_tốc_75_tấn.jpg		75 tấn	2020-06-07 14:32:36.699015+00	2020-06-07 14:32:36.699049+00	2
7	Tàu kéo 1200cv	tàu_kéo_1200cv.jpg		1200cv	2020-06-07 14:38:04.98208+00	2020-06-07 14:38:04.982125+00	2
8	Tàu vận tải đa năng 150 tấn	tàu_vận_tải_đa_năng_150_tấn.jpg		150 tấn	2020-06-07 14:40:14.595718+00	2020-06-07 14:40:14.595756+00	2
9	Xuồng tenner vỏ hợp kim nhôm	xuồng_tender_vỏ_hợp_kim_nhôm.jpg		z114	2020-06-07 14:42:28.673098+00	2020-06-07 14:42:28.673134+00	2
10	Xuồng công tác vỏ hợp kim nhôm	Xuồng_công_tác_vỏ_hợp_kim_nhôm.jpg		z114	2020-06-07 14:44:46.478641+00	2020-06-07 14:44:46.478678+00	2
11	Nhà giàn tự nâng	nhà_giàn_tự_nâng.jpg		z114	2020-06-07 14:47:10.863281+00	2020-06-07 14:47:10.863317+00	3
12	Tàu kéo 1000cv	tàu_kéo_1000_cv.jpg		1000cv	2020-06-07 14:49:24.38117+00	2020-06-07 14:49:24.381208+00	3
13	Tàu trinh sát 24M	tàu_trinh_sát_24_m.jpg		24M	2020-06-07 14:51:15.933484+00	2020-06-07 14:51:15.933521+00	3
14	Tàu trinh sát 29M	tau_trinh_sát_29m.jpg		29M	2020-06-07 14:53:11.863085+00	2020-06-07 14:53:11.863121+00	3
16	Tàu tuần tiễu pháo	tuần_tuần__tiễu_pháo.jpg		z114	2020-06-07 14:57:22.639354+00	2020-06-07 14:57:22.639405+00	3
17	Tàu ngầm 100 tán	tàu_ngầm_100_tấn.jpg		100 tấn	2020-06-07 15:01:00.087806+00	2020-06-07 15:01:00.087856+00	3
18	Tàu 480 khách	tàu_480_khách.jpg		480	2020-06-07 15:03:52.581151+00	2020-06-07 15:03:52.581192+00	3
19	Xuồng cao tốc XVT01	xuồng_cao_tốc_xvt_01.jpg		XVT01	2020-06-07 15:06:05.210291+00	2020-06-07 15:06:05.210326+00	3
15	Tàu tuần tra cao tốc 37 Hải lý/giờ	tàu_tuần_tra_cao_tốc_37_hải_lý_giờ.jpg		37	2020-06-07 14:55:19.27696+00	2020-06-07 15:06:23.408371+00	3
6	Tàu tuần tra cao tốc 130 tấn	tàu_tuần_tra_cao_tốc_130_tấn_.jpg		130 tấn	2020-06-07 14:35:05.099082+00	2020-06-07 15:06:46.104064+00	2
20	Xuồng cứu hộ	xuồng_cứu_hộ.jpg		z114	2020-06-07 15:12:10.762222+00	2020-06-07 15:12:10.76226+00	3
21	Bộ lò xo dạng đĩa côn	bộ_lò_so_dnagj_đĩa_côn.jpg		z114	2020-06-07 15:16:58.998221+00	2020-06-07 15:16:58.998257+00	3
22	Bàn phím Kasu	bàn_phím_kasu.jpg	Chức năng: B&agrave;n ph&iacute;m nhập dữ liệu hệ thống điều khiển KASU (k&yacute; hiệu BP-01) được Viện Thiết kế t&agrave;u qu&acirc;n<br />\r\nsự nghi&ecirc;n cứu, ph&aacute;t triển nhằm thay thế cho b&agrave;n ph&iacute;m КЛ-85 do LB Nga sản xuất đang sử dụng<br />\r\ntr&ecirc;n c&aacute;c t&agrave;u M, Gepard, PS-500. B&agrave;n ph&iacute;m BP-01 c&oacute; h&igrave;nh d&aacute;ng, k&iacute;ch thước v&agrave; số lượng ph&iacute;m<br />\r\ntương đương với b&agrave;n ph&iacute;m КЛ-85, hoạt động ổn định theo đ&uacute;ng chức năng ở c&aacute;c chế độ l&agrave;m<br />\r\nviệc của hệ thống trong c&aacute;c điều kiện khai th&aacute;c sử dụng tr&ecirc;n t&agrave;u, đạt y&ecirc;u cầu kỹ thuật theo ti&ecirc;u<br />\r\nchuẩn qu&acirc;n sự của Bộ Quốc ph&ograve;ng.<br />\r\n&nbsp;	z114	2020-06-07 15:20:57.233423+00	2020-06-07 15:21:23.31945+00	3
23	Máy định vị hàng hải GPS/GLONASS	máy_định_vị_hàng_hải.jpg	Chức năng : Định vị dẫn đường , x&aacute;c định toạ độ t&agrave;u qu&acirc;n sự sử dụng đồng thời hoặc ri&ecirc;ng rẽ hai hệ thống định vị vệ tinh GPS của Mỹ v&agrave; GLONASS của LB Nga .	z114	2020-06-07 15:34:36.197133+00	2020-06-07 15:34:36.19717+00	3
24	Bộ điều khiển hướng dòng cửa hút của động cơ tua bin khí	bộ_điều_khiển_cánh_hướng.jpg	Chức năng: Xoay c&aacute;nh hướng d&ograve;ng đến một g&oacute;c cần thiết theo chế độ c&ocirc;ng suất của động cơ nhằm mục<br />\r\nđ&iacute;ch tạo sự ổn định kh&iacute; động học, n&acirc;ng cao hiệu suất cho buồng đốt v&agrave; giảm ứng suất động cho c&aacute;c<br />\r\nbộ phận của động cơ.<br />\r\n&nbsp;	DR76	2020-06-07 15:39:37.74261+00	2020-06-07 15:39:37.742658+00	3
25	Thiết bị báo nước	thiết_bị_báo_nước.jpg	Chức năng: Kiểm tra, gi&aacute;m s&aacute;t v&agrave; cảnh b&aacute;o khi c&oacute; nước ngập trong c&aacute;c khoang t&agrave;u<br />\r\n&nbsp;	z114	2020-06-07 15:44:17.462205+00	2020-06-07 15:44:17.462247+00	3
\.


--
-- Data for Name: addslide; Type: TABLE DATA; Schema: public; Owner: ckcvn
--

COPY public.addslide (id, name, link, content1, content2, image, active, slide_id) FROM stdin;
15	slide 1	https://stackoverflow.com/	\N	\N	banner_JoQCuL6.jpg	t	1
16	slide 2	https://stackoverflow.com/	\N	\N	tau_tuan_tra_cao_tốc_120_tan_pSyTgaq.jpg	t	1
17	slide 3	https://stackoverflow.com/	\N	\N	xuong_cuu_nan_glTVVPI.jpg	t	1
\.


--
-- Data for Name: article_category; Type: TABLE DATA; Schema: public; Owner: ckcvn
--

COPY public.article_category (id, image, slug, title, descriptions, created_date, updated_date, lft, rght, tree_id, level, parent_id) FROM stdin;
1		tin-tuc-su-kien	Tin tức & sự kiện	Tin tức & sự kiện	2020-07-01 15:51:02.290035+00	\N	1	8	3	0	\N
2		tin-tuc-su-kien/su-kien-va-hoat-dong	Sự kiện và hoạt động	Sự kiện và hoạt động	2020-07-01 15:51:02.292092+00	\N	4	5	3	1	1
3		tin-tuc-su-kien/tin-tuc-nganh	Tin tức ngành	Tin tức ngành	2020-07-01 15:51:02.293884+00	\N	2	3	3	1	1
4		khoa-hoc-va-cong-nghe	Khoa học và công nghệ	Khoa học và công nghệ	2020-07-01 15:51:02.29566+00	\N	1	12	1	0	\N
6		khoa-hoc-va-cong-nghe/giai-phap-va-cong-nghe-thiet-ke-tau	Giải pháp và công nghệ thiết kế tàu	Giải pháp và công nghệ thiết kế tàu	2020-07-01 15:51:02.297421+00	\N	2	3	1	1	4
7		khoa-hoc-va-cong-nghe/nghien-cuu-trao-doi	Nghiên cứu - trao đổi	Nghiên cứu - trao đổi	2020-07-01 15:51:02.299221+00	\N	4	5	1	1	4
5		khoa-hoc-va-cong-nghe/hoat-dong-hop-tac	Hoạt động hợp tác	Hoạt động hợp tác	2020-07-01 15:51:02.300949+00	\N	6	7	1	1	4
8		gioi-thieu	Giới thiệu	Giới thiệu	2020-07-01 15:51:02.302618+00	\N	1	10	2	0	\N
9		gioi-thieu/lanh-dao-vien	Lãnh đạo viện	Lãnh đạo viện	2020-07-01 15:51:02.304303+00	\N	12	13	2	1	8
10		gioi-thieu/lich-su	Lịch sử phát triển	Lịch sử phát triển	2020-07-01 15:51:02.306074+00	\N	2	3	2	1	8
11		gioi-thieu/chuc-nang-nhiem-vu	Chức năng - Nhiệm vụ	Chức năng - Nhiệm vụ	2020-07-01 15:51:02.307815+00	\N	4	5	2	1	8
12		gioi-thieu/nang-luc	Năng lực	Năng lực	2020-07-01 15:51:02.309593+00	\N	6	1	2	1	8
13		gioi-thieu/nang-luc/nhan-luc	Nhân lực	Nhân lực	2020-07-01 15:51:02.311354+00	\N	9	10	2	2	8
14		gioi-thieu/nang-luc/co-so-ha-tang	Cơ sở hạ tầng	Cơ sở hạ tầng	2020-07-01 15:51:02.313076+00	\N	7	8	2	2	8
\.


--
-- Data for Name: create_group_slide; Type: TABLE DATA; Schema: public; Owner: ckcvn
--

COPY public.create_group_slide (id, name, active) FROM stdin;
1	nhom 1	t
\.


--
-- Data for Name: product_category; Type: TABLE DATA; Schema: public; Owner: ckcvn
--

COPY public.product_category (id, image, slug, title, descriptions, created_date, updated_date, lft, rght, tree_id, level, parent_id) FROM stdin;
1		san-pham-dich-vu	Sản phẩm & dịch vụ	Sản phẩm & dịch vụ	2020-07-01 15:51:02.658619+00	\N	1	8	1	0	\N
2		san-pham-dich-vu/san-pham-khac	Sản phẩm khác	Sản phẩm khác	2020-07-01 15:51:02.660623+00	\N	6	7	1	1	1
5		san-pham-dich-vu/san-pham-khac/bien-soan-tai-lieu	Biên soạn tài liệu	Biên soạn tài liệu	2020-07-01 15:51:02.662326+00	\N	13	14	1	2	2
6		san-pham-dich-vu/san-pham-khac/thi-cong-dong-moi-sua-chua-hien-dai-hoa-tau	Giám sát thi công, đóng mới, sửa chữa, hiện đại hóa tàu	Giám sát thi công, đóng mới, sửa chữa, hiện đại hóa tàu	2020-07-01 15:51:02.663958+00	\N	7	8	1	2	2
7		san-pham-dich-vu/san-pham-khac/bao-dam-ky-thuat-tau	Bảo đảm kỹ thuật tàu	Bảo đảm kỹ thuật tàu	2020-07-01 15:51:02.665502+00	\N	9	10	1	2	2
8		san-pham-dich-vu/san-pham-khac/khai-thac-su-dung-va-dam-bao-ky-thuat	Khai thác sử dụng và đảm bảo kỹ thuật	Khai thác sử dụng và đảm bảo kỹ thuật	2020-07-01 15:51:02.667291+00	\N	11	12	1	2	2
3		san-pham-dich-vu/san-pham-nghien-cuu	Sản phẩm nghiên cứu khoa học	Sản phẩm nghiên cứu khoa học	2020-07-01 15:51:02.669081+00	\N	2	3	1	1	1
4		san-pham-dich-vu/san-pham-thiet-ke	Sản phẩm thiết kế tàu	Sản phẩm thiết kế tàu	2020-07-01 15:51:02.670745+00	\N	4	5	1	1	1
9		san-pham-dich-vu/tieu-chuan-do-luong-chat-luong	TIÊU CHUẨN - ĐO LƯỜNG - CHẤT LƯỢNG	TIÊU CHUẨN - ĐO LƯỜNG - CHẤT LƯỢNG	2020-07-01 15:51:02.672593+00	\N	6	11	1	1	1
10		san-pham-dich-vu/tieu-chuan-do-luong-chat-luong/cong-tac-do-luong-thu-nghiem	Công tác Đo lường, thử nghiệm	Công tác Đo lường, thử nghiệm	2020-07-01 15:51:02.674392+00	\N	9	10	1	2	9
11		san-pham-dich-vu/tieu-chuan-do-luong-chat-luong/tieu-chuan-van-ban-quy-pham-nganh-dong-tau	Tiêu chuẩn, văn bản quy phạm ngành đóng tàu	Tiêu chuẩn, văn bản quy phạm ngành đóng tàu	2020-07-01 15:51:02.676121+00	\N	7	8	1	2	9
\.


--
-- Name: Article_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvn
--

SELECT pg_catalog.setval('public."Article_post_id_seq"', 9, true);


--
-- Name: Product_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvn
--

SELECT pg_catalog.setval('public."Product_product_id_seq"', 25, true);


--
-- Name: addslide_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvn
--

SELECT pg_catalog.setval('public.addslide_id_seq', 17, true);


--
-- Name: article_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvn
--

SELECT pg_catalog.setval('public.article_category_id_seq', 1, false);


--
-- Name: create_group_slide_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvn
--

SELECT pg_catalog.setval('public.create_group_slide_id_seq', 1, true);


--
-- Name: product_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckcvn
--

SELECT pg_catalog.setval('public.product_category_id_seq', 1, false);


--
-- Name: Article_post Article_post_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public."Article_post"
    ADD CONSTRAINT "Article_post_pkey" PRIMARY KEY (id);


--
-- Name: Product_product Product_product_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public."Product_product"
    ADD CONSTRAINT "Product_product_pkey" PRIMARY KEY (id);


--
-- Name: addslide addslide_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.addslide
    ADD CONSTRAINT addslide_pkey PRIMARY KEY (id);


--
-- Name: article_category article_category_parent_id_slug_2e115ea3_uniq; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.article_category
    ADD CONSTRAINT article_category_parent_id_slug_2e115ea3_uniq UNIQUE (parent_id, slug);


--
-- Name: article_category article_category_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.article_category
    ADD CONSTRAINT article_category_pkey PRIMARY KEY (id);


--
-- Name: create_group_slide create_group_slide_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.create_group_slide
    ADD CONSTRAINT create_group_slide_pkey PRIMARY KEY (id);


--
-- Name: product_category product_category_parent_id_slug_4e7c48ae_uniq; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.product_category
    ADD CONSTRAINT product_category_parent_id_slug_4e7c48ae_uniq UNIQUE (parent_id, slug);


--
-- Name: product_category product_category_pkey; Type: CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.product_category
    ADD CONSTRAINT product_category_pkey PRIMARY KEY (id);


--
-- Name: Article_post_category_id_bf48cd76; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX "Article_post_category_id_bf48cd76" ON public."Article_post" USING btree (category_id);


--
-- Name: Product_product_category_id_ded7ea18; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX "Product_product_category_id_ded7ea18" ON public."Product_product" USING btree (category_id);


--
-- Name: addslide_slide_id_7b45bab2; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX addslide_slide_id_7b45bab2 ON public.addslide USING btree (slide_id);


--
-- Name: article_cat_parent__86e9c6_idx; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX article_cat_parent__86e9c6_idx ON public.article_category USING btree (parent_id);


--
-- Name: article_cat_title_46e0c7_idx; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX article_cat_title_46e0c7_idx ON public.article_category USING btree (title);


--
-- Name: article_category_parent_id_7918a69e; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX article_category_parent_id_7918a69e ON public.article_category USING btree (parent_id);


--
-- Name: article_category_slug_c9738fba; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX article_category_slug_c9738fba ON public.article_category USING btree (slug);


--
-- Name: article_category_slug_c9738fba_like; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX article_category_slug_c9738fba_like ON public.article_category USING btree (slug varchar_pattern_ops);


--
-- Name: article_category_tree_id_3576cbd5; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX article_category_tree_id_3576cbd5 ON public.article_category USING btree (tree_id);


--
-- Name: product_cat_parent__b54811_idx; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX product_cat_parent__b54811_idx ON public.product_category USING btree (parent_id);


--
-- Name: product_cat_title_74c9a5_idx; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX product_cat_title_74c9a5_idx ON public.product_category USING btree (title);


--
-- Name: product_category_parent_id_f6860923; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX product_category_parent_id_f6860923 ON public.product_category USING btree (parent_id);


--
-- Name: product_category_slug_e1f8ccc4; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX product_category_slug_e1f8ccc4 ON public.product_category USING btree (slug);


--
-- Name: product_category_slug_e1f8ccc4_like; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX product_category_slug_e1f8ccc4_like ON public.product_category USING btree (slug varchar_pattern_ops);


--
-- Name: product_category_tree_id_f3c46461; Type: INDEX; Schema: public; Owner: ckcvn
--

CREATE INDEX product_category_tree_id_f3c46461 ON public.product_category USING btree (tree_id);


--
-- Name: Article_post Article_post_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public."Article_post"
    ADD CONSTRAINT "Article_post_category_id_fkey" FOREIGN KEY (category_id) REFERENCES public.article_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Product_product Product_product_category_id_ded7ea18_fk_product_category_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public."Product_product"
    ADD CONSTRAINT "Product_product_category_id_ded7ea18_fk_product_category_id" FOREIGN KEY (category_id) REFERENCES public.product_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: addslide addslide_slide_id_7b45bab2_fk_create_group_slide_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.addslide
    ADD CONSTRAINT addslide_slide_id_7b45bab2_fk_create_group_slide_id FOREIGN KEY (slide_id) REFERENCES public.create_group_slide(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: article_category article_category_parent_id_7918a69e_fk_article_category_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.article_category
    ADD CONSTRAINT article_category_parent_id_7918a69e_fk_article_category_id FOREIGN KEY (parent_id) REFERENCES public.article_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_category product_category_parent_id_f6860923_fk_product_category_id; Type: FK CONSTRAINT; Schema: public; Owner: ckcvn
--

ALTER TABLE ONLY public.product_category
    ADD CONSTRAINT product_category_parent_id_f6860923_fk_product_category_id FOREIGN KEY (parent_id) REFERENCES public.product_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

