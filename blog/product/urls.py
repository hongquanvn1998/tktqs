from django.contrib import admin
from django.urls import path
from .views import *
from django.conf.urls import url

app_name = "product"

urlpatterns = [
    path('', dashboard, name = "dashboard"),
    # path('item/<int:id>',allProduct, name="allProduct"),
    # path('addproduct/', products, name = "addproduct"),
    path('detail-product/<int:id>', detail, name = "detail"),
    # path('update/<int:id>', updateProduct, name = "updateProduct"),
    # path('delete/<int:id>', deleteProduct, name = "deleteProduct"),

    # path('category-dashboard/', categorydashboard, name = "categorydashboard"),
    # path('add-category-product/', addcategoryProduct, name = "addcategoryProduct"),
    # path('category-update/<int:id>', updateCategoryProduct, name = "updateCategoryProduct"),
    # path('category-delete/<int:id>', deleteCategoryProduct, name = "deleteCategoryProduct"),
    
    # path('contact-dashboard/',dashboardContact,name="dashboardContact"),
    # path('contact-delete/<int:id>', deleteContact, name = "deleteContact"),
]