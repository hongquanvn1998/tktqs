from django.shortcuts import render, HttpResponse, redirect, get_object_or_404, reverse
from django.forms import modelformset_factory
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpResponseRedirect
from .forms import ImageForm, ProductForm, CategoryForm, ContactForm, ProductTypeForm
from .models import Product, Images, Category, Contact, ProductType
from article.models import Category as article_category
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

@login_required(login_url="user:login")
def products(request):
    ImageFormSet = modelformset_factory(Images,fields=('image',), extra=5)
    TypeFormSet = modelformset_factory(ProductType,fields=("name","unit","value","id"), extra=11)
    if request.method == 'POST':
        productForm = ProductForm(request.POST or None,request.FILES or None)
        formset = ImageFormSet(request.POST or None, request.FILES or None)
        typesformset = TypeFormSet(request.POST or None)
        if productForm.is_valid() and formset.is_valid() and typesformset.is_valid():
            product_form = productForm.save(commit=False)
            product_form.user = request.user
            product_form.save()
            for f in formset:
                try:
                    if f.cleaned_data:
                        photo = Images(post=product_form, image=f.cleaned_data.get('image'))
                        photo.save()
                except Exception as e:
                    break
            for i in typesformset:
                try:
                    if i.cleaned_data:
                        typeform = ProductType(post_article=product_form,name=i.cleaned_data.get('name'),value=i.cleaned_data.get('value'),unit=i.cleaned_data.get('unit'))
                        typeform.save()
                except Exception as e:
                    break
            messages.success(request,
                             "Đăng sản phẩm thành công!")
            return redirect("product:dashboard")
        else:
            print(productForm.errors, formset.errors)
    else:
        productForm = ProductForm()
        formset = ImageFormSet(queryset=Images.objects.none())
        typesformset = TypeFormSet(queryset=ProductType.objects.none())
    context = {'productForm': productForm, 'formset': formset,'typeformset':typesformset}
    return render(request, 'product/addproduct.html',context)

@login_required(login_url="user:login")
def dashboard(request):
    products = Product.objects.all()
    context = {
        "products" : products
    }
    return render(request,"product/dashboard.html", context)

# @login_required(login_url="user:login")
# def addproduct(request):
#     form = ProductForm(request.POST or None, request.FILES or None)
#     if form.is_valid():
#         product = form.save(commit = False)
#         product.save()
#         messages.warning(request, "Tạo bài viết thành công!")
#         return redirect("product:dashboard")
#     context = {
#         "form" : form
#     }
#     return render(request,"product/addproduct.html", context)

def detail(request, id):
    category_list = article_category.objects.filter(parent_id = 1)
    science_list = article_category.objects.filter(parent_id = 4)
    prod_category_list = Category.objects.filter(parent_id = 1)
    prod_other_category_list = Category.objects.filter(parent_id = 2)
    prod_standrad_category_list = Category.objects.filter(parent_id = 9)
    

    products = get_object_or_404(Product, id = id)
    prod_type_list = ProductType.objects.filter(product = products)
    all_products = Product.objects.filter(category_id=products.category_id)[:6]
    photos = Images.objects.filter(post=products)
    for i in prod_type_list:
        print(i)
    form = ContactForm(request.POST or None)
    if form.is_valid():
        contact = form.save(commit = False)
        contact.save()
        messages.warning(request, "Gửi yêu cầu thành công!")
    context = {
        "products" : products,
        "photos" : photos,
        "all_products":all_products,
        "form" : form,
        "category_list":category_list,
        "science_list":science_list,
        "prod_category_list":prod_category_list,
        "prod_other_category_list":prod_other_category_list,
        "prod_standrad_category_list":prod_standrad_category_list,
        "prod_type_list":prod_type_list,
    }
    return render(request,"item_detail.html", context)

def allProduct(request,slug):
    
    category_list = article_category.objects.filter(parent_id = 1)
    science_list = article_category.objects.filter(parent_id = 4)
    prod_category_list = Category.objects.filter(parent_id = 1)
    prod_other_category_list = Category.objects.filter(parent_id = 2)
    prod_standrad_category_list = Category.objects.filter(parent_id = 9)

    category = get_object_or_404(Category, slug = slug)
    products = Product.objects.filter(category_id = category.id)
    
    form = ContactForm(request.POST or None)
    if form.is_valid():
        contact = form.save(commit = False)
        contact.save()
        return redirect('allProduct', slug=category.slug)

    paginator = Paginator(products, 8)
    page = request.GET.get('page')
    products = paginator.get_page(page)
    context = {
        "category":category,
        "products" : products,
        "category_list":category_list,
        "science_list":science_list,
        "prod_category_list":prod_category_list,
        "prod_other_category_list":prod_other_category_list,
        "prod_standrad_category_list":prod_standrad_category_list,
        "form":form,
    }
    return render(request,"item.html", context)

def get_category(request,id):
    categories = Category.objects.filter(parent_id=id)
    paginator = Paginator(categories, 6)
    page = request.GET.get('page')
    categories = paginator.get_page(page)

    category_list = article_category.objects.filter(parent_id = 1)
    science_list = article_category.objects.filter(parent_id = 4)
    prod_category_list = Category.objects.filter(parent_id = 1)
    prod_other_category_list = Category.objects.filter(parent_id = 2)
    prod_standrad_category_list = Category.objects.filter(parent_id = 9)

    context = {
        "categories":categories,
        "category_list":category_list,
        "science_list":science_list,
        "prod_category_list":prod_category_list,
        "prod_other_category_list":prod_other_category_list,
        "prod_standrad_category_list":prod_standrad_category_list,
    }
    return render(request,"category_product.html", context)