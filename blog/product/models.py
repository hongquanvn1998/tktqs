from django.db import models
from ckeditor.fields import RichTextField
from django.utils import timezone
from ckeditor_uploader.fields import RichTextUploadingField
from mptt.models import MPTTModel, TreeForeignKey
from slugify import slugify
from django.core.validators import RegexValidator
from datetime import datetime

# Create your models here.
class Category(MPTTModel): 
    parent = TreeForeignKey('self',null=True, blank=True,
    related_name='children', verbose_name='Danh mục cha',
    db_index=True, on_delete=models.CASCADE, 
    default=0,)
    image = models.ImageField(blank = True, null = True, verbose_name="Ảnh đại diện danh mục")
    slug = models.SlugField(max_length=500, verbose_name='Đường dẫn')
    title = models.CharField(max_length=255, verbose_name='Tên danh mục')
    descriptions = models.CharField(max_length=100, blank=True,null=True, verbose_name='Mô tả')
    created_date =  models.DateTimeField(auto_now_add=True, blank=True,null=True, verbose_name='Ngày tạo')
    updated_date =  models.DateTimeField(blank=True,null=True, verbose_name='Cập nhật ngày')

    def __str__(self):
        return self.title 

    class Meta:
        unique_together = (('parent', 'slug',))  
        verbose_name_plural = "Danh mục sản phẩm"
        verbose_name = 'Danh mục sản phẩm'
        indexes =(
            models.Index(fields=['title']),
            models.Index(fields=['parent']),
            )
    class MPTTMeta: 
        order_insertion_by = ['id']

    def _generate_slug(self):
        if self.parent is None:
            r = slugify(self.title)
            self.slug = r
            return self
        else:
            r= slugify(self.title)
            self.slug = '%s/%s' % (self.parent.slug,r)
            return self
    
    def _change_slug(self): 
        if self.parent is None:
            return self._generate_slug()
        
        if self.parent.slug == '':
            return self._generate_slug() 

        _index = self.slug.find(self.parent.slug)
        if _index < 0:
            self._generate_slug() 
        if _index == 0:
            self._generate_slug() 
        else:
            self._generate_slug()  

        return self

        
    def save(self, *args, **kwargs): 
        if not self.pk:
            self._generate_slug() 
        else:
            self._change_slug()
            self.updated_date = datetime.now()

        super().save(*args, **kwargs)


    def get_slug_list(self):
        try:
            ancestors = self.get_ancestors(include_self=True)
        except:
            ancestors = []
        else:
            ancestors = [ i.slug for i in ancestors]
            slugs = []
        for i in range(len(ancestors)):
            slugs.append('/'.join(ancestors[:i+1]))
        return slugs

    @property
    def image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url



class Product(models.Model):
    category = TreeForeignKey('Category',on_delete=models.CASCADE, verbose_name='Danh mục')
    title = models.CharField(max_length=500,verbose_name="Tên sản phẩm")
    image = models.ImageField(verbose_name="Ảnh đại diện sản phẩm")
    content = RichTextUploadingField(blank=True,null=True,verbose_name="Nội dung",external_plugin_resources=[(
                                        'youtube',
                                        '/static/base/vendor/ckeditor_plugins/youtube/youtube/',
                                        'plugin.js',
                                    ),])
    code = models.CharField(max_length=100,verbose_name="Mã sản phẩm",blank=True,null=True,)
    created_date = models.DateTimeField(auto_now_add=True,verbose_name='Ngày tạo',blank=True,null=True)
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Cập nhật")
    
    def __str__(self):
        return self.title
    class Meta:
        db_table = 'Product_product'
        ordering = ["-created_date"]
        verbose_name = 'Sản phẩm'
        verbose_name_plural = 'Sản phẩm'
    @property
    def image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url
    # @property
    # def get_list(value, arg):
    #     res = [ x[arg] for x in value ]
    #     if type(res[0]) is list:
    #         res = list(itertools.chain(*res))
    #     return res
def get_image_filename(instance, filename):
    id = instance.post.id
    return "post_images/%s" % (id)  


class Images(models.Model):
    post = models.ForeignKey(Product,on_delete = models.CASCADE, default=None, verbose_name='Bài viết')
    image = models.ImageField(upload_to=get_image_filename,verbose_name='Image',blank=True,null=True)

    def __str__(self):
        return self.post.title + " Image"
    class Meta:
        db_table='products_images'
        verbose_name ='Hình ảnh'
        verbose_name_plural ='Hình ảnh' 

class ProductType(models.Model):
    class Meta:
        db_table='products_feature'
        verbose_name ='Thông số'
        verbose_name_plural ='Thông số' 

    product = models.ForeignKey(Product,on_delete = models.CASCADE, default=None, verbose_name='Sản phẩm')
    name = models.CharField(max_length=255,default=None,verbose_name="Tên thuộc tính",null=True,blank=True )
    unit = models.CharField(max_length=100,default=None,blank=True,null=True,verbose_name="Đơn vị")
    value = models.FloatField(verbose_name="Giá trị",blank=True,null=True)
    # active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

class Contact(models.Model):
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    full_name = models.CharField(max_length=500,verbose_name="Họ và Tên")
    phone = models.CharField(verbose_name="Điện thoại",validators=[phone_regex], max_length=11, blank=True)
    email = models.CharField(max_length=250,verbose_name="Email")
    address = models.CharField(max_length=500,verbose_name="Địa chỉ")
    note = models.CharField(max_length=500,verbose_name="Ghi chú")
    created_date = models.DateTimeField(auto_now_add=True,verbose_name='Ngày tạo',blank=True,null=True)
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Cập nhật vào",blank=True,null=True)
    class Meta:
        db_table = "contact"
        verbose_name = 'Liên hệ'
        verbose_name_plural = 'Liên hệ'

