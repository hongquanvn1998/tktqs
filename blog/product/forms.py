from django import forms
from .models import Product, Images,Category,Contact, ProductType
from django.forms.models import inlineformset_factory

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = [
            "title",
            "descriptions",
            'parent',
            "slug",
        ]
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # unused_files = Category.objects.filter(category__isnull=True)
        instance = kwargs.get("instance")



class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = [
            "title",
            "code",
            'category',
            "image",
            "content",
        ]
    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     unused_files = Category.objects.filter(category__isnull=True)
    #     instance = kwargs.get("instance")

class ImageForm(forms.ModelForm):
    image = forms.ImageField(label='Image')    
    class Meta:
        model = Images
        fields = ('image', )

class ProductTypeForm(forms.ModelForm):
    name = forms.CharField(required = False,label="Tên thuộc tính")
    unit = forms.CharField(required = False,label="Đơn vị")
    value = forms.FloatField(label="Giá trị",required = False)
    class Meta:
        model = ProductType
        fields = ["name",
        "unit",
        "value", "id"]
    # def __init__(self, *args, **kwargs):
    #     super(ProductTypeForm, self).__init__(*args, **kwargs)
    #     self.fields['name'].required = False
    #     self.fields['unit'].required = False
    #     self.fields['value'].required = False
    #     self.fields['active']=True
        
# BookImageFormSet = inlineformset_factory(BookForm, BookImage, extra=2)
# BookPageFormSet = inlineformset_factory(Product, ProductType,form=ProductForm, extra=5)

class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = [
            "full_name",
            'phone',
            "email",
            "address",
            "note",
        ]


