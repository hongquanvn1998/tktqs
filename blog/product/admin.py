from django.contrib import admin
from .models import Product, Images, Category, Contact, ProductType
from django.forms.widgets import CheckboxSelectMultiple
from django.db import models
# Register your models here.


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display =('full_name','phone','email','address','note','created_date')

class ProductImageAdmin(admin.StackedInline):
    model = Images
    extra = 1
class TypeInline(admin.StackedInline):
    model = ProductType
    fieldsets = (
        ('', { 
            'fields': (
                ('name','unit','value'),
            )
        }),    
 
    )
    extra = 1
    
# @admin.register(ProductType)
# class ProductTypeAdmin(admin.ModelAdmin):
    
#     list_display =('name','value','unit')
    

class CategoryInline(admin.TabularInline):
    model = Category
    fieldsets = (
        ('', { 
            'fields': (
                ('title','descriptions'),
                ('image'),
            )
        }), 
    )   
    extra =0
    
@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    fieldsets = (
        ('', { 
            'fields': (
                ('parent','title'),
                ('descriptions','image'),
            )
        }),    
 
    )   
    list_display = (
                    'title',
                    'created_date',
                    'parent',
                    'descriptions'
                    )
    list_display_links = (
                        'title',
                        )
    search_fields = (
                        'title',
                    )
    list_filter = (
                    'created_date',
                    )
    list_display =('id','title','slug','parent')
    inlines = [CategoryInline]


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Thông tin chung', { 
            'fields': (
                ('title','code'),
                ('category'),
                ('image'),
                ('content'),
                )
        }),    
 
    )
    inlines = [ProductImageAdmin,TypeInline]
    list_display = ('code','title','category','image','content')
    list_display_links = ('title','code','category')

# @admin.register(Images)
# class ProductImageAdmin(admin.ModelAdmin):
#     pass