from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
from .forms import *
def register(request):
    form = RegisterForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        newUser = User(username = username)
        newUser.set_password(password)
        newUser.save()
        messages.success(request,"Başarıyla Kayıt Oldunuz!")
        login(request,newUser)
        return redirect("index")
    context = {
        'form': form
    }
    return render(request, "register.html", context)

def loginUser(request):
    form  = LoginForm(request.POST or None)
    
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user = authenticate(username = username, password = password)
        if user is None:
            messages.warning(request, "Tên đăng nhập hoặc mật khẩu không đúng")
            context = {
                "form" : form
            }
            return render(request, "admin/login.html", context)
        messages.success(request,"Bạn đã đăng nhập thành công!")
        login(request, user)
        return redirect("index")
    context = {
        "form" : form
    }
    return render(request, "admin/login.html", context)
def logoutUser(request):
    logout(request)
    messages.info(request,"Đăng xuất thành công!")
    return redirect("index")