from django import forms
from django.contrib.auth.models import User

class LoginForm(forms.Form):
    username = forms.CharField(max_length = 50, label="Tên đăng nhập")
    password = forms.CharField(max_length = 20, label = "Mật khẩu", widget = forms.PasswordInput)
    def clean(self):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")

class RegisterForm(forms.Form):
    username = forms.CharField(max_length = 50, label="Tên đăng nhập")
    password = forms.CharField(max_length = 20, label = "Mật khẩu", widget = forms.PasswordInput)
    confirm = forms.CharField(max_length= 20, label = "Xác nhận", widget = forms.PasswordInput)
    
    def clean(self):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")
        confirm  = self.cleaned_data.get("confirm")
        if password and confirm and password != confirm:
            self.add_error('confirm', "Mật khẩu không hợp lệ!")
        elif User.objects.filter(username = username).count() > 0:
            self.add_error('username', "Người dùng đã tồn tại")
        return super(RegisterForm, self).clean()
